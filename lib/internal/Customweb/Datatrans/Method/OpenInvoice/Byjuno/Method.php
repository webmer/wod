<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */




/**
 *
 * @author Sebastian Bossert
 * @Method(paymentMethods={'ByjunoInvoice', 'ByjunoAccount', 'ByjunoInstallment'})
 *
 */
class Customweb_Datatrans_Method_OpenInvoice_Byjuno_Method extends Customweb_Datatrans_Method_OpenInvoice_AbstractInvoice {

	public function getFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $authorizationMethod, $isMoto, $customerPaymentContext){
		$elements = array();
		$birthday = $orderContext->getBillingDateOfBirth();
		if ($birthday === null || empty($birthday)) {
			/* @var $customerPaymentContext Customweb_Payment_Authorization_IPaymentCustomerContext */
			$defaultYear = null;
			$defaultMonth = null;
			$defaultDay = null;
			if ($customerPaymentContext !== null) {
				$map = $customerPaymentContext->getMap();
				if (isset($map['date_of_birth']['year'])) {
					$defaultYear = $map['date_of_birth']['year'];
				}
				if (isset($map['date_of_birth']['month'])) {
					$defaultMonth = $map['date_of_birth']['month'];
				}
				if (isset($map['date_of_birth']['day'])) {
					$defaultDay = $map['date_of_birth']['day'];
				}
			}
			
			$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('year', 'month', 'day', $defaultYear, $defaultMonth, $defaultDay);
		}
		
		$gender = $orderContext->getBillingGender();
		if ($gender == "company" || empty($gender)) {
			$genders = array(
				'female' => Customweb_I18n_Translation::__('Female'),
				'male' => Customweb_I18n_Translation::__('Male') 
			);
			$default = Customweb_Datatrans_SalutationUtil::getGender($orderContext->getBillingAddress()->getSalutation());
			$control = new Customweb_Form_Control_Select('gender', $genders, $default);
			$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("Please select your gender.")));
			
			$element = new Customweb_Form_Element(Customweb_I18n_Translation::__('Gender'), $control, 
					Customweb_I18n_Translation::__('Please select your gender.'));
			$elements[] = $element;
		}
		
		$control = new Customweb_Form_Control_TextInput("phone", $orderContext->getBillingAddress()->getPhoneNumber());
		$elements[] = new Customweb_Form_Element(Customweb_I18n_Translation::__("Phone"), $control);
		$control = new Customweb_Form_Control_TextInput("cellphone", $orderContext->getBillingAddress()->getMobilePhoneNumber());
		$mobileElement = new Customweb_Form_Element(Customweb_I18n_Translation::__("Mobile Phone"), $control);
		
		if ($this->isFingerprintActive()) {
			$elements[] = $this->createDeviceFingerprintNoScript($orderContext);
			$mobileElement->appendJavaScript($this->createDeviceFingerprintJavascript($orderContext));
		}
		
		$elements[] = $mobileElement;
		
		return $elements;
	}

	private function isFingerprintActive(){
		$orgId = trim($this->getPaymentMethodConfigurationValue('ORG_ID'));
		return !empty($orgId) && $this->getSolvencyCheckConfiguration() == 'authorization';
	}

	private function createDeviceFingerprintNoScript(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$snippet = '<noscript><iframe style="width: 100px; height: 100px; border: 0; position: absolute; top: -5000px;" src="https://h.online-metrix.net/tags?org_id=ORG_ID&session_id=UNIQUE_SESSION_ID"></iframe></noscript>';
		$orgId = $this->getPaymentMethodConfigurationValue('ORG_ID');
		$uniqueSessionId = $orderContext->getCheckoutId();
		$snippet = str_replace("ORG_ID", $orgId, $snippet);
		$snippet = str_replace("UNIQUE_SESSION_ID", $uniqueSessionId, $snippet);
		$control = new Customweb_Form_Control_HiddenHtml('datatrans-device-fingerprint-noscript', $snippet);
		return new Customweb_Form_HiddenElement($control);
	}

	private function createDeviceFingerprintJavascript(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$snippet = 'https://h.online-metrix.net/fp/tags.js?org_id=ORG_ID&session_id=UNIQUE_SESSION_ID';
		$orgId = $this->getPaymentMethodConfigurationValue('ORG_ID');
		$uniqueSessionId = $orderContext->getCheckoutId();
		$snippet = str_replace("ORG_ID", $orgId, $snippet);
		$snippet = str_replace("UNIQUE_SESSION_ID", $uniqueSessionId, $snippet);
		$snippet = 'var js = document.createElement("script");
js.type = "text/javascript";
js.src = "' . $snippet . '";
document.body.appendChild(js);';
		return $snippet;
	}

	public function getPaymentMethodType(){
		return 'INT';
	}

	private function getSubPaymentMethodType(){
		$parameters = $this->getPaymentMethodParameters();
		return $parameters["sub_pm"];
	}

	public function getRecurringAuthorizationParameters(Customweb_Datatrans_Authorization_Transaction $transaction){
		$formData = array();
		if ($transaction->getInitialTransaction() !== null) {
			$formData = $transaction->getInitialTransaction()->getByjunoParameters();
		}
		$parameters = $this->getAuthorizationParameters($transaction, $formData);
		return $parameters;
	}

	protected function getCustomerParameters(Customweb_Datatrans_Authorization_Transaction $transaction){
		$parameters = $this->getBillingAddressParameters($transaction);
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$parameters['sub_pmethod'] = $this->getSubPaymentMethodType();
		$parameters['uppCustomerDetails'] = 'yes';
		$parameters['uppCustomerId'] = $transaction->getTransactionContext()->getOrderContext()->getCustomerId();
		if ($this->isFingerprintActive()) {
			$parameters['intrumDeviceFingerprintId'] = $orderContext->getCheckoutId();
		}
		$street = $parameters['uppCustomerStreet'];
		$streetParts = Customweb_Util_Address::splitStreet($street, $orderContext->getBillingAddress()->getCountryIsoCode(), 
				$orderContext->getBillingAddress()->getPostCode());
		$parameters['uppCustomerStreet'] = $streetParts['street'];
		$parameters['uppCustomerStreet2'] = $streetParts['street-number'];
		$parameters['uppCustomerLanguage'] = $orderContext->getLanguage()->getIso2LetterCode();
		
		return $parameters;
	}

	public function getAuthorizationParameters(Customweb_Datatrans_Authorization_Transaction $transaction, array $formData){
		$parameters = parent::getAuthorizationParameters($transaction, $formData);
		if (isset($formData['year']) && isset($formData['month']) && isset($formData['day'])) {
			$parameters['uppCustomerBirthDate'] = $formData['year'] . '-' . $formData['month'] . '-' . $formData['day'];
			$customerContext = $transaction->getPaymentCustomerContext();
			if ($customerContext !== null) {
				$map = array();
				$map['date_of_birth']['year'] = intval($formData['year']);
				$map['date_of_birth']['month'] = intval($formData['month']);
				$map['date_of_birth']['day'] = intval($formData['day']);
				$customerContext->updateMap($map);
			}
		}
		
		if (isset($formData['gender'])) {
			if (strtolower($formData['gender']) == 'female') {
				$parameters['uppCustomerGender'] = 'Female';
			}
			else {
				$parameters['uppCustomerGender'] = 'Male';
			}
		}
		
		$parameters['uppCustomerType'] = 'P';
		
		if (isset($parameters['uppCustomerGender'])) {
			$transaction->setCustomerGender($parameters['uppCustomerGender']);
		}
		
		if (isset($parameters['uppCustomerBirthDate'])) {
			$transaction->setCustomerBirthDate($parameters['uppCustomerBirthDate']);
		}
		
		$parameters['uppCustomerCellPhone'] = $formData['cellphone'];
		$parameters['uppCustomerPhone'] = $formData['phone'];
		
		$transaction->setByjunoParameters($parameters);
		
		return $parameters;
	}

	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData){
		$check = $this->getSolvencyCheckConfiguration();
		if ($check == 'prevalidate' || $check == 'validate') {
			$this->initiateValidation($orderContext, $paymentContext, $formData);
		}
	}

	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext){
		parent::preValidate($orderContext, $paymentContext);
		
		$check = $this->getSolvencyCheckConfiguration();
		if ($check == 'prevalidate') {
			$this->initiateValidation($orderContext, $paymentContext);
		}
	}

	private function getSolvencyCheckConfiguration(){
		return strtolower($this->getPaymentMethodConfigurationValue('solvency'));
	}

	private function initiateValidation(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, $formData = null){
		$currency = $orderContext->getCurrencyCode();
		if ($currency != 'CHF') {
			throw new Exception(
					Customweb_I18n_Translation::__('Currency !currency not supported, must be CHF', array(
						'!currency' => $currency 
					)));
		}
		$address = $orderContext->getBillingAddress();
		$amount = Customweb_Datatrans_Util::formatAmount($orderContext->getOrderAmountInDecimals(), $currency);
		
		$birthday = $address->getDateOfBirth();
		$gender = ucfirst($address->getGender());
		if (isset($formData['year']) && isset($formData['month']) && isset($formData['day'])) {
			$birthday = Customweb_Core_DateTime::_()->setDate(intval($formData['year']), intval($formData['month']), intval($formData['day']));
		}
		
		if (isset($formData['gender'])) {
			$gender = 'Female';
			if (strtolower($formData['gender']) == 'male') {
				$gender = 'Male';
			}
		}
		if (empty($birthday) || empty($gender)) {
			return;
		}
		
		$data = array(
			"gender" => $gender,
			"phone" => $formData['phone'],
			"cellphone" => $formData['cellphone'] 
		);
		
		$this->processValidation($address, $amount, $currency, $birthday, $data, $orderContext);
	}

	public function getValidationResponse(Customweb_Payment_Authorization_OrderContext_IAddress $address, $amount, $currency, DateTime $birthday, $additional, Customweb_Payment_Authorization_IOrderContext $orderContext){
		$upp = 'uppCustomer';
		$streetParts = Customweb_Util_Address::splitStreet($address->getStreet(), $address->getCountryIsoCode(), $address->getPostCode());
		
		$parameters = array(
			'amount' => $amount,
			'currency' => $currency,
			'reqtype' => 'SCN',
			'pmethod' => $this->getPaymentMethodType(),
			$upp . 'Gender' => $additional['gender'],
			$upp . 'FirstName' => $address->getFirstName(),
			$upp . 'LastName' => $address->getLastName(),
			$upp . 'Street' => $streetParts['street'],
			$upp . 'Street2' => $streetParts['street-number'],
			$upp . 'Country' => $address->getCountryIsoCode(),
			$upp . 'City' => $address->getCity(),
			$upp . 'ZipCode' => $address->getPostCode(),
			$upp . 'BirthDate' => $birthday->format('d.m.Y'),
			$upp . 'Email' => $address->getEMailAddress(),
			$upp . 'Phone' => $additional['phone'],
			$upp . 'CellPhone' => $additional['cellphone'],
			$upp . 'Id' => $orderContext->getCustomerId(),
			$upp . 'Language' => strtolower($orderContext->getLanguage()->getIso2LetterCode()) 
		);
		
		if ($this->isFingerprintActive()) {
			$parameters['intrumDeviceFingerprintId'] = $orderContext->getCheckoutId();
		}
		$xmlRequest = new Customweb_Datatrans_XmlRequest($this->getGlobalConfiguration()->getMerchantId());
		$xmlRequest->setReferenceNumber('sc' . $orderContext->getCheckoutId());
		$xmlRequest->setAuthorizationRequest();
		$xmlRequest->setParameters(Customweb_Datatrans_Util::fixCustomerParametersForRemoteRequest($parameters));
		if ($this->getGlobalConfiguration()->isTestMode()) {
			$xmlRequest->setTestOnly();
		}
		Customweb_Datatrans_Util::signXmlRequest($this->getGlobalConfiguration(), $xmlRequest);
		
		$response = Customweb_Datatrans_Util::sendXmlRequest($this->getGlobalConfiguration()->getXmlAuthorizationUrl(), $xmlRequest);
		$responseParameters = Customweb_Datatrans_Util::getAuthorizationParametersFromXmlResponse($response);
		$this->addUsedAmount($orderContext, $amount);
		if (strstr($responseParameters['allowedPaymentMethods'], $this->getSubPaymentMethodType()) !== false) {
			return self::SUCCESS;
		}
		return self::FAIL;
	}

	private function processValidation(Customweb_Payment_Authorization_OrderContext_IAddress $address, $amount, $currency, DateTime $birthday, $additional, Customweb_Payment_Authorization_IOrderContext $orderContext){
		$status = $this->getValidationStatus($address, $amount, $currency, $birthday, $additional, $orderContext);
		if ($status === self::FAIL) {
			throw new Exception(Customweb_I18n_Translation::__("This payment method is currently unavailable."));
		}
	}
}
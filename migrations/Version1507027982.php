<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1507027982
 *
 * @package Migration
 */
class Version1507027982 implements Migration
{
    /**
     * Version1507027982 constructor.
     * Inject Dependency
     *
     */
    public function __construct() { }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $identifier = 'cms_footer_banner_container';
        $store_id = 0;
        $block = $objectManager->create('Magento\Cms\Model\Block');
        $block->setStoreId($store_id); // store for block you want to update
        $block->load($identifier, 'identifier');
        $block->setIdentifier($identifier);
        $block->setTitle('cms_footer_banner_container');
        $block->setIsActive(1);
        $block->setStores($store_id);
        $block->save();

    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
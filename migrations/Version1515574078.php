<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1515574078
 *
 * @package Migration
 */
class Version1515574078 implements Migration
{
    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    private $resourceConfig;
    /**
     * Version1515574078 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $info = array(
            'customer/address_templates/html'=>'{{depend prefix}}{{var prefix}} <br />{{/depend}}
{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}{{depend firstname}}<br/>{{/depend}}
{{depend company}}{{var company}}<br />{{/depend}}
{{if street1}}{{var street1}}<br />{{/if}}
{{depend street2}}{{var street2}}<br />{{/depend}}
{{depend street3}}{{var street3}}<br />{{/depend}}
{{depend street4}}{{var street4}}<br />{{/depend}}
{{if city}}{{var city}},  {{/if}}{{if region}}{{var region}}, {{/if}}{{if postcode}}{{var postcode}}{{/if}}<br/>
{{var country}}<br/>
{{depend telephone}}T: {{var telephone}}{{/depend}}
{{depend fax}}<br/>F: {{var fax}}{{/depend}}
{{depend vat_id}}<br/>VAT: {{var vat_id}}{{/depend}}'
        );

        foreach ($info as $key =>$value) {
            $this->resourceConfig->saveConfig(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
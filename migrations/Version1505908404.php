<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505908404
 *
 * @package Migration
 */
class Version1505908404 implements Migration
{
    /**
     * Version1505908404 constructor.
     * Inject Dependency
     *
     */
    public function __construct() { }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $object_manager =\Magento\Framework\App\ObjectManager::getInstance();
        $linksArray = array(
            array(
                'name'=>'neuigkeiten',
                'link'=>'#',
                'extra'=>'1',
                'stores'=>'0',
                'order'=>'0',
                'status'=>'1'
            ),
            array(
                'name'=>'abonnement',
                'link'=>'#',
                'extra'=>'1',
                'stores'=>'0',
                'order'=>'1',
                'status'=>'1'
            ),
            array(
                'name'=>'marken',
                'link'=>'marken/show/allbrands',
                'extra'=>'1',
                'stores'=>'0',
                'order'=>'2',
                'status'=>'1'
            ),
        );


        foreach ($linksArray as $item ) {
            $model = $object_manager->create('Magiccart\Magicmenu\Model\Magicmenu');
            $model->setData($item);
            $model->save();

        }


    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
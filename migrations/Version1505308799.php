<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505308799
 *
 * @package Migration
 */
class Version1505308799 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Advertising')
            ->setIdentifier('advertising')
            ->setContentHeading('Werbung')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('Advertising')
            ->setIdentifier('advertising')
            ->setContentHeading('Advertising')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p><span lang="en-GB">Ads on worldofdelight.ch</span></p>
<p><span lang="en-GB">On worldofdelight.ch you can use especially attractive placements for your company, your products, or your services, and present them ideal.</span></p>
<p><span lang="en-GB">Your benefits summarized:</span></p>
<ul class="adv_list">
<li><span lang="en-GB">You are present where your client is.</span></li>
<li><span lang="en-GB">Your placement is directed at your target audience and efficient.</span></li>
<li><span lang="en-GB">No spreading loss</span></li>
<li><span lang="en-GB">Measurable results and reporting&rsquo;s</span></li>
<li><span lang="en-GB">Flexible and individual on/offline advertising efforts</span></li>
</ul>
<p><span lang="en-GB">No platforms offer audience targeted sales approach to smokers of the age of 18 and above. Profit now and save your presence on unique starting conditions.</span></p>
<p><span lang="en-GB">We love to advise you.</span></p>
<p><span lang="en-GB"><b>Contact and booking</b></span></p>
<div class="contact_inner">
<p><span lang="en-GB">worldofdelight.ch</span></p>
<p><span lang="en-GB">Antonio Papa</span></p>
<p><span lang="en-GB">Bahnhofstrasse 28</span></p>
<p><span lang="en-GB">6300 Zug</span></p>
<p><span lang="en-GB">Phone:  <span class="adv_phone">+41 (0) 76 596 54 11</span></p>
<p><span lang="en-GB">E-mail: <span class="adv_email">werbung(at)worldofdelight.ch</span></p>
</div>
<a class="wod_button" href="#">Download media data</a>';
    }

    private function getContentDe()
    {
        return '<p>Werben auf worldofdelight.ch</p>
<p>Auf worldofdelight.ch haben Sie die M&ouml;glichkeit, besonders attraktive Werbepr&auml;senzen f&uuml;r Ihr Unternehmen, Ihre Produkte oder Ihre Dienstleistungen zu buchen und optimal zu pr&auml;sentieren.</p>
<p>Ihre Vorteile auf einen Blick:</p>
<ul class="adv_list">
<li>Sie sind da pr&auml;sent, wo Ihre K&auml;ufer aktiv sind</li>
<li>Ihr Werbeplatz ist zielgruppengenau und effizient</li>
<li>Kein Streuverlust</li>
<li>Messbare Resultate und Reportings</li>
<li>flexible und individuelle on/offline Werbemassnahmen</li>
</ul>

<p>Keine Plattform bietet Ihnen heute eine zielgerichtetere Kundenansprache an Raucher &uuml;ber 18 Jahre. Profitieren Sie jetzt und sichern Sie sich Ihre Pr&auml;senz zu einzigartigen Startkonditionen.</p>
<p>Wir beraten Sie gerne.</p>
<p><b>Kontakt und Buchung</b></p>
<div class="contact_inner">
<p><span lang="en-US">worldofdelight.ch</span></p>
<p><span lang="en-US">Antonio Papa</span></p>
<p><span lang="en-US">Bahnhofstrasse 28</span></p>
<p>6300 Zug</p>
<p>Telefon: <span class="adv_phone">+41 (0) 76 596 54 11</span></p>
<p>E-Mail:  <span class="adv_email">werbung(at)worldofdelight.ch</span></p>
</div>
<a class="wod_button" href="#">Mediadaten herunterladen</a>';
    }

}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class Version1507186460
 *
 * @package Migration
 */
class Version1507186460 implements Migration
{
    /**
     * @var ConfigInterface
     */
    private $resourceConfig;
    /**
     * Version1507186460 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        ConfigInterface $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $info = array(
            'customer/address/dob_show'=>'opt',
            'customer/address/prefix_show'=>'opt',
            'customer/address/prefix_options'=>'Herr;Frau'

        );

        foreach ($info as $key =>$value) {
            $this->resourceConfig->saveConfig(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
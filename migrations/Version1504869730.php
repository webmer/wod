<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1504869730
 *
 * @package Migration
 */
Class Version1504869730 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504869730 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('AGB und Datenschutz')
            ->setContentHeading('Allgemeine Geschäftsbedingungen')
            ->setIdentifier('agb-und-datenschutz')
            ->setCustomLayoutUpdateXml('<referenceContainer name="before.body.end"><block class="Magento\Framework\View\Element\Template" name="test" template="Magento_Theme::include_js.phtml"/></referenceContainer> ')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('AGB und Datenschutz')
            ->setContentHeading('Terms and conditions')
            ->setIdentifier('agb-und-datenschutz')
            ->setCustomLayoutUpdateXml('<referenceContainer name="before.body.end"><block class="Magento\Framework\View\Element\Template" name="test" template="Magento_Theme::include_js.phtml"/></referenceContainer> ')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p>This website is provided to you according to the terms and conditions at hand (successive «terms and conditions»).
    This website is owned by worldofdelight.ch and the following terms «we», «us» / «our» refer to Genusswelt. The
    execution of the orders in the World of Deilght online shop on this website (successive «worldofdelight.ch») are
    carried out through Genusswelt AG, Bahnhofstrasse 28, 6300 Zug, e-mail-addresse: kontakt(at)genusswelt.ch).</p>
<ul class="data_inner">
    <li class="title_paragraph toggle">1. Area of applicataion</li>
    <div class="text_wrap">
        <p><span lang="en-GB">These terms and conditions are applicable to the usage of this website and the worldofdelight.ch online shops on whis website as well as shopping on worldofdelight.ch. With your registration on this webseite and its usage and the usage of worldofdelight.ch you are accepting these terms and conditions.</span>
        </p>
        <p><span lang="en-GB">Your shoppings on worldofdelight.ch and its contracts of purchase are axclusively subject to these terms and conditions.</span>
        </p>
        <p><span lang="en-GB">Furthermore, these terms and conditions apply to the advertisings displayed on this website or carried out by the brand worldofdelight.ch, especially competitions and raffles, unless other, specific terms and conditions are explicitly disclosed for certain activities.</span>
        </p>
        <p><span lang="en-GB">We reserve one&rsquo;s right to adabt and change these terms and conditions at any time. Applicaple are the terms and conditions at the time when the user is using the website or at the time the shopping was made on worldofdelight.ch. You are responsible to check the terms and conditions regarding changes.</span>
        </p>
        <p><span lang="en-GB">In case of discrepancy between the German, French, Italian, or English version of these terms and conditions the German version is applied.</span>
        </p>
    </div>
    <li class="title_paragraph toggle">2. Access to the website and password</li>
    <div class="text_wrap">
        <p><span lang="en-GB">This website as well as the worldofdelight.ch onlie shop are directed to smokers of at least 18 years of age that are domiciled in Switerland and use the tobaccoproducts for personal usage. You must verify your age to use this website and worldofdelight.ch. Furthermore, the location of the computer with which you access this website and worldofdelight.ch is located through recording the IP-address of your computer. Access to this website and worldofdelight.ch is only granted from a location within Switzerland. Furthermore, you must login and choose a username and a password that are only for the usage by yourselve and must not be given to third parties. This measure is aimed to prevent usage of the website through unauthorized third parties. You as user of this website and worldofdelight.ch are bound to take all measurements to protect the secrecy of your user name and password.</span>
        </p>
        <p><span lang="en-GB">You are held to immediately inform us when there is cause of assumption that someone else got to know your username or password or that there is a danger that your username or password is used without consent. You are responsible for all actions that happen through usage of your username or password. We reject any liability for damage caused by circulation of your username and password due to third parties.</span>
        </p>
        <p><span lang="en-GB">Your personal information that is collection throughout the registration process must correspond mith the throuth and be correct.</span>
        </p>
        <p><span lang="en-GB">Your user account allows usage of this website and the access to worldofdelight.ch only for your private and not commercial usage.</span>
        </p>
        <p><span lang="en-GB">We reserve one&rsquo;s right do delete and close your user account withouth any previous information if you infringe with your obligation that arise through these terms and conditions</span>
        </p>
    </div>
    <li class="title_paragraph toggle">3. Usage of this website and worldofdelight.ch</li>
    <div class="text_wrap">
        <p><span lang="en-GB">You are personally responsible for all accesses on this website and worldofdelight.ch that are made through your internet connection. You must not give your password to third parties to access this website or worldofdelight.ch as a registered user.</span>
        </p>
        <ul class="list_agb">
            <li>Furthermore, you must not use this website or worldofdelight.ch respectively send us, this website,
                other
                users of this website, or worldofdlight.ch content in any way that:
            </li>

            <li>is violating right, laws, regulations, or terms of the applicable legal system;</li>
            <li>is fraudulent, criminal or illigal intent;</li>
            <li>is unfounded or outdated;</li>
            <li>is obscene; offensive, pornographic, vulgar, profane, racist, sexist, discriminating, insulting,
                derogative,
                damaging, pestering, threatening, aggravating, malicious, abusive, full of hate, minatorial, defaming,
                untrue, or of political nature;
            </li>
            <li>is pretending to be another person or institution or untruly affect a relationship to another person or
                institution;
            </li>
            <li>could harm the copyright or other intangible rights (including among other things copyright, brands
                rights,
                and broadcasting rights) or the privacy or other rights of our company or any third party;
            </li>
            <li>could run coutrary to our interest and the instrest of worldofdelight.ch;</li>
            <li>could run countrary to certain rules and requirements that we on this website regarding certain parts of
                the
                website of worldofdelight.ch or this website in general determine;
            </li>
            <li>
                or that are connected to the usage, procurement, or transmitting of viruses, unwanted e-mails, trojans,
                trap
                doors, backdoors, easter eggs, worms, time bombs, cancelbots, or computer programming routines through a
                person that intends to damage a system, data, or personal information, or to manipulate, spy on, or
                steal
                components.
            </li>
        </ul>

    </div>
    <li class="title_paragraph toggle">4. Conditions for competitions on worldofdeligt.ch</li>
    <div class="text_wrap">
        <p><span lang="en-GB">All competitions on worldofdelight.ch are directed strictly to smokers of at least 18 years of age that are domiciled in Switzerland. Only people registrated on worldofdelight.ch may take part in these competitions.</span>
        </p>
        <p><span lang="en-GB">Employees of worldofdelight.ch, their partners and service providers, as well as family members of employees are excluded from taking part in the competitions.</span>
        </p>
        <p><span lang="en-GB">By taking part in a competition on worldofdelight.ch a declaration of consent is assumed with the general terms and conditions of worldofdelight.ch as well as the particular terms and conditions of the respective competition.</span>
        </p>
        <p><span lang="en-GB">Futhermore, we reserve one&rsquo;s right to end a competition by our own discretionary authority. The ending of a competition ist not connected to any right of any kind of compensation. No rights are obtained and no service can be demanded. </span>
        </p>
        <p><span lang="en-GB">Furthermore, we disclaim liability if any data entered into the form or e-mails from participants are not recognized by the system respectively if due to a technical or other error a person is prevented from takeing part in the competition.</span>
        </p>
        <p><span lang="en-GB">We also reserve one&rsquo;s right to exclude participants from the competition. Especially participants which deceive or try to deceive (signing up more participant accounts), or participants who infringe with the general or specific terms and conditions of the competition in any way that is trying to misuse the offered competition.</span>
        </p>
        <p><span lang="en-GB">Incidentally, all remeining regulation of these terms and condition, especially those in regard of access of the website, security policy, guarantee and liability exclusion, and privacy apply.</span>
        </p>
    </div>
    <li class="title_paragraph toggle">5. Entering a purchase contract on worldofdelight.ch</li>
    <div class="text_wrap">
        <p><span lang="en-GB">We contracted Genusswelt AG with the selling of products that are ordered via worldofdelight.ch. So, when you order on worldofdelight.ch you buy the ordered items from Genusswelt AG. Furthermore, Genusswelt AG is responsible for the composition, delivery, and billing of your order.</span>
        </p>

        <p><span lang="en-GB">As soon as you call up worldofdelight.ch with your password you have the option to choose the offered goods and put them into your virtual cart by clicking the button &laquo;add to cart&raquo;. To continue your shopping, you must open the cart on the right of the website and click on &laquo;view cart&raquo;. A new screen opens with information regarding the good in the cart. Then you may start your order process by clicking on &laquo;buy&raquo;. On the following screen, you may need to enter your shipping address. You&rsquo;ll be taken to the next step by clicking &laquo;proceed&raquo;. The following screens ask for shipping and payment method. When yo reached the last step of the order process and as soon as you entered the mandatory payment details you must accept the terms and conditions as well as the privacy policy and may then finish your order by clicking &laquo;confirm order&raquo;. An order confirmation will appear. An order confirmation is also sent to you by e-mail. We recommend you print out your order and the terms and conditions.</span>
        </p>
        <p><span lang="en-GB">Your order is a statement of intent of you to buy items. After receiving your order, you receie an order confirmation from Genusswelt AG which notifies the receiving of your order. The order confirmation ist not an acceptance of your ordern. The order confirmation contains all relevant information regarding your order. In the case that the order confirmation contains unfounded information you are obligated to inform Genusswelt AG immediately.</span>
        </p>
        <p><span lang="en-GB">Your order is only confirmend and a purchase contract come about when Genussweilt AG sends you an e-mail which confirmes the shipment of your order.</span>
        </p>
        <p><span lang="en-GB">Genusswelt AG will inform you if necessary, if it decides not to take your order.</span>
        </p>
        <p><span lang="en-GB">We reserve one&rsquo;s right to restrain you from usage and access of this website and worldofdelight.ch and to delete your user account without any previous message should we or Genusswelt AG have knowledge of a violation of the present terms and conditions regarding a purchase on worldofdelight.ch.</span>
        </p>
        <p><span lang="en-GB">Should you have problems with placing an ordern, please consult the FAQ.</span></p>
    </div>
    <li class="title_paragraph toggle">6. Presentation, availability, and delivery of goods</li>
    <div class="text_wrap">
        <p><span lang="en-GB">The products on offen on worldofdelight.ch (specification, colour, price, availability) generraly to not have a binding character. We and Genusswelt AG reserve one&rsquo;s right to change the products on offer or remove certain products completely from our offer. We and Genusswelt AG exclude any liability for typographical errors, incorrect or incomplete information regarding productdescrition.</span>
        </p>
        <p><span lang="en-GB">We are makeing an effort to ensure the best possible availability of the on worldofdelight.ch offered goods. The presentation of vertain items on worldofdelight.ch does not mean that we guarantee its availability. Genusswelt AG is only obligated to deliver goodsd that ar in stock.</span>
        </p>

        <p><span lang="en-GB">Geusswelt AG may not fully fulfill your order should ordered items not be available. Worldofdelight.ch reserves one&rsquo;s right to cancel and order or only partly fulfill an order. In that case worldofdelight.ch will inform you.</span>
        </p>
        <p><span lang="en-GB">Delivery is carried out by courier to the shipping address provided by you during the ordering process and is limited to Switzerland.</span>
        </p>
        <p><span lang="en-GB">Generally, the delivery oft he orders which have been accepted from worldofdelight.ch take place at address given on the orderconfirmation. Should you not be available during the time of delivery your shipment, if possible, is placed in your mailbox. If your shipment does not allow this due to its size, the courier will leave a message regarding the following procedure. Shimpents with three unsuccessful deliveries are returned to worldofdelight.ch. After charging the delivery cost (cash on delivery) the shipment may be delivered or after written advance notification picked up directly by worldofdelight.ch. The liability to pay the goods entered through the ordering process remains. If the goods are not picked up within six months or cannot be delivered by cash on delivery, they are returned into the stock of worldofdelight.ch. A refund of the already made payments does not have to be plead.</span>
        </p>
        <p><span lang="en-GB">Shipping costs of CHF 7.- are remitted in Switzerland from an order value of CHF 50.- (calculated without consideration of worldofdelight.ch-rebate but including gift vouchers) or more (excl. VAT) depending on product category. The free delivery only applies to the regulary delievery within 3 workdays. If you use vouchers or codes from advertisings it will not be considered when calculating the above mentioned minimum amount.</span>
        </p>
        <p><span lang="en-GB">The delivery deadlines are nonbinding and may differ. The same applies for the time of delivery. Violation of the scheduled delivery date do not justify claims for damages. It is not possible to set a specific day or time for the delivery of the goods. Liability claims regarding delivery schedule are excluded.</span>
        </p>
    </div>
    <li class="title_paragraph toggle">7. Reselling</li>
    <div class="text_wrap">
        <p>Reselling or dissemination of goods bought on worldofdelight.ch to third parties is strictly
            prohibited.</p>
    </div>
    <li class="title_paragraph toggle">8. Prices</li>
    <div class="text_wrap">
        <p>All prices displayed on worldofdelight.ch are including the relevant VAT. The prices shown on
            worldofdelight.ch during the time of purchase apply.
            Should the vlue of your delivery not be within the free delivery amount (see previous section), CHF 7.-
            postage is charged to you bill.
        </p>
    </div>
    <li class="title_paragraph toggle">9. Payment</li>
    <div class="text_wrap">
        <p><span lang="en-GB">You may conduct your payment by credit card, debit card, post card, or invoice (factorage through our partner MF Group). The appropriate amount will be charged to your account as soon as your order is shipped. As soon as der shipping of your order is in handling and the ordered goods are handed to the courier, all refunds become impossible.</span>
        </p>

        <p><span lang="en-GB">To secure your personal data we use the encryption technique Secure Socket Layer (SSL) as well als further certified encryption technologies and systems.</span>
        </p>
        <p><span lang="en-GB">After your account is established you have the possibility during your online shoppen to decide all the time if you would like to save your credit card information for future transactions or if you want to delete them. If you wish to delete them you must reenter you credit card information every time you make a transaction.</span>
        </p>
        <p><span lang="en-GB">Please consider that the payment by credit card, depending on your credit card supplier and contract, may be charged extra by said company and further costs and feed may apply.</span>
        </p>
        <p><span lang="en-GB">Worldofdelight.ch reserves one&rsquo;s right to exclude acess to the website, access to worldofdelight.ch, and deleting your user account in case of past due payment oder lacking financial strength.</span>
        </p>
        <p><span lang="en-GB">Payement by PowerPay invoice</span></p>
        <p><span lang="en-GB">Thanks to the PowerPay invoice by MF Group AG you can pay your shopping easily by invoice. It is an absolute secure and easy to use means of payment. Payment by month is only available via www.worldofdelight.ch. Acceptance of the sales terms of www.worldofdelight.ch includes the prvious acceptance of the terms and conditions of MF Group AG for usage of PowerPay Invoice https://www.mfgroup.ch/agb/.</span>
        </p>
        <p><span lang="en-GB">Acceptance of your payment by PowerPay monthly invoice occure in step 3 &laquo;payment&raquo;
            of the checkout process and results from MF Group AG. Schould you payment be declied may only MF Group AG share their reasons with you.</span>
        </p>
        <p><span lang="en-GB">http://www.mfgroup.ch/orderdenied</span></p>
        <p><span lang="en-GB">Settling open payments is excluded.</span></p>
    </div>
    <li class="title_paragraph toggle">10. Theft and misuse of delivered goods</li>
    <div class="text_wrap">
        <p>As soon as the goods are delivered respectively placed in their mailbox any liability in case of theft or
            misuse is declined. It is incumbent upon the buyer to ensure that the foods are not sold to miners of
            the age of unter 18 or are used by you.
        </p>
    </div>
    <li class="title_paragraph toggle">11. Caveat emptor</li>
    <div class="text_wrap">
        <p><span lang="en-GB">This Website and worldofdelight.ch as well as its contents are &laquo;as you are&raquo;
            and &laquo;as available&raquo; provided. All information regarding goods on worldofdelight.ch are nonbinding and do not acquire assurance.</span>
        </p>

        <p><span lang="en-GB">We decline in the biggest way possible, and through the within applicable law acceptable extent, the liability or guarantee regarding the proper functioning of the website and worldofdelight.ch, the possibility to call up and use the website and worldofdelight.ch, the integrity, accuracy, and up-to-dateness of the information or data on the website and on worldofdelight.ch, on the absence of defect on the website respectively worldofdelight.ch or its content, and especially in regard of the absence of viruses or other malware, and in regard of absence of special characteristics which may be in connection with or could be expected from this website, worldofdelight.ch, or their content.</span>
        </p>
        <p><span lang="en-GB">You call up, make demands on, or download content from this website or worldofdelight.ch it happens at your own risk. Worldofdelight.ch is not liable for loss of data or damage to your computersystem that happen due to the usage of this Website or woeldofdelight.ch.</span>
        </p>
        <p><span lang="en-GB">If your shipment is damaged at delivery you can bring it to your courier location for purpose of free return. Herefore you will have to fill out a return note at the location of the courier.</span>
        </p>
        <p><span lang="en-GB">Worldofdelight.ch reserves one&rsquo;s right to adapt, change, shorten, complete, and/or remove completely from the new withouth providing the existing version of this website, worldofdelight.ch, or their contents.</span>
        </p>
    </div>
    <li class="title_paragraph toggle">12. Liability exclusion / indemnification</li>
    <div class="text_wrap">
        <p><span lang="en-GB">Worldofdelight.ch declines in the biggest way possible, and through the within applicable law acceptable extent, the liability or guarantee regarding possible damage, including loss of profit, loss of data, interruption of business procedures, or damage in any way that is in connected to the usage of this website, of worldofdelight.ch respectively with their contents could arise, especially in connection with the usage of supplied documents and/or applications, or through impossibility of usage of those documents or applications, the participation in possibly offered competitions or advertising activities and/or the purchase or shipment of items or goods in any way in connection with this website or worldofdelight.ch could result in. Especially liability is declined for possible damage, including loss of profit, loss of data, or damage of any kind that arised due to an incomplete or delayed delivery, defects concerning the goods, theft, or other occurances regarding the delivery of the goods.</span>
        </p>
        <p><span lang="en-GB">It is your responsibility in case of changes of address to update your data. Worldofdeligth.ch disclaims liability for problems that arise due to wrong address data.</span>
        </p>
        <p><span lang="en-GB">Worldofdelight.ch disclaims liability for action caused by vicarious agents as of article 101 of the Swiss Code of Obligations.</span>
        </p>
        <p><span lang="en-GB">Furthermore, worldofdelight.ch disclaims in the biggest way possible, and through the within applicable law acceptable extent, the liability for possible damage in connection with the use of the products offered and bought on worldofdelight.ch.</span>
        </p>

        <p><span lang="en-GB">This website or worldofdelight.ch may contain links to other websites. Those websites are not necessarily maintained and supervised by us. Worldofdeligt.ch declines in the biggest way possible, and through the within applicable law acceptable extent, the liability for possible damage regarding the access and the usage of these linked websites.</span>
        </p>
        <p><span lang="en-GB">You are bound to reimburse worldofdelight.ch towards any liability claim that are connected to damage in regard of illegal usage of the website and of worldofdelight.ch as well as the illegal purchase of goods on worldofdelight.ch including the illegal sale of these good ordered from you.</span>
        </p>
    </div>
    <li class="title_paragraph toggle">13. Intangible property rights</li>
    <div class="text_wrap">
        <p><span lang="en-GB">Usage of this website and worldofdelight.ch does not mean that you acquire licensing rights in regard of intellectual property of content of this website and worldofdelight.ch.</span>
        </p>
        <p><span lang="en-GB">This website, worldofdelight.ch as well as all their contents (especially software, data, designs, graphics, and files) are and remain property of worldofdelight.ch and are protected by the equivalent laws regarding intellectual property rights including copy right and brand rights. Any unauthorized usage of this website respectively worldofdelight.ch, especially usage of this website, worldofdelight.ch or its contents for professional or commercial reasons of any kind as well as the reproduction, displaying, transfer to others, message, passing, distribution, changing, licensing, the sell, or any other using of this website or of worldofdelight.ch or their content, texts, text parts, static or animated graphics, audio files, software, goods, or services as well as other data or information is without our previous written approval explicitly forbidden. Forbidden are especially methods of framing and inline linking of the website and wordofdelight.ch contents.</span>
        </p>
    </div>
    <li class="title_paragraph toggle">14. Privacy protection</li>
    <div class="text_wrap">
        <p>Information regarding collection and processing of your personal data which you provide throughout the
            usage of our website and worldofdelight.ch as well as through your purchases can be found in our privacy
            policy (Para. 18)
        </p>
    </div>
    <li class="title_paragraph toggle">15. Contact</li>
    <div class="text_wrap">
        <p>Any questions about the terms and conditions respectively our privacy policy is referred written to the
            following address: Genusswelt AG – World of Delight, Bahnhofstrasse 28, 6300 Zug. Any questions
            regarding weorldofdelight.ch and your purchases on worldofdelight.ch are addressed via e-mail to
            kontakt(at)genusswelt.ch.
        </p>
    </div>
    <li class="title_paragraph toggle">16. Severability clause</li>
    <div class="text_wrap">
        <p>If regulations of these terms and conditions, now or in the future, in parts or fully, should nullify or
            if these terms and conditions have legal loopholes, the rest of these regulations are unaffected and
            remain in effect. The applicable legal regulations enter into force at those invalid regulations of
            these terms and conditions.
        </p>
    </div>
    <li class="title_paragraph toggle">17. Applicable law and place of jurisdiction</li>
    <div class="text_wrap">
        <p><span lang="en-GB">The terms and conditions at hand and especially the contractual relationship between you and worldofdelight.ch is liable to Swiss law, except for the United Nations Convention on Contracts for the International Sale of Goods.</span>
        </p>
        <p><span
                lang="en-GB">Place of jurisdiction is place of residence of the client which must be in Switzerland.</span>
        </p>
        <p><span lang="en-GB">Pf&auml;ffikon, 2014.</span></p>
    </div>
    <li class="title_paragraph toggle">18. Privacy policy</li>
    <div class="text_wrap">
        <p><span lang="en-GB">The information at hand regarding privacy policy and handling of personal data apply only to this website. This does not include websites that are linked to this website. Links to other websites are marked as such. Please consider that wo do not have control over other websites. That is why this privacy policy does not apply to other websites. We recommend you read the privacy policy of the websites visited by you.</span>
        </p>
        <p><span lang="en-GB">Personal data</span></p>
        <p><span lang="en-GB">Genusswelt AG collects and uses personal data only according to the regulations of the Swiss Federal data privacy act. Personal data includes information about your identity such as name, address, e-mail-address, phone number, or date of birth. These informations are only collected and saved when you actively send them to us.</span>
        </p>
        <p><span lang="en-GB">In the regulations of this privacy policy Genusswelt AG clarifies which of your data is collected, processed, and used. Please take your time to read this privacy policy carefully before you make use of this website or send you personal data. By registration on this website you accept the regulations of the privacy policy at hand.</span>
        </p>
        <p><span lang="en-GB">Active collection and processing of data</span></p>
        <p><span lang="en-GB">We do not, without your previous permission, capture data actively about you on this website if an agreement of yours is necessary according to the Swiss Federal privacy law. It is your decision if you want to submit your personal data to us or not. Furthermore, Genusswelt AG collects and processes actively data about you only if you submit them voluntarily (for example regarding your agreement to receive messages [by e-mail or courier]).</span>
        </p>
        <p><span lang="en-GB">Passive collection of data</span></p>
        <p><span lang="en-GB">During your stay on our website data might be collected in a passive manner (viz. data is collected even if you do not actively provide them). A range of technologies and methods is used, for example internet protocol addresses, cookies, internet tags, navigational data; Google Analytics.</span>
        </p>
        <p><span lang="en-GB">So-called &laquo;navigational data&raquo; (protocol data, server logs, and clickstream data) are used for systems administration, optimizing of website content, market research, and for sending information to the website user. Data concerning the person are deleted immediately.</span>
        </p>

        <p><span lang="en-GB">We use internet protocol addresses (IP) on our website. A IP-address is a number assigned by your internet provider to your computer which enables you to use the internet. By visiting a website data is saved automatically. It is possible, that indirectly data regarding your person is collected. Thereby it is also common that this data is collected when you visit websites of other providers. This includes for example information about the browser you are using, the visited websites, or the amount of time you spent on those websites. You will be deleted shortly after, if it is possible to assign this that to specific persona. We especially use this information to check if you are currently calling up our website from within Switzerland. Furthermore, this data is collected anonymously to improve our website for our users or to provide security and stability for them.</span>
        </p>
        <p><span lang="en-GB">On this website data for marketing usage is collected and used. Furthermore, this data is used to improve the content on our website. This data is not assigned to a specific user though. You therefore may search our website anonymously. We use, for example, Google Analytics, a web analytics service by Google Inc. that collects information about your user experience and uses cookies for that.</span>
        </p>
        <p><span lang="en-GB">A cookie is a small text file that is sent from the website you call up to your browser and serves to collect data about you and your preferences. &laquo;session cookies&raquo;
            are small temporary text files that are deleted as soon as you close your browser window or shut down your computer. Session cookies are used to optimize the navigation used on the website and to collect additional statistical data.</span>
        </p>
        <p><span lang="en-GB">&laquo;Permanent cookies&raquo; are small temporary text files that are deleted as soon as you close your browser window or shut down your computer. Permanent cookies save information on your computer for various uses. For example, to collect specific information provided by you (address data). Furthermore, permanent cookies help finding out which websites are of interest for the specific user. This allows us to adapt the website to the specific requirements of the user.</span>
        </p>
        <p><span lang="en-GB">Cookies are no danger to your computer. If you still don&rsquo;t want cookies to be saved on your computer you may adjust your computer to show a message which announces the installation of cookies.</span>
        </p>
        <p><span lang="en-GB">How do I deactivate cookies?</span></p>
        <p><span lang="en-GB">If you want to deactivate cookies you must change the settings in your browser so that cookies are no longer accepted. How to change these settings depends on the browser you are using. Subsequent we explain how you can deactivate cookies on the most used browsers.</span>
        </p>
        <p><span lang="en-GB">Microsoft Internet Explorer:</span></p>
        <p><span lang="en-GB">In the menu choose &laquo;extras&raquo;
            and the option &laquo;internet options&raquo;</span>
        </p>
        <p><span lang="en-GB">Click on the tab &laquo;privacy&raquo;</span></p>
        <p><span lang="en-GB">In &laquo;advanced&raquo; you may choose your preferred settings.</span></p>

        <p><span lang="en-GB">Mozilla Firefox:</span></p>
        <p><span lang="en-GB">In the menu choose &laquo;extras&raquo; and the option &laquo;settings&raquo;</span></p>
        <p><span lang="en-GB">Click on the tab &laquo;privacy&raquo;</span></p>
        <p><span lang="en-GB">There click on the entry &laquo;delete singe cookies&raquo;
            and choose your preferred options</span></p>
        <p><span lang="en-GB">Chrome</span></p>
        <p><span lang="en-GB">Call up the &laquo;settings&raquo; page</span></p>
        <p><span lang="en-GB">Click on &laquo;advanced settings&raquo;</span></p>
        <p><span lang="en-GB">In the are &laquo;privacy&raquo; choose the button &laquo;content settings&raquo;</span>
        </p>
        <p><span lang="en-GB">Choose the rubric &laquo;cookies&raquo; your preferred option</span></p>
        <p><span lang="en-GB">What happens when I deactivate cookies?</span></p>
        <p><span lang="en-GB">This depends on which cookies you deactivate. Generally, it is anticipated that that websites with deactivated cookies will not work properly. When you only deactivate cookies from third parties you can still shop on our website. When you deactivate all cookies, you cannot shop on our website.</span>
        </p>
        <p><span lang="en-GB">Passive collection of personality profile</span></p>
        <p><span lang="en-GB">By accepting these privacy policy, you comply with worldofdelight.ch&rsquo;s collection of information about your shopping in the online shop and usage for marketing analysis. Based on your shopping data analysis of your cart can be made that reflect your buying patter. This can lead to the creation of personality profiles. While collecting such buying patterns and for the use of analysis of consumption behaviour, personal data can be transferred from Switzerland to commissioned companies. Worldofdelight.ch transfers this task to Genusswelt AG. They, as well as all in the process involved companies, are bound to adhere the privacy policy and to adhere suitable measurements for data security.</span>
        </p>
        <p><span lang="en-GB">Usage and transferal of information</span></p>
        <p><span lang="en-GB">Weorldofdelight.ch and Genusswelt AG as well as involved partners use your personal data exclusively for the following purposes:</span>
        </p>
        <p><span lang="en-GB">Verification of your age;</span></p>
        <p><span lang="en-GB">Verification of the location of your computer and therefore verification if you are calling up our website from within Switzerland;</span>
        </p>
        <p><span lang="en-GB">Processing your order and providing the service offered on our website and information that you make demands on;</span>
        </p>
        <p><span lang="en-GB">Administration of your user account;</span></p>
        <p><span
                lang="en-GB">Observation and execution of financial transaction regarding payments that you make online;</span>
        </p>
        <p><span lang="en-GB">Observation of downloading of data from our website;</span></p>

        <p><span lang="en-GB">Improvement of the layout respectively the content on the pages of our website and their user-friendly adjustment;</span>
        </p>
        <p><span lang="en-GB">Identification of users on our website;</span></p>
        <p><span lang="en-GB">Answering your inquiries;</span></p>
        <p><span lang="en-GB">Sending of information relating to your inquiries;</span></p>
        <p><span lang="en-GB">Dependent on your approval during the registration &ndash; while registration you may click a separate checkbox that accept sending of information and messages by e-mail or courier &ndash;
            we may send you further information about</span></p>
        <p><span lang="en-GB">This website</span></p>
        <p><span lang="en-GB">Our other websites, viz. our specific Genusswelt AG brands websites;</span></p>
        <p><span lang="en-GB">Our products</span></p>
        <p><span lang="en-GB">Sales promotion and sponsoring activities of Genusswelt AG</span></p>
        <p><span lang="en-GB">Other companies connected to Genusswelt AG or our partners including information about their products and services, sales promotions, and so forth.</span>
        </p>
        <p><span lang="en-GB">If you would like to retrieve your agreement later, you can click in one of the e-mail you receive from us the link &laquo;unsubscribe&raquo;
            or call up your user account to change your preferences.</span></p>
        <p><span lang="en-GB">Collecting save information about the top location of a single user and usage of specific aspects of the website respectively sending of a link in an email to a registered receiver as well as supplying of collected anonymized data to third parties, for example publisher, such anonymized data may cannot be used to identify you personally.</span>
        </p>
        <p><span lang="en-GB">Your personal data is only then given to third parties if such a transferral is needed to process your order, viz. for delivering your ordered goods or execution of payments or to provide other services requested by you, including among other things information services respectively messages (via e-mail or courier) that require this. Since the online shop from World of Delight is practiced by Genusswelt AG, we transfer the personal data necessary to this company. Genusswelt AG is furthermore authorized to contract third parties with the provision of the services you demanded.</span>
        </p>
        <p><span lang="en-GB">Contracted companies to which your personal data is transferred to are restricted to use this personal data only to provide the service demanded by you. We only transfer such personal data that is necessary to provide the service.</span>
        </p>

        <p><span lang="en-GB">Please consider that for processing of your payment by invoice, credit or debit card the terms and conditions of those institutes apply. Based on their terms and conditions you comply with transferral of the personal data that your credit or debit card institution needs to charge or credit your account is given to your credit or debit card institution and companies contracted by them when you choose to use your credit or debit card for payments. Further information regarding the processing of personal data regarding a credit or debit card payment can be found in the terms and conditions of your credit or debit card institute.</span>
        </p>
        <p><span lang="en-GB">It may occur that worldofdelight.ch, Genusswelt AG, and contracted companies are forced, based on legal regulations, court order, or order of cognisant authority to hand out personal information. In such a case, you will be informed previously about such an issue if it is allowed by applicable law.</span>
        </p>
        <p><span lang="en-GB">Access on your personal data and if applicable correction.</span></p>
        <p><span lang="en-GB">Please contact us via kontakt(at)worldofdelight.ch to make sure that your personal data is correct, up to date and complete. We will induce that the previously through the website sent personal data is actualised respectively corrected.</span>
        </p>
        <p><span lang="en-GB">Security of your data</span></p>
        <p><span lang="en-GB">We take every necessary measure to protect personal data that is transferred from your computer to our website. Especially we protect lost, misuse, unauthorized access, uncalled-for disclosure, manipulation, deleting, or annihilation. Please consider that still transferring data throughout the internet is not completely save or free of mistakes.</span>
        </p>
        <p><span lang="en-GB">If you send your personal data through our website, our system will arrange a save internet connection (SSL). Especially e-mails that are sent to this website or from this website are maybe not save. Please consider what information you sent to us by e-mail. It is your responsibility to keep your password, user ID, and other special authorization systems save.</span>
        </p>
        <p><span lang="en-GB">Other conditions</span></p>
        <p><span lang="en-GB">Usage of this website is subject to our terms and conditions.</span></p>
        <p><span lang="en-GB">Contact:</span></p>
        <p><span lang="en-GB">If you have questions, comments, or doubt regarding privacy policy or the used practises on this website please contact</span>
        </p>
        <p><span lang="en-GB">Kontakt(at)worldofdelight.ch</span></p>
        <p><span lang="en-GB">Changes of the terms and conditions at hand</span></p>

        <p><span lang="en-GB">Please consider that we, corresponding to our service and requirement, considering possible changes in data protection law, make changes to our privacy policy from time to time. Therefore, we reserve one&rsquo;s right to make changes to this privacy policy without notice in advance and in one&rsquo;s sole discretion. Please consult our website regularly for your information. It is your responsibility to check this website regularly und inform about eventual changes, especially before you send us your data.</span>
        </p></div>
</ul>';

    }

    private function getContentDe()
    {
        return '<p>
Diese Webseite wird dir gemäss den vorliegenden Allgemeinen Geschäftsbedingungen zur Verfügung gestellt (nachfolgend die
«Allgemeinen Geschäftsbedingungen»). Diese Website ist das Eigentum von worldofdelight.ch und die nachfolgend
verwendeten Begriffe "wir", "uns" / "unsere" beziehen sich auf Genusswelt. Die Abwicklung der Bestellungen im World of
Delight Online-Shop auf dieser Webseite (nachfolgend "woroldofdelight.ch") erfolgt durch Genusswelt AG, Bahnhofstrasse
28, 6300 Zug, E-Mail-Adresse: kontakt(at)genusswelt.ch).</p>
<ul class="data_inner">
    <li class="title_paragraph toggle">1. Geltungsbereich</li>
    <div class="text_wrap">
        <p>Diese Allgemeinen Geschäftsbedingungen gelten für die Benutzung dieser Website und des Worldofdelight.ch
            Online-Shops auf dieser Website sowie für Einkäufe auf worldofdelight.ch. Mit deiner Registrierung auf
            dieser
            Website und deren Benutzung sowie der Benutzung von worldofdelight.ch akzeptierst du diese Allgemeinen
            Geschäftsbedingungen.
        </p>
        <p>
            Deine Einkäufe auf worldofdelight.ch und damit die Kaufverträge unterliegen ausschliesslich diesen
            Allgemeinen
            Geschäftsbedingungen.
        </p>
        <p>
            Darüber hinaus gelten diese Allgemeinen Geschäftsbedingungen für die auf dieser Website dargestellten oder
            unter
            der Marke worldofdelight.ch durchgeführten Werbeaktionen, insbesondere für Wettbewerbe und Verlosungen, es
            sei
            denn, es werden dir für solche Aktivitäten ausdrücklich andere besondere Geschäftsbedingungen mitgeteilt.
        </p>
        <p>
            Wir behalten uns das Recht vor, diese Allgemeinen Geschäftsbedingungen jederzeit zu ergänzen und abzuändern.
            Zur
            Anwendung kommen diejenigen Allgemeinen Geschäftsbedingungen, die zum Zeitpunkt deines Besuchs und deiner
            Benutzung der Website oder zum Zeitpunkt deines Einkaufs auf worldofdelight.ch gelten. Du bist dafür
            verantwortlich, die Allgemeinen Geschäftsbedingungen regelmässig auf Änderungen zu überprüfen.
        </p>
        <p>
            Für den Fall inhaltlicher Diskrepanzen zwischen der deutschen, französischen, italienischen oder englischen
            Version dieser Allgemeinen Geschäftsbedingungen gilt die deutsche Version.
        </p>
    </div>
    <li class="title_paragraph toggle">2. Zugang zur Website und Passwort</li>
    <div class="text_wrap">
        <p>Diese Website sowie der Online-Shop worldofdelight.ch richten sich ausschliesslich an Raucher über 18 Jahre
            mit
            Wohnsitz in der Schweiz, die Tabakwaren für den persönlichen Gebrauch einkaufen möchten. Um Zugang zu dieser
            Website und zu worldofdelight.ch zu erhalten, musst du dein Alter nachweisen. Weiterhin wird der Standort
            des
            Computers, von dem aus du auf die Website und worldofdelight.ch zugreifst, durch die Erfassung der
            IP-Adresse
            deines Computers überprüft. Der Zugang zur Website ist ausschliesslich von einem Standort in der Schweiz aus
            möglich. Darüber hinaus musst du dich anmelden und einen Benutzernamen und ein Passwort auswählen, die
            ausschliesslich für die Benutzung durch dich selbst bestimmt sind. Diese der Sicherheit dienenden
            Zugangsdaten
            dürfen von dir nicht an Dritte weitergegeben werden. Diese Massnahme hat zum Ziel, die Benutzung der Website
            durch unbefugte Dritte zu verhindern. Du bist als Benutzer dieser Website und von worldofdelight.ch
            verpflichtet, zum Schutz der Geheimhaltung deines Benutzernamens und Passworts alle erforderlichen
            Massnahmen zu
            ergreifen.
            <br/>
            Du bist gehalten, uns unverzüglich zu informieren, wenn du Anlass hast zu der Annahme, dass jemand deinen
            Benutzernamen oder dein Passwort herausgefunden haben könnte oder dass die Gefahr bestehen könnte, dass dein
            Benutzernamen und dein Passwort ohne Zustimmung verwendet werden könnten. Du bist verantwortlich für
            sämtliche
            Handlungen, die unter Verwendung deines Benutzernamens und deines Passworts erfolgen. Wir lehnen jede
            Verantwortung ab für einen Schaden, der mit der Weitergabe deines Benutzernamens und deines Passworts an
            Dritte
            entsteht.
        </p>
        <p>
            Die persönlichen Daten, die wir im Rahmen des Registrierungsprozesses von dir abfragen, müssen der Wahrheit
            entsprechen und zutreffend sein.
        </p>
        <p>
            Dein Benutzerkonto erlaubt die Verwendung dieser Website und den Zugang zu worldofdelight.ch nur für deine
            eigenen privaten, nicht kommerziellen Zwecke.
        </p>
        <p>
            Wir behalten uns das Recht vor, deine Registrierung jederzeit und ohne dich vorher zu informieren zu löschen
            und
            dein Benutzerkonto zu schliessen, falls du gegen deine in diesen Allgemeinen Geschäftsbedingungen
            aufgeführten
            Pflichten verstösst.
        </p>
    </div>
    <li class="title_paragraph toggle">3. Benutzung dieser Website und worldofdelight.ch</li>
    <div class="text_wrap">
        <p>Du bist persönlich verantwortlich für alle Zugriffe auf diese Website und auf worldofdelight.ch, die über
            deine
            Internet-Verbindung erfolgen. Es ist dir nicht erlaubt, dein Passwort an Dritte weiterzugeben, um diesen den
            Zugang zur Website als registrierter Benutzer zu ermöglichen.
        </p>
        <p>
            Es ist dir weiterhin nicht erlaubt, diese Website oder worldofdelight.ch dazu zu benutzen, uns, dieser
            Website,
            anderen Benutzern dieser Website oder worldofdelight.ch Inhalte zu senden, die in irgendeiner Weise:
        <ul class="list_agb">

            <li> gegen Recht, Gesetze, Vorschriften oder Richtlinien anwendbarer Rechtsordnungen verstossen;
            </li>

            <li>
                betrügerischer, krimineller oder gesetzeswidriger Natur sind;
            </li>

            <li>
                unzutreffend oder veraltet sind;
            </li>

            <li> obszöner, anstössiger, pornografischer, vulgärer, gotteslästerlicher, rassistischer, sexistischer,
                diskriminierender, beleidigender, herabwürdigender, schädlicher, belästigender, bedrohender,
                ärgerlicher,
                böswilliger, missbräuchlicher, hasserfüllter, bedrohlicher, diffamierender, unwahrer oder politischer
                Art
                sind;
            </li>

            <li>
                sich als eine andere Person oder Institution ausgeben oder fälschlicherweise eine Beziehung zu einer
                anderen
                Person oder Institution vorgeben;
            </li>
            <li> die Urheberrechte oder anderer Immaterialgüterrechte (einschliesslich unter anderem Copyright-Rechte,
                Marken-
                und Senderechte) oder den Datenschutz oder andere Rechte unseres Unternehmens oder eines Dritten
                verletzen
                könnten;
            </li>
            <li>unseren ureigenen Interessen und den Interessen von worldofdelight.ch zuwiderlaufen könnten;</li>
            <li> bestimmten Regeln oder Anforderungen, die wir auf dieser Website in Bezug auf bestimmte Teile der
                Website,
                von
                worldofdelight.ch oder dieser Website im Allgemeinen festlegen, zuwiderlaufen;
            </li>
            <li>oder die im Zusammenhang stehen mit dem Einsatz, der Herbeiführung oder der Übertragung von Viren,
                unerwünschten
                E-Mails, Trojanern, sog. Falltüren (Trap Doors), Backdoors, sog. Easter Eggs, Würmern, Zeitbomben,
                Cancelbots
                oder Computerprogrammierroutinen durch deine Person, welche die Absicht verfolgen, ein System, Daten
                oder
                persönliche Informationen zu schädigen, in schädlicher Weise zu manipulieren, heimlich auszuspionieren
                oder
                Bestandteile derselben zu entwenden.
            </li>
        </ul>
    </div>
    <li class="title_paragraph toggle">4. Bedingungen für Wettbewerbe auf worldofdelight.ch</li>
    <div class="text_wrap">
        <p>Die auf worldofdelight.ch organisierten Wettbewerbe richten sich ausschliesslich an Raucher über 18 Jahre mit
            Wohnsitz in der Schweiz. An diesen Wettbewerben dürfen nur Personen teilnehmen, die auf worldofdelight.ch
            registriert sind.
        </p>
        <p>
            Ausgeschlossen von einer Teilnahme an solchen Wettbewerben sind die Mitarbeitenden von Worldofdelight.ch,
            deren
            Partner und Dienstleistern sowie die Familienmitglieder dieser Mitarbeitenden.
        </p>
        <p>
            Die Teilnahme an einem Wettbewerb auf worldofdelight.ch setzt deine Einverständniserklärung sowohl mit den
            Allgemeinen Geschäftsbedingungen von worldofdelight.ch als auch mit den besonderen Geschäftsbedingungen des
            jeweiligen Wettbewerbs voraus.
        </p>
        <p>
            Weiterhin behalten wir uns jederzeit das Recht vor, nach unserem eigenen Ermessen einen Wettbewerb zu
            beenden.
            Die Einstellung eines solchen Wettbewerbs ist mit keinerlei Recht auf irgendeine Art von Entschädigung
            verbunden. Es werden keinerlei Rechte erworben und es können keine Leistungen verlangt werden.</p>
        <p>Dar&uuml;ber hinaus &uuml;bernehmen wir keinerlei Haftung, falls in den Formularen eingegebene Daten oder
            E-Mails von Teilnehmern vom System nicht erkannt werden bzw. falls aufgrund eines technischen oder sonstigen
            Defekts eine Person daran gehindert wird, an einem Wettbewerb teilzunehmen.</p>
        <p>Wir behalten uns auch vor, Teilnehmer von Wettbewerben auszuschliessen. Dies gilt insbesondere f&uuml;r
            Teilnehmer, die t&auml;uschen oder versuchen zu t&auml;uschen (Erstellen mehrerer Teilnehmerkonten), oder f&uuml;r
            Teilnehmer, die gegen die Allgemeinen oder gegen die besonderen Gesch&auml;ftsbedingungen eines Wettbewerbs
            verstossen oder die in irgendeiner Weise die angebotenen Wettbewerbe missbr&auml;uchlich benutzen.</p>
        <p>Im &Uuml;brigen gelten s&auml;mtliche &uuml;brigen Bestimmungen dieser Allgemeinen Gesch&auml;ftsbedingungen,
            insbesondere diejenigen hinsichtlich des Zugriffs auf die Website, hinsichtlich der Benutzungsbestimmungen,
            des Garantie- und Haftungsausschlusses sowie des Datenschutzes.</p>
        </p>
    </div>
    <li class="title_paragraph toggle">5. Abschluss eines Kaufvertrags auf worldofdelight.ch</li>
    <div class="text_wrap">
        <p>Wir haben Genusswelt AG mit dem Verkauf von Waren beauftragt, die &uuml;ber worldofdelight.ch bestellt
            werden. Wenn du auf worldofdelight.ch eine Bestellung aufgibst, kaufst du daher die bestellte Ware bei
            Genusswelt AG. Weiterhin ist Genusswelt AG verantwortlich f&uuml;r die Zusammenstellung, Lieferung und
            Rechnungstellung deiner Bestellungen.</p>
        <p>Sobald du mit Deinem Passwort Worldofdelight.ch aufrufst, hast du die M&ouml;glichkeit, auf worldofdelight.ch
            angebotene Waren auszuw&auml;hlen und in deinen virtuellen Warenkorb zu legen, indem du die Schaltfl&auml;che &laquo;In
            den Warenkorb&raquo; anklickst. Um den Einkauf fortzusetzen, musst du den Einkaufswagen am rechten Rand der
            Webseite &ouml;ffnen und dann den Button "Warenkorb ansehen" dr&uuml;cken. Es &ouml;ffnet sich eine neue
            Maske, die Informationen betreffend die Waren im Warenkorb enth&auml;lt. Dann kannst du den eigentlichen
            Bestellvorgang beginnen, indem du "Bestellen" dr&uuml;ckst. In der nachfolgenden Maske musst du die
            Lieferadresse eingeben. Du wirst sodann &uuml;ber den Button "Weiter" zum n&auml;chsten Schritt gef&uuml;hrt.
            Die folgenden Masken Fragen nach der Liefer- und Zahlungsmethode. Wenn du den letzten Schritt des
            Bestellvorganges erreichst und sobald du die zwingenden Zahlungsdetails eingegeben hast, musst du die
            Allgemeinen Gesch&auml;ftsbedingungen und die Datenschutzrichtlinien dieser Webseite akzeptieren. Nur wenn
            du alle notwendigen Informationen eingegeben hast und wenn du die Allgemeinen Gesch&auml;ftsbedingungen und
            die Datenschutzrichtlinien akzeptiert hast, kannst du die Bestellung durch Dr&uuml;cken des Buttons "Weiter"
            abschliessen. Es wird dann eine Bestellbest&auml;tigung erscheinen. Eine Bestellbest&auml;tigung erh&auml;ltst
            du auch per E-Mail. Wir empfehlen dir, deine Bestellung und die Allgemeinen Gesch&auml;ftsbedingungen
            auszudrucken.</p>
        <p>Deine Bestellung stellt eine Willensbekundung deinerseits zum Kauf von Waren dar. Nach Eingang deiner
            Bestellung erh&auml;ltst du von Genusswelt AG eine Bestellbest&auml;tigung zugesandt, mit welcher der
            Empfang deiner Bestellung notifiziert wird. Die Bestellbest&auml;tigung stellt keine Annahme deiner
            Bestellung dar. Die Bestellbest&auml;tigung enth&auml;lt s&auml;mtliche relevanten Informationen bez&uuml;glich
            deiner Bestellung. F&uuml;r den Fall, dass die Bestellbest&auml;tigung unzutreffende Informationen enth&auml;lt,
            bist du verpflichtet, Genusswelt AG unverz&uuml;glich zu benachrichtigen.</p>
        <p>Deine Bestellung gilt erst dann als best&auml;tigt und ein Kaufvertrag mit Genusswelt AG erst dann als
            zustande gekommen, wenn dir Genusswelt AG eine Versand-E-Mail zugesandt hat, in der dir der Versand deiner
            Bestellung best&auml;tigt wird.</p>
        <p>Genusswelt AG wird dich gegebenenfalls in Kenntnis setzen, falls sie entscheidet, deine Bestellung nicht
            anzunehmen.</p>
        <p>Sollten wir oder Genusswelt AG Kenntnis &uuml;ber einen Verstoss gegen die vorliegenden Allgemeinen Gesch&auml;ftsbedingungen
            im Zusammenhang mit einem Einkauf auf worldofdelight.ch erhalten, behalten wir uns das Recht vor, dich von
            der Benutzung und dem Zugriff auf diese Website und worldofdelight.ch auszuschliessen und dein Benutzerkonto
            ohne vorherige Benachrichtigung zu l&ouml;schen.</p>
        <p>Solltest du bei der Aufgabe deiner Bestellung Probleme haben, konsultiere bitte die FAQ.</p>
    </div>
    <li class="title_paragraph toggle">6. Präsentation, Verfügbarkeit und Lieferung der Waren</li>
    <div class="text_wrap">
        <p>Das auf worldofdelight.ch dargestellte Warenangebot (Spezifikation, Farben, Preise, Verf&uuml;gbarkeit) hat
            im Allgemeinen unverbindlichen Charakter. Wir und Genusswelt AG behalten uns das Recht vor, das Warenangebot
            jederzeit zu modifizieren und bestimmte Produkte vollst&auml;ndig aus dem Angebot zu nehmen. Wir und
            Genusswelt AG schliessen jede Haftung f&uuml;r typografische Fehler, unrichtige oder unvollst&auml;ndige
            Angaben bez&uuml;glich Produktbeschreibungen aus.</p>
        <p>Wir bem&uuml;hen uns, die bestm&ouml;gliche Verf&uuml;gbarkeit der auf worldofdelight.ch angebotenen Waren
            sicherzustellen. Die Pr&auml;sentation einer bestimmten Ware auf worldofdelight.ch bedeutet nicht, dass wir
            eine Garantie bez&uuml;glich der Verf&uuml;gbarkeit dieser Ware &uuml;bernehmen. Genusswelt AG ist lediglich
            zur Lieferung vorr&auml;tiger Waren verpflichtet.</p>
        <p>Sollte bestellte Waren nicht verf&uuml;gbar sein, kann Genusswelt AG deine Bestellung m&ouml;glicherweise
            nicht vollst&auml;ndig durchf&uuml;hren. Worldofdelight.ch beh&auml;lt sich das Recht vor, deine Bestellung
            entweder zu stornieren oder lediglich eine Teillieferung durchzuf&uuml;hren. In einem solchen Fall wird dich
            worldofdelight.ch entsprechend in Kenntnis setzen.</p>
        <p>Die Warenlieferung erfolgt per Kurier an die von dir im Rahmen des Bestellvorganges angegebene Lieferadresse
            und ist auf die Schweiz beschr&auml;nkt.</p>
        <p>Im Allgemeinen erfolgt die Lieferung der Bestellungen, die von smoke24 akzeptiert werden, an die in der
            Eingangsbest&auml;tigung deiner Bestellung angegebene Adresse. Falls du zum Zeitpunkt der Lieferung abwesend
            sein solltest, wird deine Warensendung, sofern dies m&ouml;glich ist, in deinem Briefkasten oder in
            deiner &bdquo;Milchbox&ldquo; f&uuml;r dich hinterlegt. Falls die Gr&ouml;sse der Warensendung dies nicht
            zul&auml;sst, hinterl&auml;sst dir der Kurier eine Benachrichtigung &uuml;ber das weitere Vorgehen. Die nach
            dreimaligem Zustellen nicht &uuml;bergebenen Produkte werden an worldofdelight.ch zur&uuml;ckgesendet. Unter
            Verrechnung der Zustellkosten (Nachnahme) k&ouml;nnen die Waren zugestellt oder nach schriftlicher
            Voranmeldung, bei worldofdelight.ch direkt abgeholt werden. Die mit dem Kauf eingegangen Verpflichtungen zur
            Zahlung der Ware bleiben bestehen. Wird die Ware innerhalb von 6 Monaten nicht abgeholt oder kann diese
            nicht per Nachnahme zugestellt, geht die Ware zur&uuml;ck in den Bestand von worldofdelight.ch. Eine R&uuml;ckverg&uuml;tung
            der bereits geleisteten Zahlungen muss schriftlich geltend gemacht werden.</p>
        <p>In der Schweiz erlassen wir die CHF 7.- Versandkosten von ab einem Bestellwert ab CHF 50.- (berechnet ohne
            Ber&uuml;cksichtigung von worldofdelight.ch-Rabatten aber inkl. Geschenkgutscheinen) oder dar&uuml;ber (ohne
            MwSt.) je nach Produktegruppe. Die kostenlose Lieferung betrifft ausschliesslich die Standardlieferungen
            innerhalb von 3 Werktagen. Wenn du f&uuml;r deinen Einkauf Gutscheine oder Codes aus Werbeaktionen einl&ouml;st,
            wird die entsprechende Gutschrift bei der Berechnung des oben erw&auml;hnten Mindesteinkaufsbetrags nicht
            mitber&uuml;cksichtigt.</p>
        <p>Diese Versandfristen sind unverbindlich und k&ouml;nnen abweichen. Gleiches gilt f&uuml;r die Uhrzeit der
            Anlieferung. Die Nichteinhaltung von Lieferterminen begr&uuml;ndet keinerlei Schadensersatzanspr&uuml;che.
            Es ist nicht m&ouml;glich, f&uuml;r die Anlieferung der Waren einen bestimmten Tag oder eine bestimmte
            Uhrzeit festzulegen. Haftungsanspr&uuml;che bez&uuml;glich Lieferterminen sind ausgeschlossen.</p>
    </div>
    <li class="title_paragraph toggle">7. Weiterverkauf</li>
    <div class="text_wrap">
        <p>Der Weiterverkauf oder die Weitergabe der auf worldofdelight.ch gekauften Waren an Dritte ist strengstens
            untersagt.</p>
    </div>
    <li class="title_paragraph toggle">8. Preise</li>
    <div class="text_wrap">
        <p>Alle auf worldofdelight.ch angezeigten Preise umfassen die MwSt. entsprechend den geltenden Steuersätzen. Es
            gelten die zum Zeitpunkt deines Einkaufs auf worldofdelight.ch angezeigten Preise.
            Sollte deine Bestellung nicht unter die Bedingungen einer kostenlosen Lieferung fallen (siehe vorstehenden
            Abschnitt), werden pauschal CHF 7.00 Portogebühren belastet.
        </p>
    </div>
    <li class="title_paragraph toggle">9. Zahlungen</li>
    <div class="text_wrap">
        <p>Du kannst deine Zahlungen entweder per Kreditkarte, Debitkarte, PostCard oder Rechnung (Faktoring abwicklung
            via Partner MF Group) durchf&uuml;hren. Der entsprechende Betrag wird von deinem Konto abgebucht, wenn deine
            Bestellung versendet worden ist. Sobald der Versand deiner Bestellung in Bearbeitung ist und die bestellten
            Waren dem Kurier &uuml;bergeben wurden, sind R&uuml;ckerstattungen nicht mehr m&ouml;glich.</p>
        <p>Zum Schutz deiner pers&ouml;nlichen Daten verwenden wir die Verschl&uuml;sselungstechnik Secure Socket Layer
            (SSL) sowie weitere zertifizierte Verschl&uuml;sselungstechnologien und -systeme.</p>
        <p>Nachdem dein Konto eingerichtet worden ist, hast Du im Zuge deines Online-Einkaufs jederzeit die M&ouml;glichkeit
            zu entscheiden, ob du deine Kreditkarten-Angaben auch f&uuml;r k&uuml;nftige Transaktionen speichern m&ouml;chtest
            oder ob du deine Daten l&ouml;schen m&ouml;chtest. Im letzteren Fall musst du deine Kreditkarten-Angaben bei
            jeder Transaktion erneut eingeben.</p>
        <p>Bitte beachte, dass durch die Zahlung mit Kreditkarte je nach dem zwischen dir und dem Kreditkarteninstitut
            abgeschlossenen Kreditkartenvertrag zus&auml;tzliche Kosten und Geb&uuml;hren entstehen k&ouml;nnen.</p>
        <p>Worldofdelight.ch behaltet sich das Recht vor, dich bei Zahlungsverzug oder mangelnder Kreditkartendeckung
            von einer k&uuml;nftigen Nutzung der Website und einem k&uuml;nftigen Zugang zu Worldofdelight.ch
            auszuschliessen und dein Benutzerkonto zu l&ouml;schen.</p>
        <p>Zahlung per PowerPay Rechnung</p>
        <p>Dank der PowerPay Rechnung der MF Group AG kannst du Deine Eink&auml;ufe einfach per Rechnung bezahlen. Es
            handelt sich dabei um ein absolut sicheres und einfach anzuwendenes Zahlungsmittel. Die Bezahlung per
            Monatsrechnung ist nur &uuml;ber www.worldofdelight.ch m&ouml;glich. Die Annahme der Allgemeinen
            Verkaufsbedingungen &uuml;ber www.worldofdelight.ch beinhaltet die im Vorfeld erfolgte Annahme der
            Allgemeinen Gesch&auml;ftsbedingungen der MF Group AG f&uuml;r Nutzung der PowerPay Rechnung
            https://www.mfgroup.ch/agb/.</p>
        <p>Die Annahme Deiner Bezahlung per PowerPay Monatsrechnung erfolgt unter Schritt 3 - "Zahlung" - des
            Auftragsablaufs und erfolgt durch die MF Group AG. Sollte die Zahlung abgelehnt werden, kann Ihnen einzig
            und allein die Firma MF Group AG die Gr&uuml;nde daf&uuml;r mitteilen.</p>
        <p>http://www.mfgroup.ch/orderdenied</p>
        <p>Eine Verrechnung offener Zahlungen ist ausgeschlossen.</p>
    </div>
    <li class="title_paragraph toggle">10. Diebstahl und Missbrauch gelieferter Waren</li>
    <div class="text_wrap">
        <p>Sobald die Waren dem Käufer zugestellt bzw. in seinem Briefkasten oder in seiner Milchbox hinterlegt worden
            sind,
            lehnen wir im Fall von Diebstahl oder Missbrauch jede Verantwortung ab. Es obliegt dem Käufer, dafür zu
            sorgen,
            dass die Waren nicht an Minderjährige unter 18 Jahren verkauft oder von Dir verwendet werden.
        </p>
    </div>
    <li class="title_paragraph toggle">11. Gewährleistungsausschluss</li>
    <div class="text_wrap">
        <p>Diese Website und worldofdelight.ch sowie die entsprechenden Inhalte werden &laquo;so wie du bist&raquo;
            und &laquo;soweit verf&uuml;gbar&raquo; bereitgestellt. S&auml;mtliche die auf worldofdelight.ch angebotenen
            Waren betreffenden Informationen sind unverbindlich und stellen keine Zusicherungen dar.</p>
        <p>Wir lehnen in dem gr&ouml;sstm&ouml;glichen durch die anwendbaren Gesetze zul&auml;ssigen Ausmass jegliche
            Verantwortung oder Garantie ab in Bezug auf das ordnungsgem&auml;sse Funktionieren der Website und von
            worldofdelight.ch, auf die M&ouml;glichkeit, die Website und worldofdelight.ch aufzurufen und zu benutzen,
            auf die Vollst&auml;ndigkeit, Genauigkeit und Aktualit&auml;t der Inhalte, auf Informationen oder Daten auf
            der Website und auf worldofdelight.ch, auf die Abwesenheit von Defekten auf der Website bzw. auf
            worldofdelight.ch oder der dort vorhandenen Inhalte, und insbesondere in Bezug auf die Abwesenheit von Viren
            oder sonstiger Schadsoftware, und in Bezug auf das Nichtvorhandensein besonderer Eigenschaften, die m&ouml;glicherweise
            in Verbindung mit dieser Website, worldofdelight.ch oder deren Inhalten erwartet werden k&ouml;nnten. Wenn
            du diese Website oder worldofdelight.ch aufrufst, in Anspruch nimmst oder Inhalte von dort herunterl&auml;dst,
            so erfolgt dies auf deine eigene Gefahr. Wenn du daher in der Folge dieses Gebrauchs Datenverlust oder Sch&auml;den
            an deinem Computersystem erleidest, dann ist worldofdelight.ch hierf&uuml;r nicht verantwortlich.</p>
        <p>Sollte die Sendung bei Anlieferung besch&auml;digt sein, kannst du sie zum Zweck des kostenlosen
            Retourenversands zu deiner Kurierdienststelle zur&uuml;ckbringen. Hierzu musst du auf der Kurierdienststelle
            einen Retourenschein ausf&uuml;llen.</p>
        <p>Worldofdelight.ch beh&auml;lt sich das Recht vor, diese Website, worldofdelight.ch bzw. Inhalte derselben
            jederzeit nach eigenem Ermessen zu ver&auml;ndern, anzupassen, zu k&uuml;rzen, zu vervollst&auml;ndigen
            und/oder komplett aus dem Netz zu nehmen, ohne die bisherige Version weiter bereitzustellen.</p>
    </div>
    <li class="title_paragraph toggle">12. Haftungsausschluss / Schadloshaltung</li>
    <div class="text_wrap">
        <p>Worldofdelight.ch lehnt in dem gr&ouml;sstm&ouml;glichen durch die anwendbaren Gesetze zul&auml;ssigen
            Ausmass jegliche Haftung f&uuml;r m&ouml;gliche Sch&auml;den ab, einschliesslich f&uuml;r entgangene
            Gewinne, Datenverlust, Unterbrechung der Gesch&auml;ftst&auml;tigkeit oder Schaden jeglicher Art, die im
            Zusammenhang mit der Nutzung dieser Website, von worldofdelight.ch bzw. deren Inhalten entstehen k&ouml;nnten,
            insbesondere im Zusammenhang mit der Verwendung von dort bereitgestellten Dokumenten und/oder Anwendungen,
            oder die sich aus der Unm&ouml;glichkeit der Nutzung dieser Dokumente und Anwendungen, der Beteiligung an
            allf&auml;llig angebotenen Wettbewerben oder Werbeaktionen und/oder dem Kauf oder Versand von Artikeln oder
            Waren jeglicher Art in Verbindung mit dieser Website oder worldofdelight.ch ergeben k&ouml;nnten.
            Insbesondere wird keinerlei Haftung &uuml;bernommen f&uuml;r m&ouml;gliche Sch&auml;den, einschliesslich f&uuml;r
            entgangene Gewinne, Datenverlust oder Schaden jeglicher Art, die sich aus einer Lieferverhinderung oder
            einer versp&auml;teten Lieferung, die Waren betreffenden Defekten, Diebstahl oder anderen Ereignissen in
            Verbindung mit der Lieferung der Waren ergeben k&ouml;nnten.</p>
        <p>Es obliegt insbesondere deiner Verantwortung, im Fall von &Auml;nderungen deiner Adresse deine Angaben zu
            aktualisieren. Worldofdelight.ch schliesst jede Haftung f&uuml;r Probleme aus, die sich aus falschen
            Adressenangaben ergeben.</p>
        <p>Worldofdelight.ch schliesst weiterhin jegliche Haftung aus f&uuml;r Handlungen, die durch Erf&uuml;llungsgehilfen
            im Sinne des Art. 101 des Schweizerischen Obligationenrechts begangen werden.</p>
        <p>Dar&uuml;ber hinaus lehnt Worldofdelight.ch in dem gr&ouml;sstm&ouml;glichen durch die anwendbaren Gesetze
            zul&auml;ssigen Ausmass jegliche Haftung f&uuml;r m&ouml;gliche Sch&auml;den in Verbindung mit der
            Verwendung der auf worldofdelight.ch angebotenen und erworbenen Waren ab.</p>
        <p>Diese Website oder Worldofdelight.ch enthalten m&ouml;glicherweise Links zu anderen Websites. Die
            entsprechenden Websites werden nicht in jedem Fall von uns betrieben oder durch uns &uuml;berwacht.
            Worldofdelight.ch lehnt in dem gr&ouml;sstm&ouml;glichen durch die anwendbaren Gesetze zul&auml;ssigen
            Ausmass jegliche Haftung f&uuml;r m&ouml;gliche Sch&auml;den in Verbindung mit dem Zugang zu diesen und der
            Benutzung dieser verlinkten Websites ab.</p>
        <p>Du verpflichtest dich, somke24.ch gegen&uuml;ber s&auml;mtlichen Haftungsanspr&uuml;chen Dritter, die sich
            auf Sch&auml;den im Zusammenhang mit der gesetzeswidrigen Verwendung dieser Website und von
            Worldofdelight.ch durch dich sowie dem gesetzeswidrigen Kauf von Waren auf worldofdelight.ch einschliesslich
            dem gesetzeswidrigen Weiterverkauf von Waren durch dich beziehen, schadlos zu halten.</p>
    </div>
    <li class="title_paragraph toggle">13. Immaterialgüterrechte</li>
    <div class="text_wrap">
        <p>Die Verwendung dieser Website und von worldofdelight.ch bedeutet nicht, dass dir Lizenzrechte in Bezug auf
            die
            geistigen Eigentumsrechte am Inhalt dieser Website und worldofdelight.ch eingeräumt werden.
        </p>
        <p>
            Diese Website, worldofdelight.ch sowie sämtliche dort eingestellten Inhalte (insbesondere Software, Dateien,
            Designs, Grafiken und Daten) sind und bleiben Eigentum von worldofdelight.ch und sind durch die
            entsprechenden
            Gesetze bezüglich geistiger Eigentumsrechte einschliesslich Urheber- und Markenschutzrechte geschützt. Jede
            unbefugte Benutzung dieser Website bzw. von worldofdelight.ch, insbesondere die Benutzung dieser Website
            bzw.
            von worldofdelight.ch oder von deren Inhalten zu professionellen oder kommerziellen Zwecken jeglicher Art
            sowie
            die Reproduktion, Darstellung, Weitergabe an andere, Mitteilung, das in Umlauf bringen, die Verbreitung,
            Veränderung, Lizenzzuteilung, der Verkauf oder jegliche sonstige Verwertung dieser Website oder von
            worldofdelight.ch oder von deren Inhalten, Texten, Textteilen, statischen oder animierten Grafiken,
            Audiodaten,
            Software, Waren oder Dienstleistungen sowie sonstigen Daten oder Informationen ist ohne unsere vorherige
            schriftliche Genehmigung ausdrücklich untersagt. Verboten sind insbesondere Methoden wie Framing und Inline
            Linking der Website- und worldofdelight.ch-Inhalte.
        </p>
    </div>
    <li class="title_paragraph toggle">14. Datenschutz</li>
    <div class="text_wrap">
        <p>Informationen im Zusammenhang mit der Sammlung und Verarbeitung deiner im Zuge deiner Nutzung dieser Website
            und
            von worldofdelight.ch sowie im Zuge deiner dort getätigten Wareneinkäufe erfassten persönlichen Daten
            findest du
            in unseren Datenschutzrichtlinien (Pt 18)
        </p>
    </div>
    <li class="title_paragraph toggle">15. Kontakt</li>
    <div class="text_wrap">
        <p>Allfällige Fragen über die Allgemeinen Geschäftsbedingungen bzw. über unsere Datenschutzrichtlinien richtest
            du
            bitte schriftlich an unsere folgende Adresse: worldofdelight.ch, Churerstrasse 47, 8808 Pfäffikon. Bei
            Fragen zu
            worldofdelight.ch und zu deinen über worldofdelight.ch aufgegebenen Bestellungen kannst du dich via E-Mail
            an
            smoke(at)worldofdelight.ch wenden.
        </p>
    </div>
    <li class="title_paragraph toggle">16. Salvatorische Klausel</li>
    <div class="text_wrap">
        <p>Sofern Bestimmungen dieser Allgemeinen Geschäftsbedingungen jetzt oder in der Zukunft in Teilen oder in Gänze
            nichtig sein sollten oder falls diese Allgemeinen Geschäftsbedingungen rechtliche Lücken aufweisen sollten,
            bleiben die übrigen Bestimmungen hiervon unberührt und weiter uneingeschränkt in Kraft. Die anwendbaren
            gesetzlichen Regelungen treten an die Stelle der ungültigen Bestimmungen dieser Allgemeinen
            Geschäftsbedingungen.
        </p>
    </div>
    <li class="title_paragraph toggle">17. Geltendes Recht und Gerichtsstand</li>
    <div class="text_wrap">
        <p>Die vorliegenden Allgemeinen Geschäftsbedingungen und insbesondere auch das Vertragsverhältnis zwischen dir
            und
            worldofdelight.ch unterliegen Schweizer Recht, ausschliesslich des Übereinkommens der Vereinten Nationen
            über
            Verträge über den internationalen Warenkauf, und sind entsprechend auszulegen.
        </p>
        <p>
            Der Gerichtsstand ist der Wohnort des Kunden, der in der Schweiz liegen muss. </p>
        <p>
            Pfäffikon, 2014.</p>
    </div>
    <li class="title_paragraph toggle">18. Datenschutzrichtlinien</li>
    <div class="text_wrap">
        <p>Die vorliegenden Informationen betreffend Datenschutz und die Bearbeitung pers&ouml;nlicher Daten
            ("Datenschutzrichtlinien") gelten ausschliesslich f&uuml;r diese Website. Dies schliesst keine Websites ein,
            die mit dieser Website verkn&uuml;pft sind. Links zu anderen Websites sind als solche kenntlich gemacht.
            Bitte beachte, dass wir keinerlei Kontrolle &uuml;ber andere Websites haben. Daher gelten diese
            Datenschutzrichtlinien auch nicht f&uuml;r andere Websites. Wir empfehlen dir daher, die
            Datenschutzrichtlinien der jeweils von dir besuchten Websites zu lesen.</p>
        <p>Pers&ouml;nliche Daten</p>
        <p>Genusswelt AG sammelt und verwendet pers&ouml;nliche Daten ausschliesslich gem&auml;ss den Bestimmungen des
            Schweizerischen Datenschutzgesetzes. Pers&ouml;nliche Daten umfassen Informationen &uuml;ber deine Identit&auml;t
            wie etwa Name, Adresse, E-Mail-Adresse, Handy-Nummer oder Geburtsdatum. Diese Informationen sammeln und
            speichern wir nur dann, wenn sie uns von dir in aktiver Weise zugesendet worden sind.</p>
        <p>In den Bestimmungen dieser Datenschutzrichtlinien erl&auml;utert Genusswelt AG, welche Daten wir erfassen,
            verarbeiten und verwenden. Bitte nimm dir Zeit, diese Datenschutzrichtlinien sorgf&auml;ltig durchzulesen,
            bevor du die Website benutzt oder pers&ouml;nliche Daten versendest. Indem du dich auf dieser Website
            registrierst, stimmst du den Bestimmungen der vorliegenden Datenschutzrichtlinien zu.</p>
        <p>Aktives Sammeln und Verarbeiten von Daten</p>
        <p>Wir werden nicht ohne deine vorherige bewusste Zustimmung in aktiver Weise Daten &uuml;ber dich auf dieser
            Website erfassen, sofern eine solche Zustimmung entsprechend den Datenschutzgesetzen der Schweiz
            erforderlich ist. Es obliegt daher ganz deiner Entscheidung, ob du uns deine pers&ouml;nlichen Daten &uuml;bermittelst
            oder nicht. Weiterhin sammelt und verarbeitet Genusswelt AG in aktiver Weise &uuml;ber dich nur Daten, die
            du freiwillig &uuml;bermittelt bzw. mitgeteilt hast (beispielsweise in Verbindung mit deiner Einwilligung
            zum Erhalt von Mitteilungen [per E-Mail oder Kurier]).</p>
        <p>Passives Sammeln von Daten</p>
        <p>W&auml;hrend deines Aufenthalts auf unserer Website werden unter Umst&auml;nden &uuml;ber dich auch Daten auf
            passive Weise gesammelt (d.h. es werden Daten &uuml;ber dich auch dann gesammelt, wenn du diese nicht aktiv
            bereitgestellt hast). Dabei kommen eine Reihe von Technologien und Methoden zur Anwendung, beispielsweise
            Internet-Protokoll-Adressen, Cookies, Internet-Tags, Navigationsdatenbeschaffung; Google Analytics.</p>
        <p>Sogenannte &laquo;Navigationsdaten&raquo; (Protokolldateien, Serverlogs und Clickstream-Daten) werden f&uuml;r
            die Systemverwaltung, f&uuml;r die Optimierung von Website-Inhalten, zu Marktforschungszwecken und f&uuml;r
            den Versand von Informationen an Website-Besucher ben&ouml;tigt. Personenbezogene Daten werden sofort wieder
            gel&ouml;scht.</p>
        <p>Wir verwenden auf unserer Website Internet-Protokoll-Adressen (IP). Eine IP-Adresse ist eine deinem Computer
            von deinem Internet Provider zugewiesene Nummer, die es dir erm&ouml;glicht, das Internet zu benutzen. Beim
            Besuch einer Website werden automatisch Daten gespeichert. Dabei ist es m&ouml;glich, dass indirekt auf
            deine Person bezogene Informationen gesammelt werden. Dementsprechend ist es ebenfalls &uuml;blich, dass
            solche Daten gesammelt werden, wenn du die Websites anderer Anbieter besuchst. Dies umfasst beispielsweise
            Informationen &uuml;ber den von dir benutzten Webbrowser, die von dir besuchten Webseiten oder deine
            Verweildauer auf diesen Webseiten. Sofern es m&ouml;glich ist, diese Daten bestimmten Personen zuzuordnen,
            wirst du von uns kurz darauf wieder gel&ouml;scht. Insbesondere verwenden wir die entsprechenden
            Informationen dazu, um zu &uuml;berpr&uuml;fen, ob du unsere Website von einem Standort in der Schweiz aus
            aufrufst. Dar&uuml;ber hinaus werden die Daten in anonymisierter Form zum Zweck der Verbesserung unserer
            Website f&uuml;r unsere Benutzer aufgezeichnet oder um deren Sicherheit und Stabilit&auml;t
            sicherzustellen.</p>
        <p>Auf dieser Website werden Daten zu Marketingzwecken aufgezeichnet und verwendet. Dar&uuml;ber hinaus werden
            diese Daten f&uuml;r die Verbesserung der von uns auf der Website bereitgestellten Informationen verwendet.
            Dabei werden Daten allerdings nicht bestimmten Benutzern zugeordnet. Du kannst unsere Website daher anonym
            durchsuchen. Benutzerprofile werden ausschliesslich unter einem Pseudonym erstellt, wobei Cookies benutzt
            werden. Wir verwenden beispielsweise Google Analytics, einen von Google Inc. bereitgestellten
            Webanalyse-Dienst, der Informationen &uuml;ber dein Benutzer-Verhalten sammelt und dazu Cookies
            verwendet.</p>
        <p>Bei einem Cookie handelt es sich um eine kleine Textdatei, die von der angesteuerten Website an deinen
            Webbrowser gesendet wird und dazu dient, Daten &uuml;ber dich und deine Pr&auml;ferenzen zu sammeln. &laquo;Sitzungscookies&raquo;
            sind kleine tempor&auml;ren Textdateien, die gel&ouml;scht werden, sobald du das Browserfenster schliesst
            oder den Computer ausschaltest. Sitzungscookies werden zur Optimierung der Navigation auf Internetseiten
            verwendet und um zus&auml;tzliche statistische Daten zu sammeln</p>
        <p>&laquo;Permanente Cookies&raquo; sind kleine tempor&auml;re Textdateien, die gel&ouml;scht werden, sobald du
            das Browserfenster schliesst oder den Computer ausschaltest. Permanente Cookies speichern Informationen auf
            deinem Computer, die verschiedenen Zwecken dienen, beispielsweise um bestimmte von dir verwendete
            Informationen zu erfassen (z.B. Adressangaben). Weiterhin tragen Permanente Cookies dazu bei herauszufinden,
            welche unserer Webseiten f&uuml;r die einzelnen Benutzer von besonderem Interesse sind. Dies erlaubt es uns,
            Webseiten an die spezifischen Erfordernisse der Besucher anzupassen.</p>
        <p>Cookies sind keine Gefahr f&uuml;r deinen Computer. Wenn du trotzdem nicht m&ouml;chtest, dass auf deinem
            Computer Cookies gespeichert werden, kannst du deinen Computer so einrichten, dass eine Meldung angezeigt
            wird, welche die Installation von Cookies ank&uuml;ndigt.</p>
        <p>Wie deaktiviere ich Cookies?</p>
        <p>Wenn du Cookies deaktivieren m&ouml;chtest, musst du in deinem Browser die entsprechenden
            Einstellungen &auml;ndern, sodass Cookies nicht mehr akzeptiert werden. Wie du diese Einstellungen &auml;ndern
            kannst, h&auml;ngt davon ab, welchen Browser du verwendest. Nachfolgend erkl&auml;ren wir dir, wie du
            Cookies bei den am h&auml;ufigsten verwendeten Browsern deaktivierst:</p>
        <p>Bei Microsoft Internet Explorer:</p>
        <p>W&auml;hle im Men&uuml; &laquo;Extras&raquo; die Option &laquo;Internetoptionen&raquo;</p>
        <p>Klicke auf die Registerkarte &laquo;Datenschutz&raquo;</p>
        <p>W&auml;hle unter &laquo;Erweitert&raquo; die gew&uuml;nschte Einstellung aus</p>
        <p>Bei Mozilla Firefox:</p>
        <p>W&auml;hle im Men&uuml; &laquo;Extras&raquo; die Option &laquo;Einstellungen&raquo;</p>
        <p>Klicke auf die Registerkarte &laquo;Datenschutz&raquo;</p>
        <p>Klicke dort auf den Eintrag &laquo;Einzelne Cookies l&ouml;schen&raquo; und w&auml;hle die gew&uuml;nschten
            Optionen aus</p>
        <p>Bei Chrome:</p>
        <p>Rufe die Seite &laquo;Einstellungen&raquo; auf</p>
        <p>Klicke auf &laquo;Erweiterte Einstellungen&raquo;</p>
        <p>Klicke unter dem Punkt &laquo;Datenschutz&raquo; auf die
            Schaltfl&auml;che &laquo;Inhaltseinstellungen&raquo;</p>
        <p>W&auml;hle in der Rubrik &laquo;Cookies&raquo; die relevante Option</p>
        <p>Was geschieht, wenn ich Cookies deaktiviere?</p>
        <p>Das h&auml;ngt davon ab, welche Cookies du deaktivierst. Generell ist allerdings damit zu rechnen, dass die
            Website mit deaktivierten Cookies nicht richtig funktioniert. Wenn du lediglich die Cookies von
            Drittparteien deaktivierst, kannst du auf unserer Website nach wie vor deine Eink&auml;ufe t&auml;tigen.
            Wenn du alle Cookies deaktivierst, kannst du keine Eink&auml;ufe auf unserer Website t&auml;tigen.</p>
        <p>Passives Sammeln von Pers&ouml;nlichkeitsprofilen</p>
        <p>Indem du diese Datenschutzrichtlinien akzeptierst, erkl&auml;rst du dein Einverst&auml;ndnis damit, dass
            smoke24 Informationen &uuml;ber deine Eink&auml;ufe im Onlineshop Worldofdelight.ch auf dieser Website
            sammeln und diese zu Marketingzwecken analysieren darf. Auf der Grundlage deiner Einkaufsinformationen k&ouml;nnen
            Analysen deines Warenkorbs erstellt werden, die dein Verbraucherverhalten widerspiegeln. Dies kann zur
            Erstellung von Pers&ouml;nlichkeitsprofilen f&uuml;hren. Im Zuge des Sammelns solcher Einkaufsinformationen
            und zum Zweck der Durchf&uuml;hrung von Analysen des Verbraucherverhaltens k&ouml;nnen pers&ouml;nliche
            Daten aus der Schweiz an beauftragte Unternehmen &uuml;bertragen werden. Worldofdelight.ch &uuml;bergibt
            diese Aufgabe an die Genusswelt AG. Diese verpflichtet sich sowie die weiteren in diesen Prozess
            involvierten Unternehmen, zur Einhaltung der Datenschutzrichtlinien und zur Einhaltung geeigneter Massnahmen
            zur Datensicherheit.</p>
        <p>Verwendung und Weitergabe von Informationen</p>
        <p>Smoke24 und Genusswelt AG sowie die involvierten Partner, verwenden deine pers&ouml;nlichen Daten
            ausschliesslich zu folgenden Zwecken:</p>
        <p>&Uuml;berpr&uuml;fung deines Alters;</p>
        <p>&Uuml;berpr&uuml;fung des Standorts deines Computers und damit &Uuml;berpr&uuml;fung, ob du unsere Website
            von einem Standort innerhalb der Schweiz aufrufst;</p>
        <p>Bearbeitung deiner Bestellungen und Bereitstellung der &uuml;ber unsere Website angebotenen Dienstleistungen
            und Informationen, die du in Anspruch nimmst;</p>
        <p>Verwaltung deines Benutzerkontos bei uns;</p>
        <p>&Uuml;berpr&uuml;fung und Durchf&uuml;hrung finanzieller Transaktionen in Verbindung mit Zahlungen, die du
            online durchf&uuml;hrst;</p>
        <p>&Uuml;berwachung des Herunterladens von Daten von unserer Website;</p>
        <p>Verbesserung des Layouts bzw. der Inhalte der Seiten unserer Website und deren benutzerfreundliche
            Anpassung;</p>
        <p>Identifizierung von Besuchern unserer Website;</p>
        <p>Beantwortung deiner Anfragen;</p>
        <p>Zusendung von Informationen in Verbindung mit deinen Anfragen</p>
        <p>Abh&auml;ngig von deiner Zustimmung w&auml;hrend der Registrierung &ndash; in der Eingabemaske zur
            Registrierung kannst du ein separates Kontrollk&auml;stchen anklicken, wodurch du der Zusendung solcher
            Informationen und Mitteilungen per E-Mail oder Kurier zustimmst &ndash; senden wir dir m&ouml;glicherweise
            noch weitere Informationen zu &uuml;ber</p>
        <p>diese Website</p>
        <p>unsere anderen Websites, d.h. &uuml;ber unsere spezifischen Genusswelt AG Marken-Websites;</p>
        <p>unsere Produkte;</p>
        <p>Verkaufsf&ouml;rderungsaktionen und Sponsoring-Aktivit&auml;ten der Genusswelt AG;</p>
        <p>andere der Genusswelt AG angeschlossene Unternehmen oder unsere Gesch&auml;ftspartner, einschliesslich
            Informationen &uuml;ber deren Produkte und Dienstleistungen, Verkaufsf&ouml;rderungsaktionen usw.</p>
        <p>Wenn du deine Zustimmung zu einem sp&auml;teren Zeitpunkt zur&uuml;ckziehen m&ouml;chtest, klick in einer der
            E-Mails, die du von uns erh&auml;ltst, auf den Link &laquo;Abbestellen&raquo; oder rufe dein Konto auf, um
            deine Profil-Pr&auml;ferenzen zu &auml;ndern.</p>
        <p>Gewinnung sicherer Informationen &uuml;ber den Hauptstandort des einzelnen Benutzers und Verwendung
            bestimmter Aspekte der Website bzw. Zusendung eines Links in einer E-Mail an registrierte Empf&auml;nger
            sowie Bereitstellung der gewonnenen anonymisierten Daten an Dritte, beispielsweise Herausgeber; solche
            anonymisierten Daten k&ouml;nnen allerdings nicht dazu benutzt werden, dich pers&ouml;nlich zu
            identifizieren.</p>
        <p>Deine pers&ouml;nlichen Daten werden nur dann an Dritte weitergegeben, wenn eine solche Weitergabe zu Zwecken
            der Bearbeitung deiner Bestellung, d.h. f&uuml;r die Lieferung deiner bestellten Waren oder zur Abwicklung
            von Zahlungen oder zur Bereitstellung sonstiger von dir in Anspruch genommener Dienstleistungen,
            einschliesslich unter anderem Informationsdienste bzw. Mitteilungen (per E-Mail oder Kurier), erforderlich
            ist. Da der Online-Shop im Auftrag von smoke24 durch die Genusswelt AG betrieben wird, geben wir die f&uuml;r
            die Abwicklung deiner Bestellung erforderlichen pers&ouml;nlichen Daten an dieses Unternehmen weiter.
            Genusswelt AG ist dar&uuml;ber hinaus dazu befugt, Drittunternehmen mit der Bereitstellung der von dir in
            Anspruch genommenen Dienstleistungen zu beauftragen.</p>
        <p>Die beauftragten Unternehmen, an die deine pers&ouml;nlichen Daten weitergegeben werden, d&uuml;rfen diese
            pers&ouml;nliche Daten ausschliesslich f&uuml;r die Bereitstellung der von dir in Anspruch genommenen
            Dienstleistungen verwenden. Wir geben nur solche pers&ouml;nlichen Daten weiter, die f&uuml;r die
            Bereitstellung der einzelnen konkret in Anspruch genommenen Dienstleistungen erforderlich sind.</p>
        <p>Bitte beachte, dass f&uuml;r die Bearbeitung deiner Zahlungen per Rechnung, Kredit- oder Debitkarte die
            Allgemeinen Gesch&auml;ftsbedingungen der entsprechenden Institute gelten. Auf der Grundlage dieser
            Allgemeinen Gesch&auml;ftsbedingungen erkl&auml;rst du dich damit einverstanden, dass bei Verwendung deiner
            Kredit- oder Debitkarte f&uuml;r Zahlungen, und wenn du uns deine Kredit- oder Debitkarten-Angaben
            mitteilst, die pers&ouml;nlichen Daten, die dein Kredit- oder Debitkarteninstitut f&uuml;r die Abbuchung des
            f&uuml;r deine Transaktion f&auml;lligen Betrags und dessen Gutschrift auf unser Konto ben&ouml;tigt, an
            dein Kredit- oder Debitkarteninstitut und an von diesem beauftragte Unternehmen weitergeleitet werden.
            Weitere Informationen bez&uuml;glich der Verarbeitung pers&ouml;nlicher Daten im Zuge einer Kredit- oder
            Debitkartenzahlung findest du in den Allgemeinen Gesch&auml;ftsbedingungen deines Kredit- oder
            Debitkarteninstituts.</p>
        <p>Es kann vorkommen, dass Worldofdelight.ch, Genusswelt AG und die von ihm beauftragten Unternehmen gezwungen
            sind, aufgrund gesetzlicher Bestimmungen, gerichtlicher Verf&uuml;gungen oder Verf&uuml;gungen zust&auml;ndiger
            Beh&ouml;rden deine pers&ouml;nlichen Daten herauszugeben. In einem solchen Fall wirst du im Voraus &uuml;ber
            eine solche Herausgabe in Kenntnis setzen, sofern dies durch die anwendbaren Gesetze erlaubt ist.</p>
        <p>Zugriff auf deine pers&ouml;nlichen Daten und ggf. deren Berichtigung</p>
        <p>Um sicherzustellen, dass deine pers&ouml;nlichen Daten richtig, auf dem neuesten Stand und vollst&auml;ndig
            sind, setze dich bitte mit uns unter smoke(at)worldofdelight.ch in Verbindung. Wir werden dann veranlassen,
            dass die uns zuvor von dir &uuml;ber diese Website zugesandten pers&ouml;nlichen Daten aktualisiert bzw.
            berichtigt werden.</p>
        <p>Sicherheit deiner Daten</p>
        <p>Wir ergreifen die erforderlichen Massnahmen, um pers&ouml;nliche Daten zu sch&uuml;tzen, die von deinem
            Computer zu unserer Website gesendet werden. Insbesondere sch&uuml;tzen wir diese vor Verlust, Missbrauch,
            unberechtigtem Zugriff, unberechtigter Offenlegung, Manipulation, L&ouml;schen oder Vernichtung. Bitte
            beachte dennoch, dass die Versendung von Daten &uuml;ber das Internet nie vollst&auml;ndig sicher oder
            fehlerfrei ist.</p>
        <p>Wenn du deine pers&ouml;nlichen Daten elektronisch &uuml;ber unsere Website versendest, wird f&uuml;r
            deren &Uuml;bertragung auf unser System eine sichere Internetverbindung (SSL) aufgebaut. Insbesondere
            E-Mails, die zu dieser Website oder von dieser Website aus gesendet werden, sind m&ouml;glicherweise nicht
            sicher. Bitte achte stets darauf, welche Informationen du uns per E-Mail zusendest. Es obliegt dar&uuml;ber
            hinaus deiner Verantwortung, Deine Passw&ouml;rter, Benutzer-IDs und sonstigen besonderen Zugangssysteme
            sicher aufzubewahren.</p>
        <p>Sonstige Bedingungen</p>
        <p>Die Benutzung dieser Website unterliegt unseren Allgemeinen Gesch&auml;ftsbedingungen.</p>
        <p>Kontakt:</p>
        <p>Falls du Fragen, Anmerkungen oder Bedenken bez&uuml;glich dieser Datenschutzrichtlinien oder der auf dieser
            Website angewendeten Praktiken hast, wende dich bitte an:</p>
        <p>smoke(at)worldofdelight.ch.</p>
        <p>&Auml;nderungen der vorliegenden Datenschutzrichtlinien</p>
        <p>Bitte beachte, dass wir entsprechend unseren Dienstleistungen und Erfordernissen und unter Ber&uuml;cksichtigung
            m&ouml;glicher ge&auml;nderter Datenschutzgesetze von Zeit zu Zeit &Auml;nderungen an unseren
            Datenschutzrichtlinien vornehmen. Dementsprechend behalten wir uns &Auml;nderungen der in diesen
            Datenschutzrichtlinien aufgef&uuml;hrten Bestimmungen ohne Vorank&uuml;ndigung und nach eigenem Ermessen
            vor. Bitte konsultiere diesbez&uuml;glich zu deiner Information regelm&auml;ssig unsere Website. Es obliegt
            deiner Verantwortung, diese Website regelm&auml;ssig zu konsultieren und sich &uuml;ber allf&auml;llige
            zwischenzeitlich erfolgte &Auml;nderungen zu informieren, insbesondere bevor du uns pers&ouml;nliche Daten
            zusendest.</p></div>
</ul>';
    }

}
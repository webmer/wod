<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505487808
 *
 * @package Migration
 */
class Version1505487808 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Voucher')
            ->setIdentifier('voucher')
            ->setContentHeading('Voucher')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('Voucher')
            ->setIdentifier('voucher')
            ->setContentHeading('Voucher')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p lang="en-GB">Coupons / vouchers</p>
<p lang="en-GB">((Kasten links))</p>
<p lang="en-GB">Delight give away</p>
<p lang="en-GB">Give away vouchers from worldofdelight.ch&rsquo;s whole selection. Choose a standard amount (CHF 20, 50, 100, 200) or another amount. Order your voucher today! On worldofdelight.ch you can find a big selection of everything a connoisseur could need. With this voucher, you have the free choice!</p>
<p lang="en-GB">The vouchers are valid for 5 years for the whole selection. The perfect gift for any occasion!</p>
<p lang="en-GB">Please consider that only one coupon / voucher may be used for one order.</p>
<p lang="en-GB">Your first name</p>
<p lang="en-GB">Your last name</p>
<p lang="en-GB">Receiver first name</p>
<p lang="en-GB">Receiver last name</p>
<p lang="en-GB">Receiver e-mail-address</p>
<p lang="en-GB">Amount in CHF</p>
<p lang="en-GB">You message</p>
<p lang="en-GB">Criteria</p>
<p lang="en-GB">All mandatory fields must be filled.</p>
<p lang="en-GB">Names: Only letters are allowed here</p>
<p lang="en-GB">E-mail: needs an @ and a top-level-domain (eg. .ch/.de/.com/etc.)</p>
<p lang="en-GB">Amount: A number with two decimal places max, split by a fullstop (eg. 12.34)</p>
<p lang="en-GB">((Kasten rechts))</p>
<p lang="en-GB">Discount</p>
<p lang="en-GB">by</p>
<p lang="en-GB">CHF Amount.-</p>
<p lang="en-GB">For: First name last name</p>
<p lang="en-GB">By: First name last name</p>
<p lang="en-GB">Your message</p>
<p lang="en-GB"><br /> </p>
<p lang="en-GB">COUPON CODE</p>
<p lang="en-GB">CODE123</p>
<p lang="en-GB">This is how you use your code:</p>
<p lang="en-GB">Enter your code while shopping directly into the appropriate field while viewing your cart.</p>
<p lang="en-GB">Your discount is automatically subtracted from your total amount.</p>';
    }

    private function getContentDe()
    {
        return '<p>Gutscheine</p>
<p>((Kasten links))</p>
<p>Verschenke Genuss nach freier Wahl</p>
<p>Schenke einen Geschenkgutschein von worldofdelight.ch f&uuml;r das ganze Sortiment: W&auml;hle einen Standard-Betrag (CHF 20, 50, 100, 200) oder einen anderen Betrag. Bestelle noch heute deinen Wertgutschein! Auf worldofdelight.ch findest du eine grosse Geschenk-Auswahl an allem was das Geniesserherz begehrt. Mit diesem Geschenkgutschein hast du die freie Auswahl!</p>
<p>Die Geschenkgutscheine sind ab Kaufdatum 5 Jahre f&uuml;r das ganze Sortiment g&uuml;ltig: Das perfekte Geschenk f&uuml;r jeden Anlass!</p>
<p>Bitte beachte, dass jeweils nur 1 Wertgutschein pro Bestellung in Abzug gebracht werden kann.</p>
<p>Deinen Vornamen</p>
<p>Deinen Nachnamen</p>
<p>Empf&auml;nger Vornamen</p>
<p>Empf&auml;nger Nachnamen</p>
<p>Empf&auml;nger E-Mail</p>
<p>Betrag in CHF</p>
<p>Deine Mitteilung</p>
<p>Vorgaben</p>
<p>Alle Pflichtfelder m&uuml;ssen ausgef&uuml;llt sein.</p>
<p>Vor- und Nachname: Hier sind nur Buchstaben erlaubt.</p>
<p>E-Mail: Braucht ein @ und eine Top-Level-Domain (Bsp. .ch/.de/.com/etc.)</p>
<p>Betrag: Eine Zahl mit max. zwei Kommastellen, mit einem Satzpunkt getrennt (Bsp. 12.34)</p>
<p>((Kasten rechts))</p>
<p>RABATT</p>
<p>Von</p>
<p>CHF Betrag.-</p>
<p>F&uuml;r: Vorname Nachname</p>
<p>Von: Vorname Nachname</p>
<p>Deine Nachricht</p>
<p>RABATTCODE</p>
<p>CODE123</p>
<p>So kannst du deinen Rabattcode einl&ouml;sen:</p>
<p>Gib den Rabattcode beim Einkauf direkt ins Rabattfeld Deines Warenkorbes ein.</p>
<p>Dein Rabatt wird automatisch mit dem Wareneinkauf verrechnet.</p>';
    }

}
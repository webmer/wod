<?php

namespace Migration;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Snatch\Migrations\Contracts\Migration;


/**
 * Class Version1516002106
 *
 * @package Migration
 */
class Version1516002106 implements Migration
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    private $setup;
    private $context;

    /**
     * Version1516002106 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $setup
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->setup = $setup;
        /* assign object to class global variable for use in other class methods */
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $this->install($this->setup);
    }

    private function install(ModuleDataSetupInterface $setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        /**
         * Add attributes to the eav/attribute
         */
//        die('asdgasdgh');
        $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'topmarken');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'topmarken',
            [
                'group' => 'General',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Top marken', /* lablel of your attribute*/
                'input' => 'select',
                'class' => '',
                'source' => 'Meridian\TopMarken\Model\Config\Source\Options',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
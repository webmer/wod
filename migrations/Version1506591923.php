<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class Version1506591923
 *
 * @package Migration
 */
class Version1506591923 implements Migration
{
    private $eavSetupFactory;

    private $categorySetupFactory;

    private $attributeSetCollection;
    private $setup;
    private $context;
    /**
     * Version1506591923 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        ModuleDataSetupInterface $setup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetCollection = $attributeSetCollection;
        $this->setup = $setup;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $this->installAttrs($this->setup,$this->context);
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    public function installAttrs(
        ModuleDataSetupInterface $setup
    ) {

        $attributeGroupName = 'Attributes';
        /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $attrSetNameArr = array("kaffee");
        $attributeSetIdArr = [];
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        foreach ($attrSetNameArr as $attrCode){
            $attributeSetIdArr[] = $this->getAttrSetId($attrCode);
        }

        $attributesData = [
            ['code' => 'wod_herkunft', 'label' => 'Herkunft', 'order' => 54],
            ['code' => 'wod_aroma', 'label' => 'Aroma', 'order' => 55],
            ['code' => 'wod_roast', 'label' => 'Röstgrad', 'order' => 56, 'type' => 'varchar', 'input' => 'select'],
            ['code' => 'wod_intensity', 'label' => 'Intensität', 'order' => 57, 'type' => 'varchar', 'input' => 'select']
        ];
        $this->_installerAttribute($entityTypeId,$attributesData,$eavSetup,$attributeGroupName,$attributeSetIdArr);

        /* install tabak attributes */
        unset($attributesData);
        unset($attrSetNameArr);
        unset($attributeSetIdArr);
        $attrSetNameArr = ["tabak"];
        foreach ($attrSetNameArr as $attrCode){
            $attributeSetIdArr[] = $this->getAttrSetId($attrCode);
        }
        $attributesData = [
            ['code' => 'wod_nicotin', 'label' => 'Nikotin', 'order' => 54],
            ['code' => 'wod_tar', 'label' => 'Teer', 'order' => 55],
            ['code' => 'wod_kohlenmonoxid', 'label' => 'Kohlenmonoxid', 'order' => 56],
            ['code' => 'wod_origin', 'label' => 'Herkunft', 'order' => 57,'type' => 'varchar','input' => 'select','source' => 'Magento\Catalog\Model\Product\Attribute\Source\Countryofmanufacture'],
            ['code' => 'wod_length', 'label' => 'Länge', 'order' => 58],
            ['code' => 'wod_diameter', 'label' => 'Durchmesser', 'order' => 59]
        ];
        $this->_installerAttribute($entityTypeId,$attributesData,$eavSetup,$attributeGroupName,$attributeSetIdArr);

        /* install Sweets attributes */
        unset($attributesData);
        unset($attrSetNameArr);
        unset($attributeSetIdArr);
        $attrSetNameArr = ["süssigkeiten"];
        foreach ($attrSetNameArr as $attrCode){
            $attributeSetIdArr[] = $this->getAttrSetId($attrCode);
        }
        $attributesData = [
            ['code' => 'wod_brennwert', 'label' => 'Brennwert', 'order' => 54],
            ['code' => 'wod_protein', 'label' => 'Eiweiss', 'order' => 55],
            ['code' => 'wod_carbohydrate', 'label' => 'Kohlenhydrate', 'order' => 56],
            ['code' => 'wod_which_is_sugar', 'label' => 'davon Zucker', 'order' => 57],
            ['code' => 'wod_fat', 'label' => 'Fett', 'order' => 58],
            ['code' => 'wod_which_is_fatty', 'label' => 'davon gesättigt', 'order' => 59],
            ['code' => 'wod_fibers', 'label' => 'Ballaststoffe', 'order' => 60],
            ['code' => 'wod_sodium', 'label' => 'Natrium', 'order' => 61]
        ];
        $this->_installerAttribute($entityTypeId,$attributesData,$eavSetup,$attributeGroupName,$attributeSetIdArr);

        /* install Spirits attributes */
        unset($attributesData);
        unset($attrSetNameArr);
        unset($attributeSetIdArr);
        $attrSetNameArr = ["getränke"];
        foreach ($attrSetNameArr as $attrCode){
            $attributeSetIdArr[] = $this->getAttrSetId($attrCode);
        }
        $attributesData = [
            ['code' => 'wod_land', 'label' => 'Land', 'order' => 54,'type' => 'varchar','input' => 'select','source' => 'Magento\Catalog\Model\Product\Attribute\Source\Countryofmanufacture'],
            ['code' => 'wod_region', 'label' => 'Region', 'order' => 55],
            ['code' => 'wod_weingut', 'label' => 'Weingut', 'order' => 56],
            ['code' => 'wod_year_age', 'label' => 'Jahrgang', 'order' => 57],
            ['code' => 'wod_volumen', 'label' => 'Volumen', 'order' => 58],
            ['code' => 'wod_producer', 'label' => 'Produzent', 'order' => 59],
            ['code' => 'wod_product_category', 'label' => 'Artikelkategorie', 'order' => 60],
            ['code' => 'wod_chearacteristics', 'label' => 'Chraraktereigenschaft', 'order' => 61],
            ['code' => 'wod_grossgebinde', 'label' => 'Grossgebinde', 'order' => 62, 'type' => 'varchar', 'input' => 'select'],
            ['code' => 'wod_barriqueausbau', 'label' => 'Barriqueausbau', 'order' => 63,'type' => 'int','input' => 'select','source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean'],
            ['code' => 'wod_storable_until', 'label' => 'Lagerfähig bis', 'order' => 64],
            ['code' => 'wod_min_drink', 'label' => 'Minimale Trinktemperatur', 'order' => 65,'type' => 'int'],
            ['code' => 'wod_max_drink', 'label' => 'Maximale Trinktemperatur', 'order' => 66,'type' => 'int']
        ];
        $this->_installerAttribute($entityTypeId,$attributesData,$eavSetup,$attributeGroupName,$attributeSetIdArr);
    }

    protected function _installerAttribute($entityTypeId,$attributesData = array(),$eavSetup,$attributeGroupName,$attributeSetIdArr)
    {
        $this->setup->startSetup();

        foreach ($attributesData as $attributeData) {
            $attributeCode = $attributeData['code'];
            $attributeLabel = $attributeData['label'];
            $order = $attributeData['order'];
            $source = isset($attributeData['source']) ? $attributeData['source'] : '';
            $type = isset($attributeData['type']) ? $attributeData['type'] : 'text';
            $input = isset($attributeData['input']) ? $attributeData['input'] : 'text';
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'type' => $type,
                    'backend' => '',
                    'frontend' => '',
                    'label' => $attributeLabel,
                    'input' => $input,
                    'wysiwyg_enabled' => true,
                    'frontend_class' => '',
                    'source' => $source,
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    //'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
                    'apply_to' => '',
//                    'group' => $attributeGroupName
                ]
            );
            foreach ($attributeSetIdArr as $attributeSetId) {
                $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupName, $attributeCode, $order);
            }
        }

        $this->setup->endSetup();
    }

    public function getAttrSetId($attrSetName)
    {
        $attributeSet = $this->attributeSetCollection->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'attribute_set_name',
            $attrSetName
        );
        $attributeSetId = 0;
        foreach($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }
}
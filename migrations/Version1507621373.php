<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Version1507621373
 *
 * @package Migration
 */
class Version1507621373 implements Migration
{
    /**
     * @var BlockFactory
     */
    protected $_blockFactory;


    public function __construct(
        BlockFactory $blockFactory
    ) {
        $this->_blockFactory = $blockFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $block1 = [
            'title' => 'Tabak_banner',
            'identifier' => 'Tabak_banner',
            'stores' => [1,2],
            'is_active' => 1,
            'content' => $this->getContent()
        ];

        $block2 = [
            'title' => 'Kaffee_banner',
            'identifier' => 'Kaffee_banner',
            'stores' => [1,2],
            'is_active' => 1,
            'content' => $this->getContent()
        ];

        $block3 = [
            'title' => 'Süssewaren_banner',
            'identifier' => 'Süssewaren_banner',
            'stores' => [1,2],
            'is_active' => 1,
            'content' => $this->getContent()
        ];

        $block4 = [
            'title' => 'Getränke_banner',
            'identifier' => 'Getränke_banner',
            'stores' => [1,2],
            'is_active' => 1,
            'content' => $this->getContent()
        ];

        $this->_blockFactory->create()->setData($block1)->save();
        $this->_blockFactory->create()->setData($block2)->save();
        $this->_blockFactory->create()->setData($block3)->save();
        $this->_blockFactory->create()->setData($block4)->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContent(){
        return 'TEST CONTENT';
    }
}
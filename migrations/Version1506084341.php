<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class Version1506084341
 *
 * @package Migration
 */
class Version1506084341 implements Migration
{
    /**
     * @var ConfigInterface
     */
    private $resourceConfig;
    /**
     * Version1506084341 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        ConfigInterface $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }


    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $info = array(
            'alothemes/grid/mobile'=>'1',
            'alothemes/grid/portrait'=>'2',
            'alothemes/grid/landscape'=>'2',
            'alothemes/grid/tablet'=>'3',
            'alothemes/grid/notebook'=>'4',
            'alothemes/grid/desktop'=>'4',
            'alothemes/grid/visible'=>'5',
            'alothemes/grid/padding'=>'15',

        );

        foreach ($info as $key =>$value) {
            $this->resourceConfig->saveConfig(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }
        $this->resourceConfig->saveConfig(
            'design/header/logo_src',
            'websites/1/logo.png',
            'stores',
            '1'
        );
        $this->resourceConfig->saveConfig(
            'design/header/logo_src',
            'stores/2/logo.png',
            'stores',
            '2'
        );

    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
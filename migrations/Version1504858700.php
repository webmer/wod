<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1504858700
 *
 * @package Migration
 */
class Version1504858700 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('About us')
            ->setIdentifier('about-us')
            ->setContentHeading('Über Uns')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('About us')
            ->setIdentifier('about-us')
            ->setContentHeading('About us')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p>Shopping online is a thing of the past. If tobacco, coffee, spirits, or sweets – WorldofDelight.ch is the first online shop to offer all tobacco products of all brands as a standing order or single order at the best price-performance ratio.</p>
<p>Every day thousands of people run to a kiosk, gas station, shop, or restaurant to buy their daily smokes. Why spend so much time, money, and nerves on a stressful shopping experience, when it could be so much easier and priceworthy? Wordlofdelight.ch set out the goal to run the biggest online tobacco shop in Switzerland and to offer tobacco products from all over the word as simple, convenient, and priceworthy as possible.</p>
<p>We’re looking forward to your visit.</p>
<p>Your worldofdelight.ch team</p>';
    }

    private function getContentDe()
    {
        return '<p>Offline einkaufen war gestern. Egal ob Tabak, Kaffee, Sprituosen oder Süssigkeiten - WorldofDelight.ch bietet als erster Online-Shop jegliche Tabakwaren aller Marken in Form eines Abonnements und auch als Einzelbestellung zum besten Preis- /Leistungsverhältnis.</p> 
<p>Jeden Tag strömen tausende von Menschen zu Kioske, Tankstellen, Shops oder Restaurants um ihren täglichen Bedarf an Raucherwaren zu erwerben. Warum täglich so viel Zeit, Kosten und Nerven für den Einkaufsstress investieren, wenn es auch viel einfacher und preiswerter geht? worldofdelight.ch. hat sich zum Ziel gesetzt, den grössten online Tabakshop der Schweiz zu betreiben und Tabakprodukte aus aller Welt möglichst einfach, bequem und kostengünstig anzubieten.</p>
<p>Wir freuen uns, auf Deinen Besuch.</p>
<p>Dein worldofdelight.ch Team</p>';
    }

}
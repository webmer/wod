<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Version1505121150
 *
 * @package Migration
 */
class Version1505121150 implements Migration
{
    /**
     * @var BlockFactory
     */
    protected $_blockFactory;

    /**
     * Version1505121150 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        BlockFactory $blockFactory
    ) {
        $this->_blockFactory = $blockFactory;
    }


    public function up()
    {
        $blockEn = [
            'title' => 'block_footer_information',
            'identifier' => 'block_footer_information',
            'stores' => [2],
            'is_active' => 1,
            'content' => $this->getContentEn()
        ];

        $blockDe = [
            'title' => 'block_footer_information',
            'identifier' => 'block_footer_information',
            'stores' => [1],
            'is_active' => 1,
            'content' => $this->getContentDe()
        ];

        $this->_blockFactory->create()->setData($blockEn)->save();
        $this->_blockFactory->create()->setData($blockDe)->save();


    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentDe(){
        return '<div class="contacts"><span>Kontakt : </span>
                <p class="street-address">Genusswelt AG <br /> Bahnhofstrasse 28<br /> 6300 Zug</p>
                <span>Address Data : </span>
                <div class="phone-holder">
                <p class="tel">Tel 041 511 51 00</p>
                <p class="email">info@genusswelt.ch</p>
                </div>
                </div>
                <div class="information"><span class="footer_title">Informationen</span>
                <ul class="info_list">
                <li><a href="#">Über Uns</a></li>
                <li><a href="#">Impressum</a></li>
                <li><a href="#">AGB und Datenschutz</a></li>
                <li><a href="#">Werbung</a></li>
                </ul>
                </div>
                <div class="support"><span class="footer_title">Support</span>
                <ul class="info_list">
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Bestellhilfe</a></li>
                </ul>
                </div>
                <div class="service"><span class="footer_title">Service</span>
                <ul class="info_list">
                <li><a href="#">Gutscheine</a></li>
                <li><a href="#">Lieferservice</a></li>
                <li><a href="#">Zigarren Event Service</a></li>
                <li><a href="#">Wettbewerb</a></li>
                </ul>
                </div>
                <div class="payments"><span class="footer_title">Zahlungsarten</span>
                <p>Bezahlung mit Kreditkarten, PostCard <br />  oder auf Rechnung by PowerPay</p>
                <div class="payment-icons"><img src="{{media url="wysiwyg/payments.png"}}" alt="" /> <img src="{{media url="wysiwyg/payments2_3_.png"}}" alt="" /></div>
                </div>
                <div class="socials-wrap">
                <a href="#" class="social-link facebook">
                <span class="fa fa-facebook"></span>
                </a>
                <a href="#" class="social-link tumblr">
                <span class="fa fa-tumblr"></span>
                </a>
                <a href="#" class="social-link pinterest">
                <span class="fa fa-pinterest"></span>
                </a>
                <a href="#" class="social-link twitter">
                <span class="fa fa-twitter"></span>
                </a>
                </div>';
    }

    private function getContentEn(){
        return '<div class="contacts"><span>Contact : </span>
                    <p class="street-address">Genusswelt AG <br /> Bahnhofstrasse 28<br /> 6300 Zug</p>
                    <span>Address Data : </span>
                    <div class="phone-holder">
                    <p class="tel">Tel 041 511 51 00</p>
                    <p class="email">info@genusswelt.ch</p>
                    </div>
                    </div>
                    <div class="information"><span class="footer_title">Information</span>
                    <ul class="info_list">
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Legal notice</a></li>
                    <li><a href="#">Terms and conditions, and privacy</a></li>
                    <li><a href="#">Advertising</a></li>
                    </ul>
                    </div>
                    <div class="support"><span class="footer_title">Support</span>
                    <ul class="info_list">
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Order Help</a></li>
                    </ul>
                    </div>
                    <div class="service"><span class="footer_title">Service</span>
                    <ul class="info_list">
                    <li><a href="#">Voucher</a></li>
                    <li><a href="#">Delivery</a></li>
                    <li><a href="#">Cigar Event Service</a></li>
                    <li><a href="#">Competition</a></li>
                    </ul>
                    </div>
                    <div class="payments"><span class="footer_title">Payment</span>
                    <p>Pay with credit card, PostCard <br /> or by invoice by PowerPay</p>
                    <div class="payment-icons"><img src="{{media url="wysiwyg/payments.png"}}" alt="" /> <img src="{{media url="wysiwyg/payments2_3_.png"}}" alt="" /></div>
                    </div>
                    <div class="socials-wrap">
                    <a href="#" class="social-link facebook">
                    <span class="fa fa-facebook"></span>
                    </a>
                    <a href="#" class="social-link tumblr">
                    <span class="fa fa-tumblr"></span>
                    </a>
                    <a href="#" class="social-link pinterest">
                    <span class="fa fa-pinterest"></span>
                    </a>
                    <a href="#" class="social-link twitter">
                    <span class="fa fa-twitter"></span>
                    </a>
                    </div>';
    }
}
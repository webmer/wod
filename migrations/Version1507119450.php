<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class Version1507119450
 *
 * @package Migration
 */
class Version1507119450 implements Migration
{
    private $eavSetupFactory;

    private $categorySetupFactory;

    private $attributeSetCollection;
    private $setup;
    private $context;

    /**
     * Version1507119450 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetup $eavSetupFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        ModuleDataSetupInterface $setup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetCollection = $attributeSetCollection;
        $this->setup = $setup;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $value = false;
        $this->eavSetupFactory->updateAttribute('catalog_product', 'label','is_visible_on_front', $value);
        $this->eavSetupFactory->updateAttribute('catalog_product', 'show_desc','is_visible_on_front', $value);
        $this->eavSetupFactory->removeAttribute('catalog_product', 'zusatzinformationen');
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505487802
 *
 * @package Migration
 */
class Version1505487802 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1505487802 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('FAQ')
            ->setIdentifier('faq')
            ->setCustomLayoutUpdateXml('<referenceContainer name="before.body.end"><block class="Magento\Framework\View\Element\Template" name="test" template="Magento_Theme::include_js.phtml"/></referenceContainer> ')
            ->setContentHeading('H&auml;ufig gestellte Fragen')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('FAQ')
            ->setIdentifier('faq')
            ->setCustomLayoutUpdateXml('<referenceContainer name="before.body.end"><block class="Magento\Framework\View\Element\Template" name="test" template="Magento_Theme::include_js.phtml"/></referenceContainer> ')
            ->setContentHeading('Frequently asked questions')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p lang="en-GB">We love to help you find your desired tobacco product und answering your question to our product and service. You may contact us by e-mail: kontakt(at)worldofdelight.ch</p>
<ul class="data_inner">
<li class="title_paragraph toggle">How do I find my preferred tobacco product?</li>
<div class="text_wrap">
<p>On the home site, you can find all products categorized in product groups which are sorted alphabetically or you may enter your preferred brand in the search field. The matching products are shown afterwards. You can sort the results by name or price.</p>
</div>
<li class="title_paragraph toggle">Recommend a product?</li>
<div class="text_wrap">
<p lang="en-GB">This function enables you to recommend a product to a friend or acquaintance by e-mail or facebook.</p>
</div>
<li class="title_paragraph toggle">How do I order a product?</li>
<div class="text_wrap">
<p lang="en-GB">On the right next to the product you&rsquo;ll find a box. Here you may choose your preferred product and amount.</p>
<p lang="en-GB">Afterwards, you proceed with &laquo;Add to cart&raquo;.</p>
<p lang="en-GB">Your preferred item is now in your cart.</p>
<p lang="en-GB">You have the possibility to delete products in your cart, change the amount and refresh the prices</p>
<p><span lang="en-GB">If you want to proceed shopping you may click </span><span style="font-family: \'Segoe UI Symbol\', serif;"><span lang="en-GB">&laquo;</span></span><span lang="en-GB">Continue shopping&raquo;, your preferred category, or use the search field. Your chosen items will remain in your cart.</span></p>
<p lang="en-GB">If you would like to order all products click &laquo;go to Checkout&raquo; while looking at your cart.</p>
<p lang="en-GB">Login with your registered account (e-mail and password) or sign up as a new user. [next]</p>
<p lang="en-GB">Check your shipping and billing address. [next]</p>
<p lang="en-GB">If you have a coupon to use you may enter the code in the equivalent field.</p>
<p lang="en-GB">Payment methods are shown and may be chosen.</p>
<br /> 
<p lang="en-GB">You may check your data in the overview provided at the end. If your happy with the order and accept the terms and conditions, klick on the terms and conditions to accept them.</p>
</div>
<li class="title_paragraph toggle">When will I receive my shipment?</li>
<div class="text_wrap">
<p lang="en-GB">If your order was placed until 9 am it will be shipped in the following two workdays.</p>
</div>
<li class="title_paragraph toggle">How do I make use of a promotion coupon/voucher?</li>
<div class="text_wrap">
<p lang="en-GB">While viewing your cart you may enter coupon or voucher codes.</p>
</div>
<li class="title_paragraph toggle">What payment methods may I use?</li>
<div class="text_wrap">
<p lang="en-GB">Your order may be paid with invoice, Postcard, or with a visa or master card credit card. You order will be transmitted via SSL. You may share your personal information such as your credit card number unobjectionable.</p>
</div>
<li class="title_paragraph toggle">What are the shipping cost?</li>
<div class="text_wrap">
<p lang="en-GB">Your shipment will cost CHF 7.- unless your total amount exceedes CHF 50.- which will make your shipment free.</p>
</div>
</ul>';
    }

    private function getContentDe()
    {
        return '<p>Gerne helfen wir Dir, Dein gew&uuml;nschtes Tabakprodukt zu finden und beantworten Deine Frage zur unseren Produkten und Dienstleistungen. Du erreichst uns per E-Mail unter kontakt(at)worldofdelight.ch</p>
<ul class="data_inner">
<li class="title_paragraph toggle">Wie finde ich mein gew&uuml;nschtes Tabakprodukt?</li>
<div class="text_wrap">
<p>Auf der Startseite findest du einerseits alle Produkte nach Produktgruppen kategorisiert, welche alphabetisch sortiert sind oder kannst deine Marke gezielt im Suchfeld eingeben. Die passenden Produkte werden Dir anschliessend angezeigt. Das Suchergebnis kannst Du nach Name oder Preis sortieren.</p>
</div>
<li class="title_paragraph toggle">Ein Produkt empfehlen?</li>
<div class="text_wrap">
<p>Mit dieser Funktion hast Du die M&ouml;glichkeit ein Produkt an einen Freund oder Bekannten per E-Mail oder via Facebook zu empfehlen.</p>
</div>
<li class="title_paragraph toggle">Wie kann ich ein Produkt bestellen?</li>
<div class="text_wrap">
<p>Rechts neben dem Produkt findest Du einen Kasten. Hier kannst Du das gew&uuml;nschte Produkt, sowie die Menge ausw&auml;hlen.</p>
<p>Anschliessend f&auml;hrst Du mit &bdquo;Artikel in den Warenkorb&ldquo; fort.</p>
<p>Der gew&uuml;nschte Artikel befindet sich nun in Deinem Warenkorb.</p>
<p>Im Warenkorb hast Du immer die M&ouml;glichkeit Produkte wieder zu l&ouml;schen, die Anzahl zu ver&auml;ndern und den Preis zu aktualisieren.</p>
<p>Wenn Du weitere Eink&auml;ufe t&auml;tigen m&ouml;chtest, w&auml;hlen &bdquo;weiter einkaufen&ldquo; oder die gew&uuml;nschte Kategorie an, oder geben den Artikelnamen im Feld &bdquo;Suche&ldquo; ein. Die bereits gew&auml;hlten Produkte bleiben in Deinem Warenkorb erhalten.</p>
<p>Wenn Du all Deine gew&uuml;nschten Produkte bestellen m&ouml;chtest, w&auml;hle vom Warenkorb aus den Button &bdquo;zur Kasse gehen&ldquo; an.</p>
<p>Registriere Dich mit Deinem bereits registrierten Login (Email und Passwort) oder melde Dich untenstehend als Neukunde an. [weiter]</p>
<p>&Uuml;berpr&uuml;fe Deine Liefer- und Rechnungadresse. [weiter]</p>
<p>Falls Du einen Gutschein zum Einl&ouml;sen hast, kannst Du die Nummer im entsprechenden Feld eingeben.</p>
<p>Die Zahlungsart wird Dir angezeigt und kann angew&auml;hlt werden.</p>
<p>Auf der Schluss&uuml;bersicht kannst Du noch einmal alle Daten &uuml;berpr&uuml;fen. Wenn Du mit Deiner Bestellung und mit den damit verbundenen AGB&rsquo;s einverstanden bist, klicke auf die AGB&rsquo;s um diese zu akzeptieren.</p>
</div>
<li class="title_paragraph toggle">Wie schnell erhalte ich meine Lieferung?</li>
<div class="text_wrap">
<p>Sofern Deine Bestellung bis um 09.00 Uhr aufgegeben ist, werden die bestellten Artikel innerhalb zwei Werkstagen geliefert.</p>
</div>
<li class="title_paragraph toggle">Wie l&ouml;se ich einen Wertgutschein ein?</li>
<div class="text_wrap">
<p>Im Warenkorn kannst Du den Rabatt- oder Geschenkgutschein Code eingeben.</p>
</div>
<li class="title_paragraph toggle">Welche Zahlungsm&ouml;glichkeiten habe ich?</li>
<div class="text_wrap">
<p>Deine Bestellung kannst Du wahlweise per Rechnung, mit der Postcard oder mit den Kreditkarten Visa und Martercard zahlen. Die gesamte Bestellung wird SSL-verschl&uuml;sselt &uuml;bertragen. Du kannst also bedenkenfrei pers&ouml;nliche Informationen wie Deine Kreditkartennummer eingeben.</p>
</div>
<li class="title_paragraph toggle">Wie hoch sind die Versandkosten?</li>
<div class="text_wrap">
<p>Die Portogeb&uuml;hren betragen CHF 7.- Ab 50.- Bestellsumme sind die Versandkosten gratis.</p>
</div>
</ul>';
    }

}
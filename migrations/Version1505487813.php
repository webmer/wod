<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505487813
 *
 * @package Migration
 */
class Version1505487813 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Competition')
            ->setIdentifier('competition')
            ->setContentHeading('Competition')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('competition')
            ->setIdentifier('competition')
            ->setContentHeading('competition')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return 'Competition form
Salutation
Mr.
Ms./Mrs.
First name
Surname
Zip
Place
Phone
Date of Birth
Street/No.
Addition
E-Mail
Send
Criteria
All mandatory fields must be filled.
Name: Only letters are allowed here
Street/No.: Letters, numbers and a full stop
Addition: Letters, number and special characters
Zip: Must be a four-digit number
Phone: Only numbers except the plus (+) for the area code
E-Mail: Needs an @ and a top-level-domain (eg. .ch/.de/.com/etc.)

Only numbers are allowed. Make a zero if you have a single-digit (eg. 05.03.1980) Enter day (0-31), month (0-12) and then the year (1900-1999).
I accept the terms and conditions of the competition
Mandatory field
';
    }

    private function getContentDe()
    {
        return 'Wettbewerbsformular
Anrede
Herr
Frau
Vorname
Nachname
PLZ
Ort
Telefon
Geburtsdatum
Strasse/Nr.
Zusatz
E-Mail
Senden
Vorgaben
Alle Pflichtfelder müssen ausgefüllt sein.
Vor- und Nachname: Hier sind nur Buchstaben erlaubt.
Strasse/Nr.: Buchstaben, Zahlen und ein Satzpunkt 
Zusatz: Buchstaben, Zahlen und Sonderzeichen 
PLZ: muss eine vierstellige Zahl sein 
Telefon: Nur Zahlen ausser dem Plus (+) für die Vorwahl
E-Mail: Braucht ein @ und eine Top-Level-Domain (Bsp. .ch/.de/.com/etc.) 
Geburtsdatum: Hier sind nur Zahlen erlaubt. Setze jeweils eine Null bei einstelligen Angaben (Bsp. 05.03.1980) Gib den Tag (0-31), den Monat (0-12) und dann das Jahr ein (1900-1999) 
Ich akzeptiere die Wettbewerbsbedingungen
Pflichtfeld';
    }

}
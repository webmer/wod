<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1504861906
 *
 * @package Migration
 */
class Version1504861906 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Impressum')
            ->setIdentifier('impressum')
            ->setContentHeading('Impressum')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('Impressum')
            ->setIdentifier('impressum')
            ->setContentHeading('Impressum')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p class="address_wrapper">Genusswelt AG <br/>
World of Delight<br/>
Bahnhofstrasse 28<br/>
6300 Zug<br/>
kontakt(at)genusswelt.ch</p>
<p class="company_data">Company no.: CHE-116.233.881<br/>
VAT: CHE-116.233.881 MWST</p>
<span class="title_paragraph">Disclaimer</span>
<p>The author does not guarantee accuracy, precision, up-to-dateness, reliability, and completeness of the information regarding the content. Liability claims against the author due to damage, immaterial or material, which arose due to access or usage respectively not usage of the released information, through misuse of the connection or technical difficulties, are excluded. All offers are not binding. The author reserves the right to change, complement, delete, and/or stop the publication partly or forever for parts of the page or the whole offer.</p>
<span class="title_paragraph">Liability for links</span>
<p>Reference and links to third party websites are out of our area of responsibility. Liability is declined for those websites. Access and usage of such websites are at one’s own risk.
<span class="title_paragraph">Copyright</span>
<p>The copyright and all other rights to content, images, photos, or other files on the website belong to World of Delight or the explicitly named holder of rights. For reproduction of any element a written agreement of the holder of rights must be obtained in advantage.</p>

<span class="title_paragraph">Privacy protection</span>
<p>Based on article 13 of the Swiss constitution and the privacy policy of the Swiss Federal Council (Data Protection Act, DPA), every person has the right to protection of their privacy as well as protection of misuse of their personal data. We adhere to these regulations. Personal data is handled strictly confidential and be given or sold to third parties. We are making an effort, in close collaboration with our hosting providers, to protect the databases as good as possible from external access, lost, misuse, or forgery. The following data is saved when our website is accessed: IP-address, date, time, browser-inquiry, and general transmitted information about the operating system respectively the browser. This user data are basis for statistic, anonymous analysis to identify trends to which we adapt our offer.</p>';
    }

    private function getContentDe()
    {
        return '<p class="address_wrapper">Genusswelt AG<br/>
World of Delight<br/>
Bahnhofstrasse 28<br/>
6300 Zug<br/>
kontakt(at)genusswelt.ch</p>
<p class="company_data">Firmennummer: CHE-116.233.881 <br/>
MWST: CHE-116.233.881 MWST</p>
<span class="title_paragraph">Haftungsausschluss</span>
<p>Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und Vollständigkeit der Informationen. Haftungsansprüche gegen den Autor wegen Schäden materieller oder immaterieller Art, welche aus dem Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der Verbindung oder durch technische Störungen entstanden sind, werden ausgeschlossen. Alle Angebote sind unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.</p>
<span class="title_paragraph">Haftung für Links</span>
Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres Verantwortungsbereichs. Es wird jegliche Verantwortung für solche Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten erfolgen auf eigene Gefahr des Nutzers oder der Nutzerin.</p>
<span class="title_paragraph">Urheberrechte</span>
Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf der Website gehören ausschliesslich der Firma World of Delight oder den speziell genannten Rechtsinhabern. Für die Reproduktion jeglicher Elemente ist die schriftliche Zustimmung der Urheberrechtsträger im Voraus einzuholen.</p>
<span class="title_paragraph">Datenschutz</span>
Gestützt auf Artikel 13 der schweizerischen Bundesverfassung und die datenschutzrechtlichen Bestimmungen des Bundes (Datenschutzgesetz, DSG) hat jede Person Anspruch auf Schutz ihrer Privatsphäre sowie auf Schutz vor Missbrauch ihrer persönlichen Daten. Wir halten diese Bestimmungen ein. Persönliche Daten werden streng vertraulich behandelt und weder an Dritte verkauft noch weitergegeben. In enger Zusammenarbeit mit unseren Hosting-Providern bemühen wir uns, die Datenbanken so gut wie möglich vor fremden Zugriffen, Verlusten, Missbrauch oder vor Fälschung zu schützen. Beim Zugriff auf unsere Webseiten werden folgende Daten in Logfiles gespeichert: IP-Adresse, Datum, Uhrzeit, Browser-Anfrage und allg. übertragene Informationen zum Betriebssystem resp. Browser. Diese Nutzungsdaten bilden die Basis für statistische, anonyme Auswertungen, so dass Trends erkennbar sind, anhand derer wir unsere Angebote entsprechend verbessern können</p>';
    }

}
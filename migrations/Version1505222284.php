<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505222284
 *
 * @package Migration
 */
class Version1505222284 implements Migration
{
    /**
     * Version1505222284 constructor.
     * Inject Dependency
     *
     */
    public function __construct()
    {
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $identifier = 'block_footer_information';
        $store_id = 2;
        $block = $objectManager->create('Magento\Cms\Model\Block');
        $block->setStoreId($store_id); // store for block you want to update
        $block->load($identifier, 'identifier');
        $block->setIdentifier($identifier);
        $block->setTitle('block_footer_information');
        $block->setIsActive(1);
        $block->setStores($store_id);
        $block->setContent($this->getContentEn());
        $block->save();

        $content = '';
        $store_id = 1;
        $block = $objectManager->create('Magento\Cms\Model\Block');
        $block->setStoreId($store_id); // store for block you want to update
        $block->load($identifier, 'identifier');
        $block->setIdentifier($identifier);
        $block->setTitle('block_footer_information');
        $block->setIsActive(1);
        $block->setStores($store_id);
        $block->setContent($this->getContentDe());
        $block->save();
    }


    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentDe()
    {
        return '<div class="contacts">
    <span class="footer_title toggle">Kontakt</span>
    <div class="footer_info_wrap">
        <span>Kontakt : </span>
        <p class="street-address">Genusswelt AG <br/> Bahnhofstrasse 28<br/> 6300 Zug</p>
        <span>Address Data : </span>
        <div class="phone-holder">
            <p class="tel">Tel 041 511 51 00</p>
            <p class="email">info@genusswelt.ch</p>
        </div>
    </div>
</div>
<div class="information"><span class="footer_title toggle">Informationen</span>
    <ul class="info_list footer_info_wrap">
        <li><a href="/about-us">Über Uns</a></li>
        <li><a href="/impressum">Impressum</a></li>
        <li><a href="/agb-und-datenschutz">AGB und Datenschutz</a></li>
        <li><a href="/advertising">Werbung</a></li>
    </ul>
</div>
<div class="support"><span class="footer_title toggle">Support</span>
    <ul class="info_list footer_info_wrap">
        <li><a href="/faq">FAQ</a></li>
        <li><a href="/order-help">Bestellhilfe</a></li>
    </ul>
</div>
<div class="service"><span class="footer_title toggle">Service</span>
    <ul class="info_list footer_info_wrap">
        <li><a href="/voucher">Gutscheine</a></li>
        <li><a href="/delivery">Lieferservice</a></li>
        <li><a href="/cigar-event-service">Zigarren Event Service</a></li>
        <li><a href="/wettbewerb">Wettbewerb</a></li>
    </ul>
</div>
<div class="payments"><span class="footer_title toggle">Zahlungsarten</span>
    <div class="footer_info_wrap">
        <p>Bezahlung mit Kreditkarten, PostCard <br/> oder auf Rechnung by PowerPay</p>
        <div class="payment-icons">
            <img src="{{media url="wysiwyg/payments.png"}}" alt="" />
            <img src="{{media url="wysiwyg/payments2_3_.png"}}" alt="" />
        </div>
    </div>
</div>
<div class="socials-wrap">
    <span class="footer_title toggle">Social media</span>
    <div class="footer_info_wrap">

        <a href="#" class="social-link facebook">
            <span class="fa fa-facebook"></span>
        </a>
        <a href="#" class="social-link tumblr">
            <span class="fa fa-tumblr"></span>
        </a>
        <a href="#" class="social-link pinterest">
            <span class="fa fa-pinterest"></span>
        </a>
        <a href="#" class="social-link twitter">
            <span class="fa fa-twitter"></span>
        </a>

    </div>
</div>';
    }

    private function getContentEn()
    {
        return '<div class="contacts">
                <span class="footer_title toggle">Contact</span>
                <div class="footer_info_wrap">
                <span>Contact : </span>
                <p class="street-address">Genusswelt AG <br /> Bahnhofstrasse 28<br /> 6300 Zug</p>
                <span>Address Data : </span>
                <div class="phone-holder">
                    <p class="tel">Tel 041 511 51 00</p>
                    <p class="email">info@genusswelt.ch</p>
                </div>
                </div>
                </div>
                <div class="information"><span class="footer_title toggle">Information</span>
                <ul class="info_list footer_info_wrap">
                <li><a href="/about-us">About us</a></li>
                <li><a href="/impressum">Legal notice</a></li>
                <li><a href="/agb-und-datenschutz">Terms and conditions, and privacy</a></li>
                <li><a href="/advertising">Advertising</a></li>
                </ul>
                </div>
                <div class="support"><span class="footer_title toggle">Support</span>
                <ul class="info_list footer_info_wrap">
                <li><a href="/faq">FAQ</a></li>
                <li><a href="/order-help">Order Help</a></li>
                </ul>
                </div>
                <div class="service"><span class="footer_title toggle">Service</span>
                <ul class="info_list footer_info_wrap">
                <li><a href="/voucher">Voucher</a></li>
                <li><a href="/delivery">Delivery</a></li>
                <li><a href="/cigar-event-service">Cigar Event Service</a></li>
                <li><a href="/wettbewerb">Competition</a></li>
                </ul>
                </div>
                <div class="payments"><span class="footer_title toggle">Payment</span>
                <div class="footer_info_wrap">
                <p>Pay with credit card, PostCard <br /> or by invoice by PowerPay</p>
                <div class="payment-icons">
                <img src="{{media url="wysiwyg/payments.png"}}" alt="" /> 
                <img src="{{media url="wysiwyg/payments2_3_.png"}}" alt="" />
                </div>
                </div>
                </div>
                <div class="socials-wrap">
                <span class="footer_title toggle">Social media</span>
                <div class="footer_info_wrap">
                <a href="#" class="social-link facebook">
                <span class="fa fa-facebook"></span>
                </a>
                <a href="#" class="social-link tumblr">
                <span class="fa fa-tumblr"></span>
                </a>
                <a href="#" class="social-link pinterest">
                <span class="fa fa-pinterest"></span>
                </a>
                <a href="#" class="social-link twitter">
                <span class="fa fa-twitter"></span>
                </a>
                </div>
                </div>';
    }

}


<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class Version1507020188
 *
 * @package Migration
 */
class Version1507020188 implements Migration
{
    private $eavSetupFactory;

    private $categorySetupFactory;

    private $attributeSetCollection;
    private $setup;
    private $context;
    /**
     * Version1507020188 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        ModuleDataSetupInterface $setup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetCollection = $attributeSetCollection;
        $this->setup = $setup;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $this->installAttrs($this->setup,$this->context);
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    public function getAttrSetId($attrSetName)
    {
        $attributeSet = $this->attributeSetCollection->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'attribute_set_name',
            $attrSetName
        );
        $attributeSetId = 0;
        foreach($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }

    protected function _installerAttribute($entityTypeId,$attributesData = array(),$eavSetup,$attributeGroupName,$attributeSetIdArr)
    {
        $this->setup->startSetup();

        foreach ($attributesData as $attributeData) {
            $attributeCode = $attributeData['code'];
            $attributeLabel = $attributeData['label'];
            $order = $attributeData['order'];
            $source = isset($attributeData['source']) ? $attributeData['source'] : '';
            $type = isset($attributeData['type']) ? $attributeData['type'] : 'text';
            $input = isset($attributeData['input']) ? $attributeData['input'] : 'text';
            $backend = isset($attributesData['backend']) ? $attributesData['backend'] : '';
            $options = '';
            if(isset($attributesData['options'])){
                $options = [
                    'values' => [$attributesData['options']]
                ];
            }

            $eavSetup->removeAttribute($entityTypeId, $attributeCode);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'type' => $type,
                    'backend' => $backend,
                    'frontend' => '',
                    'label' => $attributeLabel,
                    'input' => $input,
                    'wysiwyg_enabled' => true,
                    'frontend_class' => '',
                    'source' => $source,
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    //'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
                    'apply_to' => '',
//                    'group' => $attributeGroupName
                    'option' => $options
                ]
            );
            foreach ($attributeSetIdArr as $attributeSetId) {
                $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupName, $attributeCode, $order);
            }
        }

        $this->setup->endSetup();
    }

    public function installAttrs(
        ModuleDataSetupInterface $setup
    ) {

        $attributeGroupName = 'Attributes';
        /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        $attrSetNameArr = array("kaffee");
        $attributeSetIdArr = [];

        foreach ($attrSetNameArr as $attrCode){
            $attributeSetIdArr[] = $this->getAttrSetId($attrCode);
        }

        $attributesData = [
            ['code' => 'wod_cupsize', 'label' => 'Tassengrösse', 'order' => 58, 'input' => 'multiselect']
        ];
        //$this->_installerAttribute($entityTypeId,$attributesData,$eavSetup,$attributeGroupName,$attributeSetIdArr);
        foreach ($attributesData as $attributeData) {
            $attributeCode = $attributeData['code'];
            $attributeLabel = $attributeData['label'];
            $order = $attributeData['order'];
            $source = isset($attributeData['source']) ? $attributeData['source'] : '';
            $type = isset($attributeData['type']) ? $attributeData['type'] : 'text';
            $input = isset($attributeData['input']) ? $attributeData['input'] : 'text';
            $backend = isset($attributesData['backend']) ? $attributesData['backend'] : '';
            $options = '';
            if(isset($attributesData['options'])){
                $options = [
                    'values' => [$attributesData['options']]
                ];
            }

            $eavSetup->removeAttribute($entityTypeId, $attributeCode);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'type' => $type,
                    'backend' => $backend,
                    'frontend' => '',
                    'label' => $attributeLabel,
                    'input' => $input,
                    'wysiwyg_enabled' => true,
                    'frontend_class' => '',
                    'source' => $source,
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    //'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
                    'apply_to' => '',
//                    'group' => $attributeGroupName
                    'option' => [
                        'values' => ['Ristretto', 'Espresso', 'Kaffee','Milchkaffee']
                    ]
                ]
            );
            foreach ($attributeSetIdArr as $attributeSetId) {
                $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupName, $attributeCode, $order);
            }
        }
    }
}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1509707854
 *
 * @package Migration
 */
class Version1509707854 implements Migration
{
    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    private $resourceConfig;
    /**
     * Version1509707854 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $info = array(
            'aminvisiblecaptcha/general/enabledCaptcha'=>'1',
            'aminvisiblecaptcha/general/captchaKey'=>'6LcdATcUAAAAAI7KGO0f6yD2wIEjY67Hme4PCI_-',
            'aminvisiblecaptcha/general/captchaSecret'=>'6LcdATcUAAAAAJN3tqmL5PK80QmlRYgTnGGXMs25',
            'aminvisiblecaptcha/general/captchaSelectors'=>'.wettbewerb-form',
            'aminvisiblecaptcha/general/captchaUrls'=>'wettbewerb
wettbewerb/index'
        );

        foreach ($info as $key =>$value) {
            $this->resourceConfig->saveConfig(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
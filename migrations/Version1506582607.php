<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class Version1506582607
 *
 * @package Migration
 */
class Version1506582607 implements Migration
{
    private $eavSetupFactory;

    private $categorySetupFactory;

    private $attributeSetCollection;
    private $setup;
    private $context;
    /**
     * Version1506582607 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        ModuleDataSetupInterface $setup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetCollection = $attributeSetCollection;
        $this->setup = $setup;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $this->install($this->setup,$this->context);
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    public function install(
        ModuleDataSetupInterface $setup
    ) {

        $attributeGroupName = 'Attributes';
        $attributesData = [
            ['code' => 'inhalt', 'label' => 'Inhalt', 'order' => 51],
            ['code' => 'zusatzinformationen', 'label' => 'Zusatzinformationen', 'order' => 52],
            ['code' => 'lieferzeit', 'label' => 'Lieferzeit', 'order' => 53],
        ];
        /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $attrSetName = 'tabak';
        $attrSetNameArr = ["tabak","kaffee","süssigkeiten","getränke"];
        $attributeSetIdArr = [];
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $this->getAttrSetId($attrSetName);
        foreach ($attrSetNameArr as $attrCode){
            $attributeSetIdArr[] = $this->getAttrSetId($attrCode);
        }

        foreach ($attributesData as $attributeData) {
            $attributeCode = $attributeData['code'];
            $attributeLabel = $attributeData['label'];
            $order = $attributeData['order'];
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => $attributeLabel,
                    'input' => 'text',
                    'wysiwyg_enabled' => true,
                    'frontend_class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => '',
                    'group' => $attributeGroupName
                ]
            );
            foreach ($attributeSetIdArr as $attributeSetId) {
                $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupName, $attributeCode, $order);
            }
        }

        $attributesData = [
            ['code' => 'show_desc', 'label' => 'Show Description', 'order' => 100]
        ];

        foreach ($attributesData as $attributeData) {
            $attributeCode = $attributeData['code'];
            $attributeLabel = $attributeData['label'];
            $order = $attributeData['order'];
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'group' => 'General',
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => $attributeLabel,
                    'input' => 'boolean',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
                ]
            );
            $attributeGroupName = 'General';
            foreach ($attributeSetIdArr as $attributeSetId) {
                $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupName, $attributeCode, $order);
            }
        }
    }

    public function getAttrSetId($attrSetName)
    {
        $attributeSet = $this->attributeSetCollection->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'attribute_set_name',
            $attrSetName
        );
        $attributeSetId = 0;
        foreach($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }
}
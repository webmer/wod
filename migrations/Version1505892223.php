<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class Version1505892223
 *
 * @package Migration
 */
class Version1505892223 implements Migration
{
    private $eavSetupFactory;

    private $categorySetupFactory;

    private $attributeSetCollection;
    private $setup;
    private $context;

    /**
     * Version1505892223 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        ModuleDataSetupInterface $setup
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetCollection = $attributeSetCollection;
        $this->setup = $setup;

    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {

        $this->install($this->setup,$this->context);

    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    public function install(
        ModuleDataSetupInterface $setup
    ) {

        $attributeGroupName = 'General';
        $attributesData = [
            ['code' => 'content', 'label' => 'Content', 'order' => 50]
        ];
        /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $attrSetName = 'tabak';
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $this->getAttrSetId($attrSetName);



        foreach ($attributesData as $attributeData) {
            $attributeCode = $attributeData['code'];
            $attributeLabel = $attributeData['label'];
            $order = $attributeData['order'];
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeCode,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => $attributeLabel,
                    'input' => 'text',
                    'wysiwyg_enabled' => true,
                    'frontend_class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );
            $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupName, $attributeCode, $order);
        }


    }

    public function getAttrSetId($attrSetName)
    {
        $attributeSet = $this->attributeSetCollection->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'attribute_set_name',
            $attrSetName
        );
        $attributeSetId = 0;
        foreach($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }
}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Version1509469338
 *
 * @package Migration
 */
class Version1509469338 implements Migration
{
    /**
     * @var BlockFactory
     */
    protected $_blockFactory;

    /**
     * Version1509469338 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        BlockFactory $blockFactory
    )
    {
        $this->_blockFactory = $blockFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $blockData = [
            'title' => 'wettbewerb content before form',
            'identifier' => 'plg_wettbewerb',
            'stores' => [1],
            'is_active' => 1,
            'content' => $this->getContentData()
        ];

        $this->_blockFactory->create()->setData($blockData)->save();

        $blockData2 = [
            'title' => 'wettbewerb modal content',
            'identifier' => 'plg_wettbewerb_modal',
            'stores' => [1],
            'is_active' => 1,
            'content' => $this->getContentModal()
        ];

        $this->_blockFactory->create()->setData($blockData2)->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentData()
    {
        return '<div class="wettbewerb_image_wrap">
        <p class="top">Beispielbild wettbewerb</p>
        <p class="bottom small_txt">Gewinne etwas</p>
        </div>
        <p>Gewinne eine Reise nach Dubai für </p>
        <p>zwei Personen im Wert von CHF 5’000.- als Hauptgewinn.</p>
        <p>Erlebe bei dieser Luxusreise die Metropole der Superlative! Geniesse die</p>
        <p>Sonne und die Strände. Besuche die moderne Architekturen und erkunde die Geschichte und die arabische Tradition.</p>
        <p class="small_txt">Zusätzlich, verlosen wir jeden Monat, unter den Teilnehmern 50 MOODS-SWEET-PACKUNGEN.</p>
        <p class="small_txt">Gewinne eine Reise nach Dubai für zwei Personen im Wert von CHF</p>
        ';
    }

    private function getContentModal()
    {
        return '<h2>wettbewerb modal content</h2>
        <p><span>Die Teilnahme am Wettbewerb ist nicht an einen Kauf gebunden. Die Gewinner werden schriftlich benachrichtigt.</span><br><span>Teilnahmeschluss ist der 30. November 2017. Der Rechtsweg ist ausgeschlossen. Über die Verlosung wird keine Korrespondenz geführt. Der Preis kann nicht umgetauscht oder in bar ausbezahlt werden. Nur eine Teilnahme pro Person. Adressen können für eigene Marketingaktivitäten von smoke24.ch und Burger Söhne AG verwendet werden.</span><br><span>Nur für Teilnehmende ab 18 Jahren mit Wohnsitz in der Schweiz und in Liechtenstein sind Teilnahmeberechtigt.</span><br><span>Zu verlosen:</span><br><span>Hauptpreis: Eine Reise für zwei Personen nach Dubai im Wert von CHF 5\'000.-, 5 Tage für 2 Personen mit Übernachtung und Flug. Die Reise muss in einem Zeitraum von einem halben Jahr bezogen werden.</span><br><span>Jeden Monat verlosen wir zusätzlich 50 MOODS-SWEET-Packungen.</span><br><span>Mit der Inanspruchnahme des Gewinns verbundene Zusatzkosten gehen zu Lasten des Gewinners. Für eine Etwaige Versteuerung des Gewinns, ist der Gewinner selbst verantwortlich. Meldet sich der Gewinner nach zweifacher Aufforderung innerhalb einer Frist von 3 Wochen nicht, kann der Gewinn auf einen anderen Teilnehmer übertragen werden.</span></p>
        ';
    }
}
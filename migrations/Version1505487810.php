<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505487810
 *
 * @package Migration
 */
class Version1505487810 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Lieferservice')
            ->setIdentifier('delivery')
            ->setContentHeading('Lieferservice')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('Delivery service')
            ->setIdentifier('delivery')
            ->setContentHeading('Delivery service')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p >You may choose between two hauliers</p>
<div class="delivery_img_wrap"></div>
<p>Decide yourself, when and where you receive to delivery.</p>

<div class="desktop_image_wrap">
<div class="desktop_image"></div>
<p>Order your preferred products on worldofdelight.ch</p>
</div>

<div class="tablet_image_wrap">
<div class="tablet_image"></div>
<p>As soon as your package is ready to ship you will receive an e-mail or text. In there you choose one of three delivery times and your preferred location.
</p>
</div>

<div class="mobile_image_wrap">
<div class="mobile_image"></div>
<p>On the day of delivery, you will receive a message which informs you about the period when your package is delivered.</p>
</div>';
    }

    private function getContentDe()
    {
        return '<p>Du kannst zwischen zwei verschiedenen Spediteuren wählen</p>
<div class="delivery_img_wrap"></div>
<p>Entscheide selbst, wann und wo geliefert wird.</p>

<div class="desktop_image_wrap">
<div class="desktop_image"></div>
<p class="order_data_wrap">Bestelle Deinen Lieblingsartikel auf worldofdelight.ch
</p>
</div>

<div class="tablet_image_wrap">
<div class="tablet_image"></div>
<p>Sobald das Paket versandbereit ist, erhaltest du eine E-Mail oder SMS. Darin wirst Du über drei mögliche Zustelltermine informiert und kannst selber entscheiden, wann und wo geliefert werden soll.
</p>
</div>

<div class="mobile_image_wrap">
<div class="mobile_image"></div>
<p>Am Tag der Zustellung erhaltest Du eine Nachricht, die dich kurz informiert, in welchem Zeitraum das Paket geliefert wird.</p>
</div>';
    }

}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

/**
 * Class Version1513775215
 *
 * @package Migration
 */
class Version1513775215 implements Migration
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    private $setup;

    private $eavSetupFactory;

    /**
     * Version1513775215 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        ModuleDataSetupInterface $setup
    ) {
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->setup = $setup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $this->install($this->setup);
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    public function install(
        ModuleDataSetupInterface $setup
    ) {
        $usedInForms = ['adminhtml_customer','customer_account_create', 'customer_account_edit', 'checkout_register'];
        /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $customerEntityID = \Magento\Customer\Model\Customer::ENTITY;
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        /*$customerSetup->removeAttribute($customerEntityID, 'wod_company');
        $customerSetup->removeAttribute($customerEntityID, 'wod_titel');

        $customerSetup->addAttribute($customerEntityID, 'wod_company', [
            'type' => 'varchar',
            'label' => 'Company',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1000,
            'position' => 1000,
            'system' => 0,
        ]);

        $attribute = $customerSetup->getEavConfig()->getAttribute($customerEntity, 'wod_company')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => $usedInForms
            ]);
        $attribute->save();

        $customerSetup->addAttribute($customerEntityID, 'wod_titel', [
            'type' => 'varchar',
            'label' => 'Titel',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1000,
            'position' => 1000,
            'system' => 0,
        ]);

        $attribute = $customerSetup->getEavConfig()->getAttribute($customerEntity, 'wod_titel')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => $usedInForms
            ]);
        $attribute->save();*/

        $attributesInfo = [
            'wod_company' => [
                'type' => 'varchar',
                'label' => 'Company',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 500,
                'position' => 500,
                'system' => 0,
                'disabled' => true
            ],
            'wod_titel' => [
                'type' => 'varchar',
                'label' => 'Titel',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 505,
                'position' => 505,
                'system' => 0,
            ],
            'wod_ship' => [
                'type' => 'varchar',
                'label' => 'Default Shipping',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 510,
                'position' => 510,
                'system' => 0,
                'readonly' => true
            ],
            'wod_payment' => [
                'type' => 'varchar',
                'label' => 'Default Payment',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 515,
                'position' => 515,
                'system' => 0,
                'readonly' => true
            ]
        ];
        foreach ($attributesInfo as $attributeCode => $attributeParams) {
            $customerSetup->addAttribute($customerEntityID, $attributeCode, $attributeParams);
            $attribute = $customerSetup->getEavConfig()
                ->getAttribute($customerEntityID, $attributeCode)
                ->addData(
                    [
                        'attribute_set_id' => $attributeSetId,
                        'attribute_group_id' => $attributeGroupId,
                        'used_in_forms'=> $usedInForms
                    ]
                );
            $attribute->save();
        }

        $setup->endSetup();
    }
}
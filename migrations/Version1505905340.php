<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505905340
 *
 * @package Migration
 */
class Version1505905340 implements Migration
{
    /**
     * Version1505905340 constructor.
     * Inject Dependency
     *
     */
    public function __construct() { }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $categories = array(
          'Kaffee',
          'Süssewaren',
          'Getränke',
          'Aktionen',
        );

        $parentId = 2;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach ($categories as $item){
        $parentCategory = $objectManager
            ->create('Magento\Catalog\Model\Category')
            ->load($parentId);
        $category = $objectManager
            ->create('Magento\Catalog\Model\Category');
        $cate = $category->getCollection()
            ->addAttributeToFilter('name',$item)
            ->getFirstItem();
        if(!$cate->getId()) {
            $category->setPath($parentCategory->getPath())
                ->setParentId($parentId)
                ->setName($item)
                ->setDescription($item)
                ->setIsActive(true);
            $category->save();
        }
        }

    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Store;


/**
 * Class Version1504080236
 *
 * @package Migration
 */
class Version1504080236 implements Migration
{
    const THEME_NAME = 'Alothemes/wod';

    /**
     * @var \Magento\Setup\Module\Setup
     */
    private $setup;

    /**
     * @var \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Theme\Model\Config
     */
    private $config;

    /**
     * Version1504080236 constructor.
     *
     * @param \Magento\Setup\Module\Setup $setup
     * @param \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory
     * @param \Magento\Theme\Model\Config $config
     */
    public function __construct(
        \Magento\Setup\Module\Setup $setup,
        \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory,
        \Magento\Theme\Model\Config $config
    )
    {
        $this->setup = $setup;
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $setup = $this->setup;

        $setup->startSetup();

        $this->assignTheme();

        $setup->endSetup();
    }

    /**
     * Assign Theme
     *
     * @return void
     */
    protected function assignTheme()
    {
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();

        /**
         * @var \Magento\Theme\Model\Theme $theme
         */
        foreach ($themes as $theme) {
            if ($theme->getCode() == self::THEME_NAME) {
                $this->config->assignToStore(
                    $theme,
                    [Store::DEFAULT_STORE_ID],
                    ScopeConfigInterface::SCOPE_TYPE_DEFAULT
                );
            }
        }
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {

    }
}
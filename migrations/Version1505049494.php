<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class Version1505049494
 *
 * @package Migration
 */
class Version1505049494 implements Migration
{
    private $resourceConfig;
    /**
     * Version1505049494 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        ConfigInterface $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {

        $this->resourceConfig->saveConfig(
            'general/locale/code',
            'de_DE',
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
        $this->resourceConfig->saveConfig(
            'general/locale/code',
            'de_DE',
            'stores',
            '1'
        );
        $this->resourceConfig->saveConfig(
            'general/locale/code',
            'en_US',
            'stores',
            '2'
        );
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1504534408
 *
 * @package Migration
 */
class Version1504534408 implements Migration
{
    /**
     * Version1504534408 constructor.
     * Inject Dependency
     *
     */
    public function __construct() { }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $parentId = 2;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $parentCategory = $objectManager
            ->create('Magento\Catalog\Model\Category')
            ->load($parentId);
        $category = $objectManager
            ->create('Magento\Catalog\Model\Category');
        $cate = $category->getCollection()
            ->addAttributeToFilter('name','Tabak')
            ->getFirstItem();
        if(!$cate->getId()) {
            $category->setPath($parentCategory->getPath())
                ->setParentId($parentId)
                ->setName('Tabak')
                ->setDescription('Tabak')
                ->setIsActive(true);
            $category->save();
        }


    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
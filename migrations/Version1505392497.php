<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505392497
 *
 * @package Migration
 */
class Version1505392497 implements Migration
{

    /**
     * Version1505392497 constructor.
     * Inject Dependency
     *
     */
    public function __construct(

    ) {

    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $object_manager =\Magento\Framework\App\ObjectManager::getInstance();
        $dir_list = $object_manager->get('\Magento\Framework\App\Filesystem\DirectoryList');
        $filePath = $dir_list->getRoot().'/brandsimport/brands.csv';
        $brandsArray = $this->getBrandsArray($filePath);
        foreach ($brandsArray as $item ) {
            $model = $object_manager->create('Meridian\TopMarken\Model\TopMarken');
                $model->setData($item);
                $model->save();
//            }
        }


    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getBrandsArray($file){

        $handle = @fopen( $file, "r");
        if ( !$handle ) {
            throw new \Exception( "Couldn't open $file!" );
        }
        $result = [];
        $first = strtolower( fgets( $handle, 4096 ) );
        $keys = str_getcsv( $first);
        while ( ($buffer = fgets( $handle, 4096 )) !== false ) {
            $array = str_getcsv ( $buffer );
            if ( empty( $array ) ) continue;
            $row = [];
            $i=0;
            foreach ( $keys as $key ) {
                $row[ $key ] = $array[ $i ];
                $i++;
            }

            $result[] = $row;
        }

        fclose( $handle );
        return $result;

    }
}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1507733905
 *
 * @package Migration
 */
class Version1507733905 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1507733905 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Bestellhilfe')
            ->setIdentifier('order-help')
            ->setContentHeading('Bestellhilfe')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('Support to order')
            ->setIdentifier('order-help')
            ->setContentHeading('Support to order')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '<p>If you are having trouble with the buying process you can find a standard solution here. If that does not help you may call us (+41 41 511 51 00) or we are happy to hear what made it difficult via email (kontakt(at)worldofdelight.ch)<br />
</p>
<span class="title_paragraph">Filling your cart</span><br />
<p>If logged in or not, clicking the &laquo;add to cart&raquo; button next to the product puts the previously entered amount in your cart. When you collected enough you may click the cart in the top right corner of the screen to go to your cart.<br />
</p>
<span class="title_paragraph">Cart overview</span><br />
<p>
Here you may check your items and make changes if necessary. Further on this site you have the possibility to make an standing order if you would like to receive your order regularly, or enter a coupon / voucher code if you received one. When all that is done you may click &laquo;go to checkout&raquo;.</p>
<span class="title_paragraph">Check out &ndash; part 1</span><br />
<p>If your logged in you may choose your shipping address or enter a new one. If your shopping as a guest you must enter your shipment details in here. In both cases you may choose your preferred carrier at the bottom. <br />
</p>
<span class="title_paragraph">Check out &ndash; part 2</span><br />
<p>It your billing address is different from your shipping address you may enter it here. Further, you have the possibility to enter a coupon / voucher code and check your order, in the box on the right, one last time. By clicking &laquo; Confirm order&raquo; it is sent to us.</p>
<span class="title_paragraph">Confirmation</span><br />
<p>You now receive an order number. The same number is sent to you by e-mail and if your logged in saved in your account in the orders section. Do not lose it, if there is any problem with your order it will be very helpful to resolve any issues.</p>';
    }

    private function getContentDe()
    {
        return '<p>Wenn dir der Bestellablauf M&uuml;he bereitet findest du hier eine Anleitung zum Standardablauf. Wenn das auch nichts bringt helfen wir dir auch gerne am Telefon (+41 41 511 51 00) oder freuen uns zu erfahren, was schwierig verst&auml;ndlich ist (kontakt(at)worldofdelight.ch).<br />
</p>
<span class="title_paragraph">
Den Warenkorb bef&uuml;llen</span><br />
<p>
Ob angemeldet oder nicht, ein Klick auf die &laquo;Kaufen&raquo;-Buttons bei den Produkten legt die davor angegebene Anzahl in den Warenkorb. Hast du genug gesammelt, kannst du mit einem Klick auf den Warenkorb oben rechts ( ) zur &Uuml;bersicht des Warenkorbs gelangen.<br />
</p>
<span class="title_paragraph">Die Warenkorb &Uuml;bersicht</span><br />
<p>Hier kannst du noch einmal deine Eink&auml;ufe &uuml;berpr&uuml;fen und gegebenenfalls die Anzahl der einzelnen Produkte anpassen. Weiter hast du auf dieser Seite noch die M&ouml;glichkeit ein Abo abzuschliessen, falls du die Bestellung regelm&auml;ssig erhalten m&ouml;chtest, oder einen Promo-Code einl&ouml;sen, solltest du einen solchen erhalten haben. Stimmt alles, geht der Bestellvorgang weiter mit einem Klick auf &laquo;zur Kasse gehen&raquo;<br />
</p>
<span class="title_paragraph">Die Kasse - Schritt 1</span><br />
<p>Falls du angemeldet bist, kannst du eine deiner Versandadressen ausw&auml;hlen oder eine neue hinzuf&uuml;gen. Wenn du als Gast einkaufst musst du hier deine Versandadresse eingeben. In beiden F&auml;llen kannst du darunter noch deine Versandart ausw&auml;hlen<br />
</p>
<span class="title_paragraph">Die Kasse &ndash; Schritt 2</span><br />
<p>Sollte deine Rechnungsadresse von der Lieferadresse abweichen kannst du diese hier erfassen. Zudem hast du nochmals die M&ouml;glichkeit einen Rabattcode anzuwenden und ein letztes Mal, in der Box rechts, deinen Einkauf zu pr&uuml;fen. Mit dem Klick auf &laquo;Bestellen&raquo; wird deine Bestellung an uns zugestellt.</p>
<span class="title_paragraph">Die Bestätigung</span>
<p>Du erhältst nun noch eine Bestellnummer. Die selbe wird dir auch per E-Mail geschickt, und falls angemeldet, erscheint in deinem Konto unter «Meine Bestellungen». Verlier sie nicht, im Falle, dass es irgendein Problem mit deiner Bestellung gäbe, wird es so leichter herauszufinden worum es geht.
</p>';
    }

}
<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1505487798
 *
 * @package Migration
 */
class Version1505487798 implements Migration
{
    /**
     * @var \Magento\Cms\Model\PageFactory
     *
     */
    protected $_pageFactory;

    /**
     * Version1504858700 constructor.
     * Inject Dependency
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $page = $this->_pageFactory->create();
        $page->setTitle('Zigarren Event Service')
            ->setIdentifier('cigar-event-service')
            ->setContentHeading('Zigarren Event Service')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(1))
            ->setContent($this->getContentDe())
            ->save();

        $page = $this->_pageFactory->create();
        $page->setTitle('Cigar Event Service')
            ->setIdentifier('cigar-event-service')
            ->setContentHeading('Cigar Event Service')
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores(array(2))
            ->setContent($this->getContentEn())
            ->save();
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getContentEn()
    {
        return '
<p class="cigarette_image_wrap"></p>
<p lang="en-GB">Invite your guests to enjoy and linger. Provide a very special experience.</p>
<span class="title_paragraph">Humidor/cigar rental service</span>
<p lang="en-GB">Are you planning a wedding, company event, or just a bigger party and need a big number of diverse cigars for an enjoyable event?</p>
<p lang="en-GB">We offer you a filled humidor with a wide variety of cigars, compiled as you wish. Only the cigars that are smoked will be charged.</p>
<p lang="en-GB">Discover our extensive humidor/cigar rental service and a lot more!</p>
<span class="title_paragraph">Cigar sommelier</span>
<p lang="en-GB">Our cigar sommelier will advise your guests friendly and competent, and tell them about the various unique features of the individual products.</p>
<p lang="en-GB">A cigar sommelier can recognize and describe the various shades of the taste. As an expert, he will help you guests find the proper cigar in the proper size. Further, he will provide clues on how to handle and store cigars correctly.</p>
<span class="title_paragraph">Cigar hostess</span>
<p lang="en-GB">Spirited, active, competent, experienced, and friendly in handling your guests. The fascinating attitude to life from composure and impulsiveness will &ldquo;infect&rdquo; your guests.</p>
<p lang="en-GB">If galas, exclusive company events, or gourmet events, our team strikes the right note and celebrates the cutting and lighting of a long filler cigar. Gladly also at your event.</p>
<span class="title_paragraph">Cigar roller</span>
<p lang="en-GB">The manufacturing of long filler cigars is art that requires long training &ndash; and is, without a question, the highlight of every cigar lounge.</p>
<p lang="en-GB">The noble cigars are formed by hand and covered with the binder and the wrapper. The process is finished with the modeling of cigar head.</p>
<p lang="en-GB">This way true masterpieces of tobacco form in front of your guests in a traditional manner. Or you let your guests try to roll one for themselves. That is, without a question, an unforgettable experience for your attendees.</p>
<p lang="en-GB">Together we will develop the ideal solution for your event.</p>
<p lang="en-GB">Also, our mobile indoor and outdoor cigar lounge are available in varied sizes and can be adjusted to your individual number of guests and room size. You may choose different elements from our event service to fit it exactly to your needs.</p>
<p lang="en-GB">On our website, we present the possibilities how you can make your events something very special. We also like to fulfill your individual needs.</p>
<p lang="en-GB">We are happy for your message to kontakt(at)worldofdelight.ch</p>';
    }

    private function getContentDe()
    {
        return '<p class="cigarette_image_wrap"></p>
<p>Lade Deine G&auml;ste zum Geniessen und Verweilen ein. Sorge so f&uuml;r ein ganz besonderes Erlebnis.</p>
<span class="title_paragraph">Humidor/Zigarren Miet-Service</span>
<p>Planst du eine Hochzeit, Fimenanlass oder einfach ein gr&ouml;sseres Fest und ben&ouml;tigst eine grosse Anzahl an diversen Zigarren f&uuml;r einen genussvollen Event?</p>
<p>Wir bieten Dir einen gef&uuml;llten Humidor mit einer grossen Sortenvielfalt an Zigarren, zusammengestellt nach deinem Wunsch. Verrechnet werden dann nur die Zigarren, welche du auch geraucht hast.</p>
<p>Entdecke jetzt vollumf&auml;nglichen Humidor/Zigarren Miet-Service und vieles mehr!</p>
<span class="title_paragraph">Cigar-Sommelier</span>
<p>Freundlich und fachkundig ber&auml;t unser Cigar-Sommelier Deine G&auml;ste &uuml;ber unser Zigarren-Angebot. Ausserdem erz&auml;hlt er Dir die verschiedenen Besonderheiten der einzelnen Produkte.</p>
<p>Ein Cigar-Sommelier kann die verschiedenen Geschmacksnuancen optimal erkennen und beschreiben. Als Experte hilft er Deinen G&auml;sten dabei, die passende Zigarre und das passende Format zu Ihrem Getr&auml;nk auszuw&auml;hlen. Zudem gibt er Dir gerne Tipps zur Handhabung und richtigen Lagerung von Zigarren.</p>
<span class="title_paragraph">Cigar-Hostess</span>
<p>Temperamentvoll, aktiv, fachkundig, erfahren und freundlich im Umgang mit Deinen Event-G&auml;sten. Das faszinierende Lebensgef&uuml;hl aus Gelassenheit und Impulsivit&auml;t wird Deine G&auml;ste f&ouml;rmlich &bdquo;anstecken&ldquo;.</p>
<p>Ob auf Galas, exklusiven Firmenevents oder auch auf Gourmetveranstaltungen, unser Team trifft stets den richtigen Ton und zelebriert das Anschneiden und Anz&uuml;nden einer Longfiller-Zigarre. Gerne auch auf Ihrer Veranstaltung.</p>
<span class="title_paragraph">Zigarren-Roller</span>
<p>Die Herstellung von Longfiller-Zigarren ist eine hohe Kunst, die eine lange Ausbildung voraussetzt &ndash; und sie ist ohne Frage das Highlight in jeder Zigarren LOUNGE.</p>
<p>Die edlen Zigarren werden rein von Hand geformt und mit dem Umblatt sowie dem Deckblatt &uuml;berrollt. Abgeschlossen wird der Prozess mit dem Modellieren des Zigarrenkopfs.</p>
<p>So entstehen vor den Augen Ihrer G&auml;ste in traditioneller Weise wahre Meisterwerke aus Tabak mit einer Menge Leidenschaft. Oder lasse Deine G&auml;ste die Zigarren-Rollung einmal selbst ausprobieren? Ganz ohne Frage ist das f&uuml;r alle Anwesenden immer ein unvergessliches Erlebnis.</p>
<p>Gemeinsam mit Dir entwickeln wir die ideale L&ouml;sung f&uuml;r Deine Veranstaltung.</p>
<p>Auch unsere mobile Indoor und Outdoor Cigar-Lounge ist in mehreren Gr&ouml;ssen verf&uuml;gbar und wird auf Deine individuellen G&auml;stezahlen und Raumgr&ouml;ssen angepasst. Du kannst dabei verschiedene Bausteine aus unserem Event-Service ausw&auml;hlen und so einen auf Sie zugeschnittenen Bereich schaffen.</p>
<p>Auf unserer Website haben wir Dir erste M&ouml;glichkeiten vorgestellt, wie Du Deinen Events eine ganz besondere Note verleihen kannst. Individuelle W&uuml;nsche erf&uuml;llen wir Dir gerne.</p>
<p>Wir freuen uns auf Deine Kontaktaufnahme unter kontakt(at)worldofdelight.ch</p>';
    }

}
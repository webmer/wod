<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class Version1509527126
 *
 * @package Migration
 */
class Version1509527126 implements Migration
{
    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    private $resourceConfig;
    /**
     * Version1509527126 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        // TODO: Implement up() method.
        $info = array(
            'catalog/frontend/grid_per_page_values'=>'15,30,60',
            'catalog/frontend/grid_per_page'=>'15',
            'catalog/frontend/list_per_page_values'=>'15,30,60',
            'catalog/frontend/list_per_page'=>'15',
        );

        foreach ($info as $key =>$value) {
            $this->resourceConfig->saveConfig(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }
    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
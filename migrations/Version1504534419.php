<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;

/**
 * Class Version1504534419
 *
 * @package Migration
 */
class Version1504534419 implements Migration
{
    /**
     * Version1504534419 constructor.
     * Inject Dependency
     *
     */

    /**
     * @var \Magento\Eav\Model\Entity\TypeFactory
     */
    private $typeFactory;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    private $setFactory;

    /**
     * @var \Magento\Eav\Model\AttributeSetManagement
     */
    private $attributeSetManagement;

    /**
     * @var \Magento\Eav\Model\AttributeManagement
     */
    private $attributeManagement;


    public function __construct(
        \Magento\Eav\Model\Entity\TypeFactory $typeFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $setFactory,
        \Magento\Eav\Model\AttributeSetManagement $attributeSetManagement,
        \Magento\Eav\Model\AttributeManagement $attributeManagement
    ) {
        $this->typeFactory = $typeFactory;
        $this->setFactory = $setFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->attributeManagement = $attributeManagement;

    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {
        $entityTypeCode = 'catalog_product';
        $entityType     = $this->typeFactory->create()->loadByCode($entityTypeCode);

        $attributeSetId   = $entityType->getDefaultAttributeSetId();


        foreach ($this->getAttributesName() as $attributeName) {
            $attributeSet = $this->setFactory->create();
            $data = [
                'attribute_set_name' => $attributeName, // define custom attribute set name here
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 200,
            ];

            $attributeSet->setData($data);
            $attributeSet->validate();
            $attributeSet->save();
            $attributeSet->initFromSkeleton($attributeSetId);
            $attributeSet->save();
        }
    }


    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }

    private function getAttributesName(){
        return [
            "0" => "tabak",
            "1" => "kaffee",
            "2"=>'süssigkeiten',
            "3" => 'getränke',
        ];
    }
}

<?php

namespace Migration;

use Snatch\Migrations\Contracts\Migration;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class Version1504184421
 *
 * @package Migration
 */
class Version1504184421 implements Migration
{
    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;

    /**
     * @var Website
     */
    private $websiteResourceModel;

    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var GroupFactory
     */
    private $groupFactory;

    /**
     * @var Store
     */
    private $storeResourceModel;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    private $resourceConfig;
    /**
     * Version1504184421 constructor.
     * Inject Dependency
     *
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        Store $storeResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager,
        ConfigInterface  $resourceConfig
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->storeResourceModel = $storeResourceModel;
        $this->eventManager = $eventManager;
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up()
    {

        $website = $this->websiteFactory->create();
        $website->load('base');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        foreach ($storeManager->getStores($withDefault = false) as $store){
            if($store->getCode()=='default'){
                $store->setCode('de');
                $store->setName('German');
                $this->storeResourceModel->save($store);
            }
        }
        $store = $this->storeFactory->create();
        $store->load('en');
        if(!$store->getStoreId()){
            $group = $this->groupFactory->create();
            $group->load('Main Website Store', 'name');
            $store->setCode('en');
            $store->setName('English');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
        }

    }

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down()
    {
        // TODO: Implement down() method.
    }
}
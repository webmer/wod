<?php

namespace Snatch\Migrations\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package Snatch\Migrations\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Setup module scripts
     *
     * @param SchemaSetupInterface      $setup
     * @param ModuleContextInterface    $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

		/**
         * Create table 'migrations_migration'
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('migrations_migration')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'migrations_migration'
        )->addColumn(
            'version',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'version'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'status'
        )->setComment(
            'Snatch Migrations migrations_migration'
        );

        $setup->getConnection()->createTable($table);
		/*{{CedAddTable}}*/

        $setup->endSetup();

    }
}

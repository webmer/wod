<?php


namespace Snatch\Migrations\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Migration
 *
 * @package Snatch\Migrations\Model\ResourceModel
 */
class Migration extends AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('migrations_migration', 'id');
    }

}

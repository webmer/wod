<?php


namespace Snatch\Migrations\Model\ResourceModel\Migration;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @package Snatch\Migrations\Model\ResourceModel\Migration
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Snatch\Migrations\Model\Migration',
            'Snatch\Migrations\Model\ResourceModel\Migration'
        );
    }
}

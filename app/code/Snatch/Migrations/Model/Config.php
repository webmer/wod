<?php


namespace Snatch\Migrations\Model;

use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\ValueInterface;
use Magento\Framework\DB\Transaction;
use Magento\Framework\App\Config\ValueFactory;

/**
 * Class Config
 *
 * @package Snatch\Migrations\Model
 */
class Config extends DataObject
{

	/**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

	/**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface 
     */
    protected $_scopeConfig;

	/**
     * @var \Magento\Framework\App\Config\ValueInterface
     */
    protected $_backendModel;

	/**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;
	/**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    protected $_configValueFactory;

	/**
     * @var int $_storeId
     */
    protected $_storeId;

	/**
     * @var string $_storeCode
     */
    protected $_storeCode;

    /**
     * Config constructor.
     *
     * @param StoreManagerInterface     $storeManager
     * @param ScopeConfigInterface      $scopeConfig
     * @param ValueInterface            $backendModel
     * @param Transaction               $transaction
     * @param ValueFactory              $configValueFactory
     * @param array                     $data
     *
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        ValueInterface $backendModel,
        Transaction $transaction,
        ValueFactory $configValueFactory,
        array $data = []
    ) {
        parent::__construct($data);

        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_backendModel = $backendModel;
        $this->_transaction = $transaction;
        $this->_configValueFactory = $configValueFactory;
		$this->_storeId=(int)$this->_storeManager->getStore()->getId();
		$this->_storeCode=$this->_storeManager->getStore()->getCode();
	}

    /**
     * Current store configuration
     *
     * @param $path
     * @return mixed
     */
	public function getCurrentStoreConfigValue($path){
		return $this->_scopeConfig->getValue($path,'store',$this->_storeCode);
	}
	
	/**
	 * Function for setting Config value of current store
     *
     * @param string $path,
	 * @param string $value,
     */
	public function setCurrentStoreConfigValue($path,$value){
		$data = [
            'path' => $path,
            'scope' =>  'stores',
            'scope_id' => $this->_storeId,
            'scope_code' => $this->_storeCode,
            'value' => $value,
        ];

		$this->_backendModel->addData($data);

		$this->_transaction->addObject($this->_backendModel);

		$this->_transaction->save();
	}
	
}

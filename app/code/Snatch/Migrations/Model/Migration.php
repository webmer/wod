<?php


namespace Snatch\Migrations\Model;

use Magento\Framework\Model\AbstractModel;

use Magento\Framework\Exception\MigrationException;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;


/**
 * Class Migration
 *
 * @package Snatch\Migrations\Model
 */
class Migration extends AbstractModel
{
    /**
     * @var string
     */
    const MIGRATED_STATUS = 'migrated';

    /**
     * @var string
     */
    const NOT_MIGRATED_STATUS = 'not_migrated';

    /**
     * Migration constructor.
     *
     * @param Context               $context
     * @param Registry              $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null       $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init('Snatch\Migrations\Model\ResourceModel\Migration');
    }

   
}
<?php


namespace Snatch\Migrations\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 *
 * @package Snatch\Migrations\Helper
 */
class Data extends AbstractHelper
{

    /**
     * Data constructor.
     *
     * @param Context $context
     */
	public function __construct( Context $context ) {
		parent::__construct($context);
	}
}
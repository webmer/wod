<?php


namespace Snatch\Migrations\Controller\Adminhtml\Migration;

use \Magento\Backend\App\Action;

/**
 * Class MassDelete
 *
 * @package Snatch\Migrations\Controller\Adminhtml\Migration
 */
class MassDelete extends Action
{
    /**
     * execute controller action
     */
    public function execute()
    {

        $ids = $this->getRequest()->getParam('id');

		if (!is_array($ids) || empty($ids)) {
            $this->messageManager->addError(__('Please select migration(s).'));
        } else {

            try {
                foreach ($ids as $id) {
                    $row = $this->_objectManager->get('Snatch\Migrations\Model\Migration')->load($id);
					$row->delete();
				}

                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($ids))
                );

            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

		$this->_redirect('*/*/');

    }
}

<?php


namespace Snatch\Migrations\Controller\Adminhtml\Migration;

use Magento\Backend\App\Action;

/**
 * Class Edit
 *
 * @package Snatch\Migrations\Controller\Adminhtml\Migration
 */
class Edit extends Action
{
    /**
     * Executive controller action
     */
	public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        $migration = $this->_objectManager->create('Snatch\Migrations\Model\Migration');
		
		$registryObject = $this->_objectManager->get('Magento\Framework\Registry');

        if ($id) {

            $migration->load($id);

            if (!$migration->getId()) {
                $this->messageManager->addError(__('This migration no longer exists..'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);

        if (!empty($data)) {
            $migration->setData($data);
        }

		$registryObject->register('migrations_migration', $migration);

		$this->_view->loadLayout();

        $this->_view->getLayout()->initMessages();

        $this->_view->renderLayout();
    }
}

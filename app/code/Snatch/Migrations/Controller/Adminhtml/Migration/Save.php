<?php


namespace Snatch\Migrations\Controller\Adminhtml\Migration;

use Magento\Backend\App\Action;

/**
 * Class Save
 *
 * @package Snatch\Migrations\Controller\Adminhtml\Migration
 */
class Save extends Action
{
    /**
     * execute action per controller
     */
	public function execute()
    {
        $data = $this->getRequest()->getParams();
        if ($data) {

            $model = $this->_objectManager->create('Snatch\Migrations\Model\Migration');

			$id = $this->getRequest()->getParam('id');

            if ($id) {
                $model->load($id);
            }
			
            $model->setData($data);
			
            try {

                $model->save();

                $this->messageManager->addSuccess(__('The Migration has been saved.'));

                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current' => true));
                    return;
                }

                $this->_redirect('*/*/');
                return;

            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {

                $this->messageManager->addException($e, __('Something went wrong while saving the Migration.'));

            }

            $this->_getSession()->setFormData($data);

            $this->_redirect('*/*/edit', array('migration_id' => $this->getRequest()->getParam('migration_id')));
            return;
        }

        $this->_redirect('*/*/');

    }
}

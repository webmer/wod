<?php


namespace Snatch\Migrations\Controller\Adminhtml\Migration;

use Magento\Backend\App\Action;

/**
 * Class NewAction
 *
 * @package Snatch\Migrations\Controller\Adminhtml\Migration
 */
class NewAction extends Action
{
    /**
     * Forward action to edit
     */
    public function execute()
    {
		$this->_forward('edit');
    }
}

<?php


namespace Snatch\Migrations\Controller\Adminhtml\Migration;

use Magento\Backend\App\Action;

/**
 * Class Delete
 *
 * @package Snatch\Migrations\Controller\Adminhtml\Migration
 */
class Delete extends Action
{
    /**
     * Executive controller action
     */
    public function execute()
    {
		$id = $this->getRequest()->getParam('id');
		try {
            $migration = $this->_objectManager->get('Snatch\Migrations\Model\Migration')->load($id);
            $migration->delete();

            $this->messageManager->addSuccess( __('Delete successfully !') );

        } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
        }
	    $this->_redirect('*/*/');
    }
}

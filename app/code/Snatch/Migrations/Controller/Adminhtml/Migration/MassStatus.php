<?php


namespace Snatch\Migration\Controller\Adminhtml\Migration;

use Magento\Backend\App\Action;

/**
 * Class MassStatus
 *
 * @package Snatch\Migration\Controller\Adminhtml\Migration
 */
class MassStatus extends Action
{
    /**
     * execute controller action
     */
    public function execute()
    {
		$ids = $this->getRequest()->getParam('id');

		$status = $this->getRequest()->getParam('status');

		if (!is_array($ids) || empty($ids)) {
            $this->messageManager->addError(__('Please select migration(s).'));
        } else {

            try {
                foreach ($ids as $id) {
                    $row = $this->_objectManager->get('Snatch\Firstgrid\Model\Migration')->load($id);
					$row->setData('status',$status)->save();
				}

                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($ids))
                );

            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }

        }

		$this->_redirect('*/*/');

    }
}

<?php


namespace Snatch\Migrations\Block;

use \Magento\Framework\App\RequestInterface;
use \Magento\Framework\View\LayoutInterface;
use \Magento\Framework\Event\ManagerInterface;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\App\CacheInterface;
use \Magento\Framework\View\DesignInterface;
use \Magento\Framework\Session\SessionManagerInterface;
use \Magento\Framework\Session\SidResolverInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\View\Asset\Repository;
use \Magento\Framework\View\ConfigInterface;
use \Magento\Framework\App\Cache\StateInterface;
use \Psr\Log\LoggerInterface;
use \Magento\Framework\Escaper;
use \Magento\Framework\Filter\FilterManager;
use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use \Magento\Framework\Translate\Inline\StateInterface as InlineStateInterface ;
use \Magento\Framework\Filesystem;
use \Magento\Framework\View\FileSystem as ViewFileSystem;
use \Magento\Framework\View\TemplateEnginePool;
use \Magento\Framework\App\State;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\View\Page\Config;
use \Magento\Framework\View\Element\Template\File\Resolver;
use \Magento\Framework\View\Element\Template\File\Validator;
use \Snatch\Migrations\Helper\Data;
use \Magento\Framework\Registry;
use \Snatch\Migrations\Model\Config as ModelConfig;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Framework\UrlFactory;

/**
 * Class Context
 *
 * @package Snatch\Migrations\Block
 */
class Context extends \Magento\Framework\View\Element\Template\Context
{
    /**
     * @var Data
     */
    protected $_devToolHelper;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var ModelConfig
     */
    protected $_config;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var UrlFactory
     */
    protected $_urlFactory;

    /**
     * Context constructor.
     *
     * @param RequestInterface          $request
     * @param LayoutInterface           $layout
     * @param ManagerInterface          $eventManager
     * @param UrlInterface              $urlBuilder
     * @param CacheInterface            $cache
     * @param DesignInterface           $design
     * @param SessionManagerInterface   $session
     * @param SidResolverInterface      $sidResolver
     * @param ScopeConfigInterface      $scopeConfig
     * @param Repository                $assetRepo
     * @param ConfigInterface           $viewConfig
     * @param StateInterface            $cacheState
     * @param LoggerInterface           $logger
     * @param Escaper                   $escaper
     * @param FilterManager             $filterManager
     * @param TimezoneInterface         $localeDate
     * @param InlineStateInterface      $inlineTranslation
     * @param Filesystem                $filesystem
     * @param ViewFileSystem            $viewFileSystem
     * @param TemplateEnginePool        $enginePool
     * @param State                     $appState
     * @param StoreManagerInterface     $storeManager
     * @param Config                    $pageConfig
     * @param Resolver                  $resolver
     * @param Validator                 $validator
     * @param Data                      $devToolHelper
     * @param Registry                  $registry
     * @param ModelConfig               $config
     * @param ObjectManagerInterface    $objectManager
     * @param UrlFactory                $urlFactory
     */
    public function __construct(
		RequestInterface $request,
        LayoutInterface $layout,
        ManagerInterface $eventManager,
        UrlInterface $urlBuilder,
        CacheInterface $cache,
        DesignInterface $design,
        SessionManagerInterface $session,
        SidResolverInterface $sidResolver,
        ScopeConfigInterface $scopeConfig,
        Repository $assetRepo,
        ConfigInterface $viewConfig,
        StateInterface $cacheState,
        LoggerInterface $logger,
        Escaper $escaper,
        FilterManager $filterManager,
        TimezoneInterface $localeDate,
        InlineStateInterface $inlineTranslation,
        Filesystem $filesystem,
        ViewFileSystem $viewFileSystem,
        TemplateEnginePool $enginePool,
        State $appState,
        StoreManagerInterface $storeManager,
        Config $pageConfig,
        Resolver $resolver,
        Validator $validator,
        Data $devToolHelper,
        Registry $registry,
        ModelConfig $config,
		ObjectManagerInterface $objectManager,
		UrlFactory $urlFactory
    ) {
        $this->_devToolHelper = $devToolHelper;
        $this->registry = $registry;
		$this->_config = $config;
		$this->_objectManager=$objectManager;
		$this->_urlFactory=$urlFactory;

       parent::__construct(
            $request,
            $layout,
            $eventManager,
            $urlBuilder,
            $cache,
            $design,
            $session,
            $sidResolver,
            $scopeConfig,
            $assetRepo,
            $viewConfig,
            $cacheState,
            $logger,
            $escaper,
            $filterManager,
            $localeDate,
            $inlineTranslation,
            $filesystem,
            $viewFileSystem,
            $enginePool,
            $appState,
            $storeManager,
            $pageConfig,
            $resolver,
            $validator
        );
    }

    /**
     * Function for getting developer helper object
     *
     * @return Data
     */
    public function getMigrationsHelper()
    {
        return $this->_devToolHelper;
    }


    /**
     * Function for getting registry object
     *
     * @return Registry
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * Function for getting migrations model config object
     *
     * @return ModelConfig
     */
	public function getConfig(){
		return $this->_config;
	}

    /**
     * Function for getting object manager object
     *
     * @return ObjectManagerInterface
     */
	public function getObjectManager(){
		return $this->_objectManager;
	}

    /**
     * Function for getting UrlFactory object
     *
     * @return UrlFactory
     */
	public function getUrlFactory(){
		return $this->_urlFactory;
	}

}

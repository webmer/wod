<?php


namespace Snatch\Migrations\Block\Adminhtml\Migration;

use  Magento\Backend\Block\Widget\Grid\Extended;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Store\Model\WebsiteFactory;
use Snatch\Migrations\Model\ResourceModel\Migration\Collection;
use Magento\Framework\Module\Manager;

/**
 * Class Grid
 *
 * @package Snatch\Migrations\Block\Adminhtml\Migration
 */
class Grid extends Extended
{
    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * @var Collection
     */
	protected $_collectionFactory;

    /**
     * @var WebsiteFactory
     */
    protected $_websiteFactory;

    /**
     * Grid constructor.
     *
     * @param Context           $context
     * @param Data              $backendHelper
     * @param WebsiteFactory    $websiteFactory
     * @param Collection        $collectionFactory
     * @param Manager           $moduleManager
     * @param array             $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        WebsiteFactory $websiteFactory,
		Collection $collectionFactory,
        Manager $moduleManager,
        array $data = []
    ) {
		
		$this->_collectionFactory = $collectionFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Initialization GRID general
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
		
        $this->setId('productGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
       
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return $this->_storeManager->getStore($storeId);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
		try{
			$collection =$this->_collectionFactory->load();

			$this->setCollection($collection);

			parent::_prepareCollection();
		  
			return $this;
		} catch(Exception $e) {
			echo $e->getMessage();die;
		}
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField(
                    'websites',
                    'catalog_product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left'
                );
            }
        }
        return parent::_addColumnFilterToCollection($column);
    }

    /**
     * @return $this
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
		$this->addColumn(
            'version',
            [
                'header' => __('Version'),
                'index' => 'version',
                'class' => 'version'
            ]
        );
		$this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'class' => 'status'
            ]
        );
		/*{{CedAddGridColumn}}*/

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('migrations/*/index', ['_current' => true]);
    }
}

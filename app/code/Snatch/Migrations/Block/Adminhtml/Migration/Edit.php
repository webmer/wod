<?php


namespace Snatch\Migrations\Block\Adminhtml\Migration;

use Magento\Backend\Block\Widget\Form\Container;

/**
 * Class Edit
 *
 * @package Snatch\Migrations\Block\Adminhtml\Migration
 */
class Edit extends Container
{
    /**
     *
     */
    protected function _construct()
    {
		$this->_objectId = 'id';
        $this->_blockGroup = 'Snatch_Migrations';

        $this->_controller = 'adminhtml_migration';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Migration'));
        $this->buttonList->update('delete', 'label', __('Delete Migration'));

        $this->buttonList->add(
            'saveandcontinue',
            array(
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => array(
                    'mage-init' => array('button' => array('event' => 'saveAndContinueEdit', 'target' => '#edit_form'))
                )
            ),
            -100
        );

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'hello_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'hello_content');
                }
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('checkmodule_checkmodel')->getId()) {
            return __("Edit Migration '%1'", $this->escapeHtml($this->_coreRegistry->registry('checkmodule_checkmodel')->getTitle()));
        } else {
            return __('New Migration');
        }
    }
}

<?php


namespace Snatch\Migrations\Block\Adminhtml\Migration\Edit;

/**
 * Class Tabs
 *
 * @package Snatch\Migrations\Block\Adminhtml\Migration\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('checkmodule_migration_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Migration Information'));
    }
}
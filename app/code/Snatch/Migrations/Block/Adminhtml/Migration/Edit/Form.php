<?php


namespace Snatch\Migrations\Block\Adminhtml\Migration\Edit;

use Magento\Backend\Block\Widget\Form\Generic;

/**
 * Class Form
 *
 * @package Snatch\Migrations\Block\Adminhtml\Migration\Edit
 */
class Form extends Generic
{
    /**
     * Customer Service.
     *
     * @var CustomerAccountServiceInterface
     */
    protected $_customerAccountService;

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            array(
                'data' => array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                )
            )
        );

        $form->setUseContainer(true);

        $this->setForm($form);

        return parent::_prepareForm();
    }
}

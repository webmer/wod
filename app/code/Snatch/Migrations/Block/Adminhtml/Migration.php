<?php


namespace Snatch\Migrations\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

/**
 * Class Migration
 *
 * @package Snatch\Migrations\Block\Adminhtml
 */
class Migration extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_migration';

        $this->_blockGroup = 'Snatch_Migrations';

        $this->_headerText = __('Migrations');
        $this->removeButton('add');
        parent::_construct();
    }
}

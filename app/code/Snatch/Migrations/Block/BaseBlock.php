<?php


namespace Snatch\Migrations\Block;

use Magento\Framework\View\Element\Template;

use Magento\Framework\UrlFactory;
use \Snatch\Migrations\Block\Context as SnatchContext;

/**
 * Class BaseBlock
 *
 * @package Snatch\Migrations\Block
 */
class BaseBlock extends Template
{
    /**
     * @var \Snatch\Migrations\Helper\Data
     */
	 protected $_devToolHelper;

    /**
     * @var \Magento\Framework\UrlInterface
     */
	 protected $_urlApp;

    /**
     * @var \Snatch\Migrations\Model\Config
     */
    protected $_config;

    /**
     * BaseBlock constructor.
     *
     * @param Context $context
     */
    public function __construct( SnatchContext $context)
    {
        $this->_devToolHelper = $context->getMigrationsHelper();
		$this->_config = $context->getConfig();
        $this->_urlApp=$context->getUrlFactory()->create();
		parent::__construct($context);
	
    }

    /**
     * Function for getting event details
     *
     * @return mixed
     */
    public function getEventDetails()
    {
		return  $this->_devToolHelper->getEventDetails();
    }

    /**
     * Function for getting current url
     *
     * @return string
     */
	public function getCurrentUrl(){
		return $this->_urlApp->getCurrentUrl();
	}
	
	/**
     * Function for getting controller url for given router path
     *
	 * @param string $routePath
	 * @return string
     */
	public function getControllerUrl($routePath){
		return $this->_urlApp->getUrl($routePath);
	}
	
	/**
     * Function for getting current url
     *
	 * @param string $path
	 * @return string
     */
	public function getConfigValue($path){
		return $this->_config->getCurrentStoreConfigValue($path);
	}
	
	/**
     * Function canShowMigrations
     *
	 * @return bool
     */
	public function canShowMigrations(){
		$isEnabled=$this->getConfigValue('migrations/module/is_enabled');
		if($isEnabled)
		{
			$allowedIps=$this->getConfigValue('migrations/module/allowed_ip');
			 if(is_null($allowedIps)){
				return true;
			}
			else {
				$remoteIp=$_SERVER['REMOTE_ADDR'];
				if (strpos($allowedIps,$remoteIp) !== false) {
					return true;
				}
			}
		}
		return false;
	}
	
}

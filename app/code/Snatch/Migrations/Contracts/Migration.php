<?php


namespace Snatch\Migrations\Contracts;

/**
 * Interface Migration
 *
 * @package Snatch\Migrations\Contracts
 */
interface Migration
{

    /**
     * Method for run-up migration version
     *
     * @return void
     */
    public function up();

    /**
     * Method for run-down migration version
     *
     * @return void
     */
    public function down();

}
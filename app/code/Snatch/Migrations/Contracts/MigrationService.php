<?php


namespace Snatch\Migrations\Contracts;

/**
 * Interface MigrationService
 *
 * @package Snatch\Migrations\Contracts
 */
interface MigrationService
{
    /**
     * String for parse
     *
     * @var string
     */
    const TEMPLATE_VERSION_PARSE = "[:version]";

    /**
     * Migration template file name
     *
     * @var string
     */
    const TEMPLATE_FILE_NAME = "migration_template.yaml";

    /**
     * Migration dir name
     *
     * @var string
     */
    const MIGRATION_DIR = "migrations";

    /**
     * Content what have to be ignoring on scan dirs
     *
     * @var array
     */
    const IGNORING_CONTENT = ['.', '..', ".gitkeep", ".gitignore"];

    /**
     * Migration Namespace
     *
     * @var string
     */
    const MIGRATION_NAMESPACE = "Migration";

}
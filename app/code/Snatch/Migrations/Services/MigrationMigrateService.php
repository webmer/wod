<?php


namespace Snatch\Migrations\Services;

use Snatch\Migrations\Contracts\MigrationService;
use Magento\Framework\App\Filesystem\DirectoryList;
use Snatch\Migrations\Services\Subservices\MigrationLoaderService;
use Snatch\Migrations\Model\ResourceModel\Migration\Collection;

/**
 * Class MigrationMigrateGenerateService
 *
 * @package Snatch\Migrations\Services
 */
class MigrationMigrateService implements MigrationService
{
    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    private $objectManager;

    /**
     * @var MigrationLoaderService
     */
    private $loaderService;

    /**
     * @var Collection
     */
    private $_collectionFactory;

    /**
     * Have already migrated versions list
     *
     * @var array
     */
    private $migratedVersions = [];

    /**
     * @var array
     */
    private $needToMigrateVersions = [];

    /**
     * MigrationMigrateService constructor.
     *
     * @param DirectoryList $directoryList
     * @param MigrationLoaderService $loaderService
     */
    public function __construct(
        DirectoryList $directoryList,
        MigrationLoaderService $loaderService,
        Collection $collectionFactory
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->loaderService = $loaderService;

        $this->_collectionFactory = $collectionFactory;

        $collection = $this->_collectionFactory->load();
        foreach ($collection as $migration) {
            $this->migratedVersions[] = $migration->getVersion();
        }

        $classes = $this->loaderService->getMigrationClasses();
        foreach ($classes as $version => $data){
            if(!in_array($version, $this->migratedVersions)){
                $this->needToMigrateVersions[] = $version;
            }
        }
    }

    /**
     * @param null $versionToMigrate
     * @return array
     */
    public function migrate($versionToMigrate = null)
    {
        $hasUpVersion = [];
        $classes = $this->loaderService->getMigrationClasses();
        foreach ($classes as $version => $data){
            if(in_array($version, $this->needToMigrateVersions)){
                if($versionToMigrate && $versionToMigrate == $version){
                    $hasUpVersion[] = $this->upMigration($version, $data["class"], $data["file"]);
                    break;
                } elseif(!$versionToMigrate) {
                    $hasUpVersion[] = $this->upMigration($version, $data["class"], $data["file"]);
                }
            }
        }
        return $hasUpVersion;
    }


    /**
     * @param $version
     */
    private function insertMigratedVersion($version){
        $model = $this->objectManager->create('Snatch\Migrations\Model\Migration');
        $model->setVersion($version);
        $model->setStatus(\Snatch\Migrations\Model\Migration::MIGRATED_STATUS);
        $model->save();
    }

    /**
     * @param null $versionToMigrate
     * @return array
     */
    public function rollback($versionRollback = null)
    {
        $hasRollbackVersion = [];
        $classes = $this->loaderService->getMigrationClasses();

        foreach ($classes as $version => $data){
            if($versionRollback && $versionRollback == $version){
                $hasRollbackVersion[] = $this->downMigration($version, $data["class"], $data["file"]);
                break;
            } elseif(!$versionRollback) {
                $hasRollbackVersion[] = $this->downMigration($version, $data["class"], $data["file"]);
            }

        }
        return $hasRollbackVersion;
    }

    /**
     * @param $version
     */
    private function removeMigratedVersion($version){
        $migration = $this->objectManager->create('Snatch\Migrations\Model\Migration');
        $migration->load($version,'version');
        $migration->delete();
    }

    /**
     * Include and run 'up' migration
     *
     * @param $version
     * @param $class
     * @param $file
     * @return mixed
     */
    private function upMigration($version, $class, $file){
        include_once($file);
        $migration = $this->objectManager->get($class);
        $migration->up();
        $this->insertMigratedVersion($version);
        return $version;
    }

    /**
     * Waiting on migration versions list
     *
     * @return array
     */
    public function getNeedToMigrationVersions(){
        return $this->needToMigrateVersions;
    }

    /**
     * Has migrated versions list
     *
     * @return array
     */
    public function getMigratedVersions(){
        return $this->migratedVersions;
    }

    /**
     * Include and run 'down' migration
     *
     * @param $version
     * @param $class
     * @param $file
     * @return mixed
     */
    private function downMigration($version, $class, $file){
        include_once($file);
        $migration = $this->objectManager->get($class);
        $migration->down();
        $this->removeMigratedVersion($version);
        return $version;
    }

}
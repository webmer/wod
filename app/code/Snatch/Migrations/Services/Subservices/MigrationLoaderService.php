<?php

namespace Snatch\Migrations\Services\Subservices;

use Snatch\Migrations\Contracts\MigrationService;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class LoaderService
 *
 * @package Snatch\Migrations\Services\Subservices
 */
class MigrationLoaderService implements MigrationService
{
    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    private $migrationPath;

    /**
     * MigrationMigrateGenerateService constructor.
     *
     * @param DirectoryList $directoryList
     */
    public function __construct( DirectoryList $directoryList )
    {
        $this->directoryList = $directoryList;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->migrationPath =
            $this->directoryList->getRoot()
            . DIRECTORY_SEPARATOR . MigrationService::MIGRATION_DIR;
    }

    /**
     * @return array
     */
    public function getMigrationClasses(){
        $migrationsClasses = [];
        $migrationFiles = $this->getMigrationDirScan();
        foreach ($migrationFiles as $file){
            $migrationsClasses[str_replace(".php", "", str_replace("Version", "", $file))]
                =
                [
                    "class" => "\\".MigrationService::MIGRATION_NAMESPACE ."\\". str_replace(".php", "", $file),
                    "file" => $this->migrationPath . DIRECTORY_SEPARATOR . $file

                ];
        }
        return $migrationsClasses;
    }

    /**
     * Scan migration dir
     *
     * @return array
     */
    private function getMigrationDirScan()
    {
        return array_diff(
            scandir( $this->migrationPath),
            MigrationService::IGNORING_CONTENT
        );
    }
}
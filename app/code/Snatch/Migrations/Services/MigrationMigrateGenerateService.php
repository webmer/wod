<?php

namespace Snatch\Migrations\Services;

use Snatch\Migrations\Contracts\MigrationService;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class MigrationMigrateGenerateService
 *
 * @package Snatch\Migrations\Services
 */
class MigrationMigrateGenerateService implements MigrationService
{

    /**
     * @var integer
     */
    private $version;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * MigrationMigrateGenerateService constructor.
     *
     * @param DirectoryList $directoryList
     */
    public function __construct( DirectoryList $directoryList )
    {
        $this->version = (int)microtime(true);
        $this->directoryList = $directoryList;
    }

    /**
     * Return generated migration version
     * @return string
     */
    public function generate()
    {
        $fileName = "Version".$this->version.".php";
        $migrationFileContent = $this->parseVersion($this->getMigrationTemplate());

        $migrationDir =
            $this->directoryList->getRoot()
            . DIRECTORY_SEPARATOR . MigrationService::MIGRATION_DIR . DIRECTORY_SEPARATOR;

        try{
            mkdir($migrationDir, 0777, true);
        } catch ( \Exception $exception ){}

        $file = fopen($migrationDir . $fileName, "w");
        fwrite($file, $migrationFileContent);
        fclose($file);
        return "Version".$this->version;
    }

    /**
     * Return content file template
     * @return string
     */
    private function getMigrationTemplate()
    {

        $templatePath =
            dirname(dirname(__FILE__))
            . DIRECTORY_SEPARATOR . "Template"
            . DIRECTORY_SEPARATOR . MigrationService::TEMPLATE_FILE_NAME;

        return file_get_contents($templatePath);
    }

    /**
     * Replace template version
     *
     * @param $content
     * @return mixed
     */
    private function parseVersion($content)
    {
        return str_replace(
            MigrationService::TEMPLATE_VERSION_PARSE,
            $this->version,
            $content
        );
    }
}
<?php
/**
 * Registration  Snatch_Migrations module
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Snatch_Migrations',
    __DIR__
);

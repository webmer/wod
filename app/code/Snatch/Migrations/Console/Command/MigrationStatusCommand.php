<?php


namespace Snatch\Migrations\Console\Command;

use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class MigrationStatusCommand
 *
 * @package Snatch\Migrations\Console\Command
 */
class MigrationStatusCommand extends Command
{

    /**
     * Init command full name and arguments
     */
    protected function configure() {

        $this->setName('snatch-migrations:status')
            ->setDescription('Run migration status');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("\n");
        $output->writeln("Show migration statuses.");
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $migrateService = $objectManager->get('\Snatch\Migrations\Services\MigrationMigrateService');

        $output->writeln("_______________________________");
        $hasMigratedVersions = $migrateService->getMigratedVersions();
        foreach ($hasMigratedVersions as $migratedVersion) {
            $output->writeln("Has migrated version: $migratedVersion");
        }

        $output->writeln("_______________________________");
        $awaitingOnMigrationVersions = $migrateService->getNeedToMigrationVersions();
        foreach ($awaitingOnMigrationVersions as $needToMigrate) {
            $output->writeln("Ready for migrate: $needToMigrate");
        }
        $output->writeln("\n");
        $output->writeln("\n");
    }

}
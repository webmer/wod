<?php


namespace Snatch\Migrations\Console\Command;

use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class MigrationMigrateCommand
 *
 * @package Snatch\Migrations\Console\Command
 */
class MigrationMigrateCommand extends Command
{

    /**
     * Init command full name and arguments
     */
    protected function configure() {

        $this->setName('snatch-migrations:migrate')
            ->setDescription('Run migration list');

        $this->addArgument('version', InputArgument::OPTIONAL, __('Migration version'));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $version = $input->getArgument("version");

        $output->writeln('Run migration scripts.');

        $migrateService = $objectManager->get('\Snatch\Migrations\Services\MigrationMigrateService');

        $migratedVersions = $migrateService->migrate($version);

        foreach ($migratedVersions as $migratedVersion) {
            $output->writeln("Has migrated version: $migratedVersion");
        }
    }

}
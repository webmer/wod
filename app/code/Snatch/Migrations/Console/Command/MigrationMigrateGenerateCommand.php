<?php


namespace Snatch\Migrations\Console\Command;

use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class MigrationMigrateGenerateCommand
 *
 * @package Snatch\Migrations\Console\Command
 */
class MigrationMigrateGenerateCommand extends Command
{

    /**
     * Init command full name
     */
    protected function configure()
    {
        $this->setName('snatch-migrations:migrate:generate')
            ->setDescription('Generate migration class.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Run snatch-migrations:migrate:generate.');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $generateService = $objectManager->get('\Snatch\Migrations\Services\MigrationMigrateGenerateService');
        $migrationClass = $generateService->generate();
        $output->writeln('Generated migration: ' . $migrationClass);
    }

}
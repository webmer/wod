<?php


namespace Snatch\Migrations\Console\Command;

use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class MigrationRollbackCommand
 *
 * @package Snatch\Migrations\Console\Command
 */
class MigrationRollbackCommand extends Command
{

    /**
     * Init command full name and arguments
     */
    protected function configure() {

        $this->setName('snatch-migrations:rollback')
            ->setDescription('Run rollback migration version or all versions');

        $this->addArgument('version', InputArgument::OPTIONAL, __('Migration version'));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $version = $input->getArgument("version");

        $output->writeln('Run migration down scripts.');

        $migrateService = $objectManager->get('\Snatch\Migrations\Services\MigrationMigrateService');

        $migratedVersions = $migrateService->rollback($version);

        foreach ($migratedVersions as $migratedVersion) {
            $output->writeln("Has rollback migration version: $migratedVersion");
        }
    }

}
<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_GiftCard
 */


namespace Amasty\GiftCard\Model\Order\Total\Creditmemo;

use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class GiftCard extends AbstractTotal
{
    /**
     * @var \Amasty\GiftCard\Helper\Data
     */
    private $helper;

    public function __construct(
        \Amasty\GiftCard\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param Creditmemo $creditmemo
     * @return $this
     */
    public function collect(Creditmemo $creditmemo)
    {
        $creditmemo = $this->helper->setTotalsToOrder($creditmemo);

        return $this;
    }
}

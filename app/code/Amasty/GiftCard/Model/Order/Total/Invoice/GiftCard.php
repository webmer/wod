<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_GiftCard
 */

namespace Amasty\GiftCard\Model\Order\Total\Invoice;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;
use Magento\Sales\Model\Order\Invoice;

class GiftCard extends AbstractTotal
{
    /**
     * @var \Amasty\GiftCard\Helper\Data
     */
    private $helper;

    public function __construct(
        \Amasty\GiftCard\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(Invoice $invoice)
    {
        $invoice = $this->helper->setTotalsToOrder($invoice);

        return $this;
    }
}

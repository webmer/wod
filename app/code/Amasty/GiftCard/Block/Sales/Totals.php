<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_GiftCard
 */


namespace Amasty\GiftCard\Block\Sales;

use Amasty\GiftCard\Model\ResourceModel\Quote\CollectionFactory;

class Totals extends \Magento\Framework\View\Element\Template
{
    /** @var CollectionFactory */
    private $quoteGiftCardCollectionFactory;

    /**
     * Totals constructor.
     *
     * @param CollectionFactory $quoteGiftCardCollectionFactory
     */
    public function __construct(CollectionFactory $quoteGiftCardCollectionFactory)
    {
        $this->quoteGiftCardCollectionFactory = $quoteGiftCardCollectionFactory;
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        if (!$parent || !method_exists($parent, 'getOrder')) {
            return $this;
        }

        $order = $parent->getOrder();

        if (!($order instanceof \Magento\Sales\Api\Data\OrderInterface)) {
            return $this;
        }

        $quoteGiftCardCollection = $this->quoteGiftCardCollectionFactory->create()
            ->addFieldToFilter('quote_id', $order->getQuoteId())
            ->joinAccount();

        $baseAmount = 0;
        $amount = 0;
        $giftCardLabel = [];
        foreach ($quoteGiftCardCollection as $quoteGiftCard) {
            if ($quoteGiftCard->getBaseGiftAmount()) {
                $baseAmount -= $quoteGiftCard->getBaseGiftAmount();
                $amount -= $quoteGiftCard->getGiftAmount();
                $giftCardLabel[] = $quoteGiftCard->getCode();
            }
        }

        if ($baseAmount < 0) {
            $giftCard = new \Magento\Framework\DataObject(
                [
                    'code' => 'amgiftcard',
                    'strong' => false,
                    'value' => $amount,
                    'base_value' => $baseAmount,
                    'label' => __('Gift Card') . ' ' . implode(', ', $giftCardLabel)
                ]
            );

            $parent->addTotalBefore($giftCard, 'grand_total');
        }

        return $this;
    }
}

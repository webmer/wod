<?php



namespace Meridian\AmGiftCart\Plugin\Magento\Quote\Model\Quote\Item;
use Closure;


class AmToOrderItem
{

    /**
     * @var \Amasty\GiftCard\Helper\Data
     */
    protected $amHelper;
    /**
     * @var \Amasty\Base\Model\Serializer
     */
    private $serializer;

    public function __construct(
        \Amasty\GiftCard\Helper\Data $amHelper,
        \Amasty\Base\Model\Serializer $serializer
    ) {
        $this->amHelper = $amHelper;
        $this->serializer = $serializer;
    }

    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);

        $keys = [
            'am_giftcard_sender_surname',
            'am_giftcard_recipient_surname'
        ];
        $productOptions = $orderItem->getProductOptions();
        $customOptions = [];

        if (is_array($item->getOptions())) {
            foreach ($item->getOptions() as $key => $itemOption) {
                if ($itemOption->getCode() == 'info_buyRequest' && $options = $itemOption->getValue()) {
                    $customOptions = $this->serializer->unserialize($options);
                }
            }
        }
        $product = $item->getProduct()->load($item->getProduct()->getId());
        foreach ($keys as $key) {
            if (array_key_exists($key, $customOptions)) {
                $productOptions[$key] = $customOptions[$key];
            }
        }


        $orderItem->setProductOptions($productOptions);

        return $orderItem;
    }
}


<?php

namespace Meridian\AmGiftCart\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;
    protected $_blockFactory;
    protected $_layout;
    /**
     * @param \Magento\Framework\App\Helper\Context   $context
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     */


    public function __construct(
        \Magento\Framework\View\Element\BlockFactory $blockFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout
    ) {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->storeManager = $storeManager;
        $this->_blockFactory = $blockFactory;
        $this->_layout = $layout;
    }

    public function getHtmlForPdf($code)
    {

        $html = $this->_layout
            ->createBlock('Meridian\AmGiftCart\Block\Pdf\Pdf')
            ->setParams($code)
            ->setTemplate('Meridian_AmGiftCart::pdf.phtml')->toHtml();
        return  $html;
    }

}

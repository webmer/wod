<?php

namespace Meridian\AmGiftCart\Model\Amasty\GiftCard;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Magento\Framework\Pricing\PriceCurrencyInterface;



class Account extends \Amasty\GiftCard\Model\Account
{
    protected $html2Pdf;
    protected $helperPdf;
    protected  $CustomUploadTransportBuilder;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Amasty\GiftCard\Model\ResourceModel\Account $resource,
        \Amasty\GiftCard\Model\ResourceModel\Account\Collection $resourceCollection,
        \Amasty\GiftCard\Model\Code $codeModel,
        \Amasty\GiftCard\Model\ResourceModel\Code $codeResourceModel,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Amasty\GiftCard\Model\Image $imageModel,
        \Amasty\GiftCard\Model\UploadTransportBuilder $uploadTransportBuilder,
        \Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Checkout\Model\SessionFactory $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        PriceCurrencyInterface $priceCurrency,
        \Amasty\GiftCard\Helper\Data $dataHelper,
        \Meridian\AmGiftCart\Helper\Data $helperPdf,
        \Meridian\AmGiftCart\Model\CustomUploadTransportBuilder $CustomUploadTransportBuilder,
        array $data = []
    ) {

        parent::__construct($context, $registry, $resource, $resourceCollection,  $codeModel, $codeResourceModel, $orderModel, $storeManager, $scopeConfig, $directoryList, $imageModel, $uploadTransportBuilder, $productResourceModel, $date,$checkoutSession,  $customerSession,$messageManager,$priceCurrency,$dataHelper, $data);
        $this->html2Pdf = new Html2Pdf('P', 'A4', 'de');
        $this->helperPdf = $helperPdf;
        $this->CustomUploadTransportBuilder = $CustomUploadTransportBuilder;
    }


    protected function getPdfEmailFile(){

    }


    public function sendDataToMail()
    {


        if (!$this->getData('recipient_email') || $this->getIsSent()) {
            return false;
        }

        $storeId = $this->getOrder()->getStoreId();
        if ($this->getData('store_id')) {
            $storeId = $this->getData('store_id');
        }

        $storeId = $this->storeManager->getStore($storeId)->getStoreId();

        $template = $this->scopeConfig->getValue(
            'amgiftcard/email/email_template',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $imageGiftCard = null;
        $id = null;
        if ($this->isImage()) {
            $id = uniqid('am_giftcard');
            $imageGiftCard = "cid:$id";
        }

        $templateParams = [
            'recipient_name' => $this->getData('recipient_name'),
            'sender_name' => $this->getData('sender_name'),
            'initial_value' => $this->dataHelper->round($this->getData('initial_value')),
            'currency_code' => $this->getOrder()->getOrderCurrencyCode(),
            'sender_message'=> $this->getData('sender_message'),
            'gift_code'=> $this->getCode(),
            'image_base64'=> $imageGiftCard,
            'expired_date'=> $this->date->date('Y-m-d', $this->getData('expired_date')),
        ];

        $from = $this->scopeConfig->getValue(
            'amgiftcard/email/email_identity',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($emailCC = trim($this->scopeConfig->getValue(
            'amgiftcard/email/email_recepient_cc',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ))) {
            $emailCC = explode(",", $emailCC);
            array_walk($emailCC, 'trim');
        }


        $html =  $this->helperPdf->getHtmlForPdf($templateParams);
        $this->html2Pdf->writeHTML($html, isset($_GET['vuehtml']));
        $filename = 'var'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'code.pdf';
        $content =  $this->html2Pdf->Output($filename,'F');

        $transportBuilder = $this->CustomUploadTransportBuilder->setTemplateIdentifier($template)
            ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
            ->setTemplateVars($templateParams)
            ->setFrom($from)
            ->addTo($this->getData('recipient_email'), $this->getData('recipient_name'))
            ->attachPdfFile(file_get_contents($filename));

        if ($emailCC) {
            $transportBuilder->addCc($emailCC);
        }
        $transport = $transportBuilder->getTransport();

        $transport->sendMessage();
        unlink($filename);

        if ($this->scopeConfig->getValue(
                'amgiftcard/email/send_confirmation_to_sender',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
            && $this->getData('sender_email')
        ) {

            $templateParams = [
                'recipient_name' => $this->getData('recipient_name'),
                'sender_name' => $this->getData('sender_name'),
                'initial_value' => $this->dataHelper->round($this->getData('initial_value')),
                'currency_code' => $this->getOrder()->getOrderCurrencyCode(),
                'sender_message' => $this->getData('sender_message'),
                'expired_date' => $this->date->date('Y-m-d', $this->getData('expired_date'))
            ];

            $transport = $this->CustomUploadTransportBuilder
                ->setTemplateIdentifier($this->scopeConfig->getValue(
                    'amgiftcard/email/email_template_confirmation_to_sender',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ))
                ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
                ->setTemplateVars($templateParams)
                ->setFrom($from)
                ->addTo($this->getData('sender_email'), $this->getData('sender_name'))
                ->getTransport();

            $transport->sendMessage();
        }

        $this->setIsSent(1);
//        unlink($filename);
        $this->getResource()->save($this);
    }
	
}
	
	
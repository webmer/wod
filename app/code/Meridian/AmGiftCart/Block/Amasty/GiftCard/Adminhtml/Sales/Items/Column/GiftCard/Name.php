<?php

namespace Meridian\AmGiftCart\Block\Amasty\GiftCard\Adminhtml\Sales\Items\Column\GiftCard;


class Name extends \Amasty\GiftCard\Block\Adminhtml\Sales\Items\Column\GiftCard\Name
{
    protected function _getGiftcardOptions()
    {
        $result = [];

        $value = $this->getItem()->getOriginalPrice();

        if ($value) {
            $result[] = [
                'label' => __('Card Value'),
                'value' => $this->dataHelper->round($value)
            ];
        }

        $value = $this->_prepareCustomOption('am_giftcard_type');
        $giftcardType = $value;

        if ($value) {
            $result[] = [
                'label' => __('Card Type'),
                'value' => $this->dataHelper->getCardType($value)
            ];
        }

        $value = $this->_prepareCustomOption('am_giftcard_image');
        if ($value) {
            $image = $this->imageModel;
            $image->getResource()->load($image, $value);
            if ($image->getId()) {
                $value = '<img src="'. $image->getImageUrl() .
                    '"  width="270px;" title="'. __('Image Id %1', $image->getId()).'"/>';
                $result[] = [
                    'label' => __('Gift Card Image'),
                    'value' => $value,
                    'custom_view'=> true,
                ];
            }

        }

        $value = $this->_prepareCustomOption('am_giftcard_sender_name') .' '. $this->_prepareCustomOption('am_giftcard_sender_surname');
        if ($value) {
            $email = $this->_prepareCustomOption('am_giftcard_sender_email');
            if ($email) {
                $value = "{$value} &lt;{$email}&gt;";
            }
            $result[] = [
                'label' => __('Gift Card Sender'),
                'value' => $value
            ];
        }

        $value = $this->_prepareCustomOption('am_giftcard_recipient_name').' '.$this->_prepareCustomOption('am_giftcard_recipient_surname');
        if ($value && $giftcardType != \Amasty\GiftCard\Model\GiftCard::TYPE_PRINTED) {
            $email = $this->_prepareCustomOption('am_giftcard_recipient_email');
            if ($email) {
                $value = "{$value} &lt;{$email}&gt;";
            }
            $result[] = [
                'label' => __('Gift Card Recipient'),
                'value' => $value
            ];
        }

        $value = $this->_prepareCustomOption('am_giftcard_message');
        if ($value) {
            $result[] = [
                'label' => __('Gift Card Message'),
                'value' => $value
            ];
        }

        if ($value = $this->_prepareCustomOption('am_giftcard_lifetime')) {
            $result[] = [
                'label'=> __('Gift Card Lifetime'),
                'value'=> __('%1 days', $value),
            ];
        }

        if ($value = $this->_prepareCustomOption('am_giftcard_date_delivery')) {
            $result[] = [
                'label'=> __('Date of certificate delivery'),
                'value'=>$this->formatDate($value, \IntlDateFormatter::SHORT, true),
            ];
        }

        $createdCodes = 0;
        $totalCodes = $this->getItem()->getQtyOrdered();
        if ($codes = $this->getItem()->getProductOptionByCode('am_giftcard_created_codes')) {
            $createdCodes = count($codes);
        }

        if (is_array($codes)) {
            foreach ($codes as &$code) {
                if ($code === null) {
                    $code = __('Unable to create.');
                }
            }
        } else {
            $codes = [];
        }

        for ($i = $createdCodes; $i < $totalCodes; $i++) {
            $codes[] = __('N/A');
        }

        $result[] = [
            'label'=> __('Gift Card Accounts'),
            'value'=>implode('<br />', $codes),
            'custom_view'=>true,
        ];

        return $result;
    }
	
}
	
	
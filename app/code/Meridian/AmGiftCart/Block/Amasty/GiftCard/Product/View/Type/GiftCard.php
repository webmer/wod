<?php

namespace Meridian\AmGiftCart\Block\Amasty\GiftCard\Product\View\Type;


class GiftCard extends \Amasty\GiftCard\Block\Product\View\Type\GiftCard
{


    public function getCustomerName()
    {
        $firstName = (string)$this->getCustomerSession()->getCustomer()->getFirstname();


        return trim($firstName);
    }

    public function getCustomerSurname()
    {

        $lastName  = (string)$this->getCustomerSession()->getCustomer()->getLastname();

        return trim($lastName);
    }
	
	
}
	
	
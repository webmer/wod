<?php

namespace Meridian\AmGiftCart\Block\Pdf;
use Magento\Framework\View\Element\Template;
use Magento\Framework\UrlInterface;


class Pdf extends Template
{
    protected $_template = "Meridian_AmGiftCart::pdf.phtml";

    protected $storeManager;

    protected function _construct(

    )
    {
        parent::_construct();
        $this->setTemplate($this->_template);

    }

    public function getMediaUrl(){
        return $this->_storeManager
            ->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }



}
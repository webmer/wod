<?php

namespace Meridian\ProductUpd\Helper;

use Magento\Framework\Pricing\Amount\AmountInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;

class Data extends  \Magento\Framework\App\Helper\AbstractHelper
{
    const FINAL_PRICE_CODE = 'finalPrice';
    const OLD_PRICE_CODE = 'oldPrice';
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_localeCurrency;

    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    protected $_localeFormat;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected $_helperWishlist;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Locale\CurrencyInterface $localeCurrency
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magento\Wishlist\Helper\Data $_helperWishlist
    )
    {
        $this->priceCurrency = $priceCurrency;
        $this->_localeCurrency = $localeCurrency;
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_registry = $objectManager->get('\Magento\Framework\Registry');
        $this->appState = $context->getAppState();
        $this->_helperWishlist = $_helperWishlist;
    }

    public function getLabels($price = null)
    {
        if($price === null){
            return $price;
        }

    }

    /**
     * Format price value
     *
     * @param float $amount
     * @param bool $includeContainer
     * @param int $precision
     * @return float
     */
    public function formatCurrency(
        $amount,
        $includeContainer = true,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION
    ) {
        return $this->priceCurrency->format($amount, $includeContainer, $precision);
    }

    /**
     * Get formatted price value including order currency rate to order website currency
     *
     * @param   float $price
     * @param   bool  $addBrackets
     * @return  string
     */
    public function formatPrice($price, $addBrackets = false, $priceType = self::FINAL_PRICE_CODE)
    {
        if ( $price instanceof \Magento\Catalog\Model\Product && $this->_isFrontend()) {
            $product = $price;
            //$firstAmountValue = $this->_getFirstOfAmount($product);
            $price = $this->_getFirstOfAmount($product);
            if(is_null($priceType)){
                $priceType = self::FINAL_PRICE_CODE;
            }
            $html = '<div class="price-box">';
            $html .= $this->formatPricePrecision($price, PriceCurrencyInterface::DEFAULT_PRECISION, $addBrackets, $priceType);
            $html .= '</div>';
            return $html;
        }
        return $this->formatPricePrecision($price, PriceCurrencyInterface::DEFAULT_PRECISION, $addBrackets, $priceType);
    }

    /**
     * @param float $price
     * @param int $precision
     * @param bool $addBrackets
     * @return string
     */
    public function formatPricePrecision($price, $precision, $addBrackets = false, $priceType)
    {
        return $this->formatPrecision($price, $precision, [], true, $addBrackets, $priceType);
    }

    /**
     * Apply currency format to number with specific rounding precision
     *
     * @param   float $price
     * @param   int $precision
     * @param   array $options
     * @param   bool $includeContainer
     * @param   bool $addBrackets
     * @return  string
     */
    public function formatPrecision(
        $price,
        $precision,
        $options = [],
        $includeContainer = true,
        $addBrackets = false,
        $priceType
    ) {
        if (is_array($options) && !isset($options['precision'])) {
            $options['precision'] = $precision;
        }
        if ($includeContainer) {
            $priceInsert = $priceParse = $this->_formatTxt($price,$options);
            $localeCode = $this->getLocaleCode();
            if($localeCode == "de_DE") {
                $decimalSymbolFrom = '.';
                $decimalSymbolTo = '.';
                $groupSymbolFrom = ',';
                $groupSymbolTo = "'";
            } else {
                $decimalSymbolFrom = '.';
                $decimalSymbolTo = '.';
                $groupSymbolFrom = ',';
                $groupSymbolTo = "'";
            }
            if($priceType == self::FINAL_PRICE_CODE) {
                $priceParse = str_replace([$groupSymbolFrom,$this->getCode(),'&nbsp;',' '],[$groupSymbolTo,'','',''],$priceParse);
                $priceParse = htmlentities($priceParse, null, 'utf-8');
                $priceParse = preg_replace("/\s|&nbsp;/",'',$priceParse);
                $priceExplode = explode($decimalSymbolFrom,$priceParse);
                $priceNumber = $priceExplode[0];
                $priceDecimals = preg_replace("/&#?[a-z0-9]+;/i","",$priceExplode[1]);
                $priceDecimals = '<span class="monets">'.$priceDecimals.'</span>';
                $currencyHtml = '<span class="currency">'.$this->getCode().'</span>';
                $priceInsert = $priceNumber . '<span class="monets-carrency-wrapper">' . $priceDecimals.$currencyHtml.'</span>';
            } elseif ($priceType == self::OLD_PRICE_CODE) {
                $priceParse = str_replace([$this->getCode(),$decimalSymbolFrom],['',$decimalSymbolTo],$priceParse);
                $priceParse = htmlentities($priceParse, null, 'utf-8');
                $priceParse = preg_replace("/\s|&nbsp;/",'',$priceParse);
                $priceInsert = $this->getCode().' '.$priceParse;
            }
            $html = '<span class="price">' . ($addBrackets ? '[' : '') . $priceInsert . ($addBrackets ? ']' : '') . '</span>';
            return $html;
        }
        return $this->_formatTxt($price, $options);
    }

    /**
     * @param float $price
     * @param array $options
     * @return string
     */
    protected function _formatTxt($price, $options = [])
    {
        if (!is_numeric($price)) {
            $price = $this->_localeFormat->getNumber($price);
        }
        /**
         * Fix problem with 12 000 000, 1 200 000
         *
         * %f - the argument is treated as a float, and presented as a floating-point number (locale aware).
         * %F - the argument is treated as a float, and presented as a floating-point number (non-locale aware).
         */
        $price = sprintf("%F", $price);
        return $this->_localeCurrency->getCurrency($this->getCode())->toCurrency($price, $options);
    }

    /**
     * Get currency code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->priceCurrency->getCurrency()->getCurrencyCode();
    }

    public function getLocaleCode()
    {
        return $this->_localeCurrency->getCurrency($this->getCode())->getLocale();
    }

    public function _getCurrentProduct(){
        return $this->_registry->registry('current_product');
    }

    public function _getCurrentCategory(){
        return $this->_registry->registry('current_category');
    }

    /**
     * @return bool
     */
    public function _isFrontend()
    {
        return $this->appState->getAreaCode() == 'frontend';
    }

    /**
     * @param $product
     *
     * @return mixed
     */
    protected function _getFirstOfAmount($product)
    {
        $productModel = $product->getPriceModel();
        $amount = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
        return $amount;
//        $listOfAmounts = $productModel->getAmounts($product);
//        if ($listOfAmounts) {
//            return $listOfAmounts[0]["price"];
//        }

//        return null;
    }

    public function isWishlistActive($id = null)
    {
        if(is_null($id)) return '';
        $classActive = '';
        $wishlistCollection = $this->_helperWishlist->getWishlistItemCollection()
            ->addFieldToFilter('product_id', $id);
        $wishlistItem = $wishlistCollection->getFirstItem();
        $wishlistId = $wishlistItem->getId();
        if ($wishlistId) {
            $classActive = ' wishlist-active';
        }
        return $classActive;
    }
}
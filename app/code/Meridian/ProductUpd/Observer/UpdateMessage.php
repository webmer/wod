<?php

namespace Meridian\ProductUpd\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class UpdateMessage implements ObserverInterface
{
    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;

    /** @var \Magento\Framework\UrlInterface */
    protected $url;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    public function __construct(
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->messageManager = $managerInterface;
        $this->url = $url;
        $this->coreRegistry = $coreRegistry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $productId = $observer->getRequest()->getParam('id');
        //$product = $this->productRepository->getById($productId);

        /** @var $currentProduct \Magento\Catalog\Model\Product */
//        $currentProduct = $this->_registry->registry('current_product');
        $currentProduct = $this->coreRegistry->registry('current_product');

        $messageCollection = $this->messageManager->getMessages(true);
        if($messageCollection->getCountByType('success')) {
            $cartLink = '<a href="' . $this->url->getUrl('checkout/cart') . '">View Cart/Checkout</a>';
            //$successMessage = $messageCollection->getLastAddedMessage()->getText() . '  ' . $cartLink;
            //$lastMessage = end($messageCollection->getItems());
            //unset($lastMessage);
            $successMessage = __('You submit you review to %1 product.', $currentProduct->getName());
            $this->messageManager->addSuccess($successMessage);
        }
        return $this;
    }
}
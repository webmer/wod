<?php

namespace Meridian\ProductUpd\Block;

class SocialButton extends \Magento\Framework\View\Element\Template {

    protected $_template = 'share.phtml';

    protected $_scopeConfig;
    protected $_registry;
    private $product;
    /**
     * [__construct description]
     * @param \Magento\Framework\View\Element\Template\Context                $context                 [description]
     * @param array                                                           $data                    [description]
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry ,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_registry = $registry;
    }

    /**
     * @return Product
     */
    private function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->_registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }

    public function getProductName()
    {
        return $this->getProduct()->getName();
    }

    public function getProductDescription()
    {
        return $this->getProduct()->getDescription();
    }

    public function getProductImage()
    {
        $image = strlen($this->getProduct()->getImage()) ? $this->getProduct()->getImage() : '';
        return $image;
    }

    /**
     * Get Store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->_storeManager->getStore()->getName();
    }

    public function getProductGalleryImagesCount()
    {
        return count($this->getProduct()->getMediaGalleryImages());
    }


}
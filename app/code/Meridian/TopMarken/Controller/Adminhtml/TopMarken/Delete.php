<?php

namespace Meridian\TopMarken\Controller\Adminhtml\TopMarken;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Delete extends \Magento\Backend\App\Action
{


    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('topmarken_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Meridian\TopMarken\Model\TopMarken');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The topmarken has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['topmarken_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a topmarken to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}

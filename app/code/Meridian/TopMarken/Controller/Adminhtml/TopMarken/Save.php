<?php

namespace Meridian\TopMarken\Controller\Adminhtml\TopMarken;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\TestFramework\ErrorLog\Logger;
use  Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var UploaderFactory
     */
    protected $uploader;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    protected $adapterFactory;

    /**
     * @var DirectoryList
     */
    protected $directory_list;

    /**
     * @var \Meridian\TopMarken\Model\ResourceModel\TopMarken\CollectionFactory
     */
    protected $_topmarkenCollectionFactory;


    /**
     * \Magento\Backend\Helper\Js $jsHelper
     * @param Action\Context $context
     */
    public function __construct(
        Context $context,
        \Magento\Backend\Helper\Js $jsHelper,
        \Meridian\TopMarken\Model\ResourceModel\TopMarken\CollectionFactory $topmarkenCollectionFactory,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        ProductRepositoryInterface $productRepository
    ) {
        $this->directory_list = $directory_list;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        $this->_jsHelper = $jsHelper;
        $this->adapterFactory = $adapterFactory;
        $this->_topmarkenCollectionFactory = $topmarkenCollectionFactory;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $data['name'] = $data['name-brands'];
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($_FILES['img']) && isset($_FILES['img']['name']) && strlen($_FILES['img']['name'])) {
                /*
                * Save image upload
                */
                try {
                    $base_media_path = 'topmarken/logo/images';
                    $uploader = $this->uploader->create(
                        ['fileId' => 'img']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    $imageAdapter = $this->adapterFactory->create();
                    $uploader->addValidateCallback('image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $mediaDirectory = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath($base_media_path)
                    );
                    $data['img'] = $base_media_path.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['img']) && isset($data['img']['value'])) {
                    if (isset($data['img']['delete'])) {
                        $data['img'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['img']['value'])) {
                        $data['img'] = $data['img']['value'];
                    } else {
                        $data['img'] = null;
                    }
                }
            }



            /** @var \Meridian\TopMarken\Model\TopMarken $model */
            $model = $this->_objectManager->create('Meridian\TopMarken\Model\TopMarken');
            $objDate = $this->_objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
            $id = $this->getRequest()->getParam('topmarken_id');
            if ($id) {
                $model->load($id);
                $data['updeted_at'] = $date = $objDate->gmtDate();
            } else {
                $data['created_at'] = $date = $objDate->gmtDate();
                $data['updated_at'] = $date = $objDate->gmtDate();
            }
            $model->setData($data);
            try {
                $model->save();
                $this->saveProducts($model, $data);
                $this->messageManager->addSuccess(__('You saved this topmarken.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['topmarken_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the topmarken.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['topmarken_id' => $this->getRequest()->getParam('topmarken_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Attach the attachments to topmarken
     * @param $model
     * @param $post
     */
    public function saveProducts($model, $post)
    {

        if (isset($post['products'])) {
            $productIds = $this->_jsHelper->decodeGridSerializedInput($post['products']);
            try {
                $oldProducts = (array) $model->getProducts($model);
                $newProducts = (array) $productIds;
                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
                $connection = $this->_resources->getConnection();
                $table = $this->_resources->getTableName(\Meridian\TopMarken\Model\ResourceModel\TopMarken::TBL_ATT_PRODUCT);
                $insert = array_diff($newProducts, $oldProducts);
                $delete = array_diff($oldProducts, $newProducts);
                $this->updeteProductAttribute($insert, $delete);
                if ($delete) {
                    $where = ['topmarken_id = ?' => (int)$model->getId(), 'product_id IN (?)' => $delete];
                    $connection->delete($table, $where);
                }
                if ($insert) {
                    $data = [];
                    foreach ($insert as $product_id) {
                        $data[] = ['topmarken_id' => (int)$model->getId(), 'product_id' => (int)$product_id];
                    }
                    $connection->insertMultiple($table, $data);
                }
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the topmarken.'));
            }
        }

    }

    protected function updeteProductAttribute($insert, $delete)
    {
        foreach ($insert as $key => $value) {
            $product = $this->productRepository->getById($value);
            $product->setTopmarken($this->getRequest()->getParam('topmarken_id'));
            $product->save();
        }
        foreach ($delete as $key => $value) {
            $product = $this->productRepository->getById($value);
            $product->setTopmarken('0');
            $product->save();

        }
    }
}

<?php

namespace Meridian\TopMarken\Controller\Show;

use Magento\Framework\App\Action\Action;
use \Magento\Framework\Registry;
use \Magento\Framework\App\Request\Http;

class Brand extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Catalog\Model\Layer\Resolver
     */
    protected $layerResolver;

    /**
     * Brand constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver
    ) {
        $this->layerResolver = $layerResolver;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $this->layerResolver->create('customlayer');
        return $this->resultPageFactory->create();
    }
}

<?php

namespace Meridian\TopMarken\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

/**
 * Custom Attribute Renderer
 *
  */
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\Table//AbstractSource
{
    /**
     * @var OptionFactory
     */
    protected $optionFactory;

    protected $topMarkenFactory;
    /**
     * @param OptionFactory $optionFactory
     */
    public function __construct(
        \Meridian\TopMarken\Model\ResourceModel\TopMarken\CollectionFactory $topMarkenFactory
    )
    {
        $this->topMarkenFactory = $topMarkenFactory;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $collection = $this->topMarkenFactory->create();
        $itemsArray = $collection->getItems();

        /* your Attribute options list*/
        $this->_options = $this->toOprionsArray($itemsArray);
        return $this->_options;
    }


    protected function toOprionsArray($itemsArray){
        $option[0]['label']= 'select brand';
        $option[0]['value']= '0';
        $i=0;
        foreach ($itemsArray as $item){
            $i++;
            $option[$i]['label']= $item->getName();
            $option[$i]['value']= $item->getTopmarken_id();

        }
        return$option;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }

    /**
     * Retrieve flat column definition
     *
     * @return array
     */
    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        return [
            $attributeCode => [
                'unsigned' => false,
                'default' => null,
                'extra' => null,
                'type' => Table::TYPE_INTEGER,
                'nullable' => true,
                'comment' => 'Custom Attribute Options  ' . $attributeCode . ' column',
            ],
        ];
    }
}


<?php

namespace Meridian\TopMarken\Model;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeCollectionFactory;

class Layer extends \Magento\Catalog\Model\Layer
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\Layer\Search\CollectionFilter
     */
    protected $filter;
//Apart from the default construct argument you need to add your model from which your product collection is fetched.

    /**
     * Layer constructor.
     * @param \Magento\Catalog\Model\Layer\Search\CollectionFilter $filter
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Layer\ContextInterface $context
     * @param \Magento\Catalog\Model\Layer\StateFactory $layerStateFactory
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product $catalogProduct
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param CategoryRepositoryInterface $categoryRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Model\Layer\Search\CollectionFilter $filter,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Layer\ContextInterface $context,
        \Magento\Catalog\Model\Layer\StateFactory $layerStateFactory,
        AttributeCollectionFactory $attributeCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product $catalogProduct,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        CategoryRepositoryInterface $categoryRepository,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $layerStateFactory,
            $attributeCollectionFactory,
            $catalogProduct,
            $storeManager,
            $registry,
            $categoryRepository,
            $data
        );
        $this->filter = $filter;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->request = $request;
    }

    public function getProductCollection()
    {

        /*Unique id is needed so that when product is loaded /filtered in the custom listing page it will be set in the
         $this->_productCollections array with unique key else you will not get the updated or proper collection.
        */

        if (isset($this->_productCollections['some_uinique_id'])) {
            $collection = $this->_productCollections['some_uinique_id'];
        } else {
            //$collection = Your logic to get your custom collection.
            $attr_code = 'topmarken';
            $attr_option_value = $this->request->getParams('idmarken');
            $collection = $this->productCollectionFactory->create();
            $store_id = $this->request->getParam('store_id');
            $collection->addAttributeToSelect('*')
                ->addAttributeToFilter($attr_code, ['finset' => $attr_option_value['idmarken']])
                ->addAttributeToFilter('visibility', '4')
                ->setStore($store_id)
                ->addFieldToFilter('status', array('eq' => 1))
                ->setOrder('id', 'DESC');
            $this->prepareProductCollection($collection);
            $this->_productCollections['some_uinique_id'] = $collection;
        }

        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return $this
     */
    public function prepareProductCollection($collection)
    {
        $this->filter->filter($collection, $this->getCurrentCategory());
        return $this;
    }

    /**
     * @return \Magento\Catalog\Api\Data\CategoryInterface|mixed
     */
    public function getCurrentCategory()
    {
        $category = $this->registry->registry('current_category');
        if ($category) {
            $this->setData('current_category', $category);
        } else {
            $category = $this->categoryRepository->get($this->getCurrentStore()->getRootCategoryId());
            $this->setData('current_category', $category);
        }
        return $category;
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getCurrentStore()
    {
        return $this->_storeManager->getStore();
    }
}

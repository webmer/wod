<?php
namespace Meridian\TopMarken\Model\ResourceModel\TopMarken;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'topmarken_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Meridian\TopMarken\Model\TopMarken', 'Meridian\TopMarken\Model\ResourceModel\TopMarken');
    }
}

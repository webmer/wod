<?php
namespace Meridian\TopMarken\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('meridian_topmarken')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('meridian_topmarken'))
                ->addColumn(
                    'topmarken_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
                )
                ->addColumn('name', Table::TYPE_TEXT, 255, ['nullable' => true])
                ->addColumn('status', Table::TYPE_BOOLEAN,  null,['nullable' => true])
                ->addColumn('img', Table::TYPE_TEXT, 255, ['nullable' => true])
                ->addColumn('brand_title', Table::TYPE_TEXT, 255, ['nullable' => true])
                ->addColumn('url_key', Table::TYPE_TEXT, 255, ['nullable' => false])
                ->addColumn('topmarken', Table::TYPE_TEXT,  null,['nullable' => true]);

            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('meridian_product_attachment_rel')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('meridian_product_attachment_rel'))
                ->addColumn('topmarken_id', Table::TYPE_INTEGER, 10, ['nullable' => false, 'unsigned' => true])
                ->addColumn('product_id', Table::TYPE_INTEGER, 10, ['nullable' => false, 'unsigned' => true], 'Magento Product Id')
                ->addForeignKey(
                    $installer->getFkName(
                        'meridian_topmarken',
                        'topmarken_id',
                        'meridian_product_attachment_rel',
                        'topmarken_id'
                    ),
                    'topmarken_id',
                    $installer->getTable('meridian_topmarken'),
                    'topmarken_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'meridian_product_attachment_rel',
                        'topmarken_id',
                        'catalog_product_entity',
                        'entity_id'
                    ),
                    'product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('TopMarken Product Attachment relation table');

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}

<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

namespace Meridian\TopMarken\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Rbslider module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $setup->startSetup();

        $version = $context->getVersion();
        $connection = $setup->getConnection();
        if (version_compare($version, '1.0.5','<')) {
            $this->addFieldPositionToMarken($setup);
        }

        if (version_compare($version, '1.0.7','<')) {
            $this->addFieldTypeIdToMarken($setup);
        }

        $setup->endSetup();
    }

    private function addFieldPositionToMarken(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();

        $tableName = 'meridian_topmarken';
        // Get module table
        $tableName = $setup->getTable($tableName);
        // Check if the table already exists
        if ($connection->isTableExists($tableName) == true) {

            $columns = [
                'position' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'size' => 11,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Position'
                ],
            ];

            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }

        }
    }

    private function addFieldTypeIdToMarken(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();

        $tableName = 'meridian_topmarken';
        // Get module table
        $tableName = $setup->getTable($tableName);
        // Check if the table already exists
        if ($connection->isTableExists($tableName) == true) {

            $columns = [
                'type_id' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'size' => 11,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Type Id'
                ],
            ];

            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }

        }
    }

}

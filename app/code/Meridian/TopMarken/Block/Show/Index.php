<?php

namespace Meridian\TopMarken\Block\Show;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $filesystem;

    protected $jsonHelper;
    /**
     * @var \Meridian\TopMarken\Model\ResourceModel\Topmarken\CollectionFactory
     */
    private $topmarkenModelFactory;

    public function __construct(
        Template\Context $context,
        \Meridian\TopMarken\Model\ResourceModel\Topmarken\CollectionFactory $topmarkenModelFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        array $data = [])
    {
        $this->filesystem =  $context->getFilesystem();
        $this->jsonHelper = $jsonHelper;
        $this->topmarkenModelFactory = $topmarkenModelFactory;
        parent::__construct($context, $data);
    }

    public function getMarkens()
    {
        $collection = $this->topmarkenModelFactory->create();
        $collection = $collection
            ->setPageSize(10);
        $collection->getSelect()->orderRand();
        $randomItems = $collection->getItems();
        foreach ($randomItems as $item) {
            $topmarken[$item->getTopmarkenId()] = $item->getImg();
        }
        return $topmarken = $this->jsonHelper->jsonEncode($topmarken);
    }

    public function getMediaPath()
    {
        return $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
    }

    public function getTopmarkenUrl()
    {

    }
}
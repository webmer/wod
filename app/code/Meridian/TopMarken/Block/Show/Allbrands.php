<?php

namespace Meridian\TopMarken\Block\Show;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Page\Config;

class Allbrands extends ListProduct
{
    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 0;
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'Magento\Catalog\Block\Product\ProductList\Toolbar';

    protected $_markenToolbarBlock = 'Meridian\TopMarken\Block\Show\MarkenList\Toolbar';

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $db;


    /**
     * Product Collection
     *
     * @var AbstractCollection
     */
    protected $_productCollection;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var Config
     */
    protected $pageConfig;

    /**
     * @var \Meridian\TopMarken\Model\ResourceModel\TopMarken
     */
    private $topmarkenemodelFactory;


    /**
     * Allbrands constructor.
     * @param ResourceConnection $resource
     * @param \Meridian\TopMarken\Model\ResourceModel\TopMarken\CollectionFactory $topmarkenemodelFactory
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param array $data
     */
    public function __construct(
        ResourceConnection $resource,
        \Meridian\TopMarken\Model\ResourceModel\TopMarken\CollectionFactory $topmarkenemodelFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
        $this->resource = $resource;
        $this->db = $resource->getConnection('default');
        $this->topmarkenemodelFactory = $topmarkenemodelFactory;
        $this->pageConfig = $context->getPageConfig();
        $this->request = $request;
    }


    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return AbstractCollection
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve loaded category collection
     *
     * @return AbstractCollection
     */
    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {

            $prodCollection = $this->getLayer()->getProductCollection();
            $this->_productCollection = $this->getBrands();
            $prodCollection->addAttributeToSelect('topmarken')->addAttributeToFilter('topmarken', array('neq' => 'NULL'));
            $arrayProducts = $this->db->fetchAll($prodCollection->getSelect());
            $arrayProducts = array_column($arrayProducts, 'topmarken');
            $uniqueMarken = array_unique($arrayProducts);
            $this->_productCollection = $this->getBrands()->addFieldToFilter('topmarken_id', array('in' => $uniqueMarken));
        }

        return $this->_productCollection;
    }

    /**
     * @return mixed
     */
    public function getBrands()
    {
        $page = ($this->request->getParam('p')) ? $this->request->getParam('p') : 1;
        $pageSize = ($this->request->getParam('limit')) ? $this->request->getParam('limit') : 15;
        $typeId = $this->request->getParam('type');
        $collection = $this->topmarkenemodelFactory->create();
        //$collection->addFieldToFilter('status',['eq' => self::STATUS_ENABLED]);
        if ($typeId) {
            $collection->addFieldToFilter('type_id', $typeId);
        }
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        $orders = $this->getAvailableOrders();
        if ($orders) {
            $toolbar->setAvailableOrders($orders);
        }
        $sort = $this->getSortBy();
        if ($sort) {
            $toolbar->setDefaultOrder($sort);
        }
        $dir = $this->getDefaultDirection();
        if ($dir) {
            $toolbar->setDefaultDirection($dir);
        }
        $modes = $this->getModes();
        if ($modes) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function getToolbarBlock()
    {
        $blockName = $this->getToolbarBlockName();
        if ($blockName) {
            $block = $this->getLayout()->getBlock($blockName);
            if ($block) {
                return $block;
            }
        }
        //$block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, uniqid(microtime()));
        $block = $this->getLayout()->createBlock($this->_markenToolbarBlock, uniqid(microtime()));
        return $block;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->pageConfig->getTitle()->set(__('Brands'));

        return $this;
    }

}
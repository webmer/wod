<?php

namespace Meridian\TopMarken\Block\Show;

use Magento\Framework\View\Element\Template;


class Filter extends Template
{
    /**
     * @var \Meridian\TopMarken\Model\ResourceModel\Topmarken\CollectionFactory
     */
    private $topmarkenModelFactory;

    protected $jsonHelper;

    protected $request;

    public function __construct(
        Template\Context $context,
        \Meridian\TopMarken\Model\ResourceModel\Topmarken\CollectionFactory  $topmarkenModelFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\Request\Http $request,
        array $data = [])
    {
        $this->jsonHelper = $jsonHelper;
        $this->topmarkenModelFactory = $topmarkenModelFactory;
        $this->request = $request;
        parent::__construct($context, $data);
    }

    public function getFilters()
    {
        $filters = [
            ['value' => '1', 'label' => __('Tabak')],
            ['value' => '2', 'label' => __('Kaffee')],
            ['value' => '3', 'label' => __('Getr&auml;nk')],
            ['value' => '4', 'label' => __('Süsswaren')]
        ];
        return $filters;
    }

    public function getFilterUrl($id = null)
    {
        if(is_null($id)) return;
        $url = $this->getUrl('*/*/*', ['_current' => true,'_use_rewrite' => true, '_query' => ['type' => $id] ]);

        return $url;
    }

    public function isFilterActive($id = null)
    {
        $type = $this->request->getParam('type');
        return $id == $type ? true : false;
    }
}
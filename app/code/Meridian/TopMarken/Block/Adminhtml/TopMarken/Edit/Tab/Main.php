<?php
namespace Meridian\TopMarken\Block\Adminhtml\TopMarken\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
    * @var \Meridian\TopMarken\Helper\Data $helper
    */
    protected $helper;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Meridian\TopMarken\Helper\Data $helper
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Meridian\TopMarken\Helper\Data $helper,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Meridian\TopMarken\Model\TopMarken */
        $model = $this->_coreRegistry->registry('tech_topmarken');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save'),
                    'method' => 'post',
                    'enctype'=>'multipart/form-data'
        ]
        ]
        );


        $form->setHtmlIdPrefix('topmarken_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('TopMarken Information')]);

        if ($model->getId()) {
            $fieldset->addField('topmarken_id', 'hidden', ['name' => 'topmarken_id']);
        }

        $fieldset->addField(
            'img',
            'image',
            [
                'name' => 'img',
                'label' => __('Logo'),
                'title' => __('Logo'),
                'required' => false,
                'note' => 'Allow image type: jpg, jpeg, gif, png',

            ]
        );

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name-brands',
                'label' => __('Brand name'),
                'title' => __('Brand name'),
                'required' => false,
            ]
        );


        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('Status'),
                'values' => [
                    ['value' => '0', 'label' => __('Unpublished')],
                    ['value' => '1', 'label' => __('Published')],

                ],
                'required' => false
            ]
        );


        $fieldset->addField(
            'brand_title',
            'text',
            [
                'name' => 'brand_title',
                'label' => __('Brand title'),
                'title' => __('Brand title'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'url_key',
            'text',
            [
                'name' => 'url_key',
                'label' => __('Url key'),
                'title' => __('Brand title'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'position',
            'text',
            [
                'name' => 'position',
                'label' => __('Position'),
                'title' => __('Position'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'type_id',
            'select',
            [
                'name' => 'type_id',
                'label' => __('Type id'),
                'title' => __('Type id'),
                'required' => false,
                'values' => [
                    ['value' => '', 'label' => __('Select')],
                    ['value' => '1', 'label' => __('Tabak')],
                    ['value' => '2', 'label' => __('Kaffee')],
                    ['value' => '3', 'label' => __('Getr&auml;nk')],
                    ['value' => '4', 'label' => __('Süsswaren')]

                ]
            ]
        );

        $fieldset->addField(
            'topmarken',
            'editor',
            [
                'name' => 'topmarken',
                'label' => __('TopMarken'),
                'title' => __('TopMarken'),
                'rows' => '5',
                'cols' => '30',
                'wysiwyg' => true,
                'config' => $this->_wysiwygConfig->getConfig(),
                'required' => false,
            ]
        );


        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Main');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}

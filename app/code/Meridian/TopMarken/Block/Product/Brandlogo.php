<?php

namespace Meridian\TopMarken\Block\Product;

use Magento\Framework\App\Request\Http;
use Magento\Framework\View\Element\Template\Context;
use Meridian\TopMarken\Model\ResourceModel\TopMarken\Collection;
use Magento\Framework\UrlInterface;


class Brandlogo extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'share.phtml';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */

    protected $_registry;
    /**
     * @var Collection
     */
    protected $topmarkenemodel;

    /**
     * @var
     */
    private $product;

    /**]
     * Brandlogo constructor.
     * @param Context $context
     * @param Collection $topmarkenemodel
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Collection $topmarkenemodel,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_registry = $registry;
        $this->topmarkenemodel = $topmarkenemodel;

    }

    public function getBrandHtml()
    {
        $brandId = $this->getProduct()->getTopmarken();
        if ($brandId) {
            $name = $this->topmarkenemodel->addFieldToFilter('topmarken_id', $brandId)->getFirstItem()->getData('img');
            $mediaPath = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $html = '<li class="top_marken_item top_marken_item_brand"><a href="'. $this->getUrl('marken/show/brand', array('idmarken' => $brandId)).'"><img alt="" src="' . $mediaPath .'resize/' .$name . '"></a></li>';
            return $html;
        }
        return '';
    }

    /**
     * @return Product
     */
    private function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->_registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }
}

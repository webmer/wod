<?php

namespace Meridian\TopMarken\Block\Product;

use Magento\Framework\App\Request\Http;
use Magento\Framework\View\Element\Template\Context;
use Meridian\TopMarken\Model\ResourceModel\TopMarken\Collection;

class CustomListDescription extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Meridian\TopMarken\Model\ResourceModel\TopMarken\Collection
     */
    protected $topmarkenemodel;

    /**
     * CustomListDescription constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Meridian\TopMarken\Model\ResourceModel\TopMarken\Collection $topmarkenemodel
     * @param \Magento\Framework\App\Request\Http $request
     * @param array $data
     */
    public function __construct(
        Context $context,
        Collection $topmarkenemodel,
        Http $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->request = $request;
        $this->topmarkenemodel = $topmarkenemodel;
    }

    /**
     * @return mixed
     */
    public function getMarkenInfo()
    {
        if ($markenId = $this->request->getParams('idmarken')) {

            return $this->topmarkenemodel->addFieldToFilter('topmarken_id', $markenId['idmarken'])->getFirstItem()->getData();
        }

    }
}

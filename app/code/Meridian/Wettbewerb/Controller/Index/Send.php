<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Controller\Index;

use \Magento\Framework\App\Action\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class Send extends Action
{
    /**
     * @var \Meridian\Wettbewerb\Helper\Data $dataHelper
     */
    protected $dataHelper;
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Meridian\Wettbewerb\Helper\Data $dataHelper
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $context->getMessageManager();
        $this->dataHelper = $dataHelper;
        parent::__construct($context);
    }

    /**
     * Wettbewerb Index
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }

        if($this->dataHelper->isEnabled()) {
            $recaptcha_response_field = $this->getRequest()->getPost('g-recaptcha-response');
            if($recaptcha_response_field) {
                $secretKey = $this->dataHelper->getSecretKey();
                $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$recaptcha_response_field."&remoteip=".$_SERVER['REMOTE_ADDR']);
                $result = json_decode($response, true);
                if(isset($result['success']) && ($result['success'])) {

                } else {
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $this->messageManager->addError(
                        __('There was an error with the recaptcha code, please try again.')
                    );
                    $resultRedirect->setPath('wettbewerb/index');
                    return $resultRedirect;
                }
            } else {
                $this->messageManager->addError(
                    __('There was an error with the recaptcha code, please try again.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('wettbewerb/index/index');
                return $resultRedirect;
            }
        }

        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);

            $error = false;

            if (!\Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['vorname']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['strasse']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['plz']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['ort']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['phone']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['birth']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['conditions']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
            }

            $this->validateEmailFormat(trim($post['email']));

            if ($error) {
                throw new \Exception();
            }

            $model = $this->_objectManager->create('Meridian\Wettbewerb\Model\Users');

            $post['created'] = date("Y-m-d H:i:s");

            $model->setData($post);

            $model->save();

            /*$this->messageManager->addSuccess(
                __('Thank you for your participation!')
            );*/
            $this->getDataPersistor()->clear('wettbewerb_form');
            $this->_redirect('wettbewerb/index/success');
            return;
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            $this->getDataPersistor()->set('wettbewerb_form', $post);
            $this->_redirect('wettbewerb/index');
            return;
        }
        //return $this->resultPageFactory->create();
    }

    /**
     * Validates the format of the email address
     *
     * @param string $email
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function validateEmailFormat($email)
    {
        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a valid email address.'));
        }
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}

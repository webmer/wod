<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Controller\Adminhtml;

abstract class Users extends \Magento\Backend\App\Action
{

    protected $_coreRegistry;
    #const ADMIN_RESOURCE = 'Meridian_Wettbewerb::top_level';
    const ADMIN_RESOURCE = 'Meridian::top_level';

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Meridian'), __('Meridian'))
            ->addBreadcrumb(__('Wettbewerb'), __('Wettbewerb'));
        return $resultPage;
    }
}

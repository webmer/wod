<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface UsersRepositoryInterface
{


    /**
     * Save Users
     * @param \Meridian\Wettbewerb\Api\Data\UsersInterface $users
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Meridian\Wettbewerb\Api\Data\UsersInterface $users
    );

    /**
     * Retrieve Users
     * @param string $usersId
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($usersId);

    /**
     * Retrieve Users matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Meridian\Wettbewerb\Api\Data\UsersSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Users
     * @param \Meridian\Wettbewerb\Api\Data\UsersInterface $users
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Meridian\Wettbewerb\Api\Data\UsersInterface $users
    );

    /**
     * Delete Users by ID
     * @param string $usersId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($usersId);
}

<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Api\Data;

interface UsersInterface
{

    const CONDITIONS = 'conditions';
    const STRASSE = 'strasse';
    const ORT = 'ort';
    const USERS_ID = 'users_id';
    const DATE_BIRTH = 'date_birth';
    const PLZ = 'plz';
    const NAME = 'name';
    const CREATED = 'created';
    const VORNAME = 'vorname';
    const ID = 'id';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const GENDER = 'gender';


    /**
     * Get users_id
     * @return string|null
     */
    public function getUsersId();

    /**
     * Set users_id
     * @param string $users_id
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setUsersId($usersId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setId($id);

    /**
     * Get created
     * @return string|null
     */
    public function getCreated();

    /**
     * Set created
     * @param string $created
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setCreated($created);

    /**
     * Get vorname
     * @return string|null
     */
    public function getVorname();

    /**
     * Set vorname
     * @param string $vorname
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setVorname($vorname);

    /**
     * Get strasse
     * @return string|null
     */
    public function getStrasse();

    /**
     * Set strasse
     * @param string $strasse
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setStrasse($strasse);

    /**
     * Get plz
     * @return string|null
     */
    public function getPlz();

    /**
     * Set plz
     * @param string $plz
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setPlz($plz);

    /**
     * Get ort
     * @return string|null
     */
    public function getOrt();

    /**
     * Set ort
     * @param string $ort
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setOrt($ort);

    /**
     * Get email
     * @return string|null
     */
    public function getEmail();

    /**
     * Set email
     * @param string $email
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setEmail($email);

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone();

    /**
     * Set phone
     * @param string $phone
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setPhone($phone);

    /**
     * Get conditions
     * @return string|null
     */
    public function getConditions();

    /**
     * Set conditions
     * @param string $conditions
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setConditions($conditions);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setName($name);

    /**
     * Get date_birth
     * @return string|null
     */
    public function getDateBirth();

    /**
     * Set date_birth
     * @param string $date_birth
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setDateBirth($date_birth);

    /**
     * Get gender
     * @return string|null
     */
    public function getGender();

    /**
     * Set gender
     * @param string $gender
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setGender($gender);
}

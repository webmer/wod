<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Api\Data;

interface UsersSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Users list.
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Meridian\Wettbewerb\Api\Data\UsersInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

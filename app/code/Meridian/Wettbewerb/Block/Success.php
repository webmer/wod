<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Block;

use Magento\Framework\View\Element\Template;

class Success extends Template
{
    /**
     * @var string $_template
     */
    protected $_template = "success.phtml";

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_scopeConfig   = $context->getScopeConfig();
    }

    /**
     * Redirect url
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        $url = !$this->_scopeConfig->getValue('wettbewerb/general/redirect_url') ? $this->getUrl() : $this->_scopeConfig->getValue('wettbewerb/general/redirect_url');
        return $url;
    }

}
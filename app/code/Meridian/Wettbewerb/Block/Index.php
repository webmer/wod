<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Index extends Template
{
    /**
     * @var string $_template
     */
    protected $_template = "index.phtml";

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        //ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        //$this->_scopeConfig = $scopeConfig;
        $this->_scopeConfig   = $context->getScopeConfig();
    }

    /**
     * Retrieve form action
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('wettbewerb/index/send');
    }

    /**
     * @return BlockInterface|mixed
     */
    protected function _getDateBlock()
    {
        $block = $this->getData('_date_block');
        if ($block === null) {
            $block = $this->getLayout()->createBlock('Magento\Framework\View\Element\Html\Date');
            $this->setData('_date_block', $block);
        }
        return $block;
    }

    /**
     * Build date element html string for attribute
     *
     * @param string $part
     * @return string
     */
    public function getDateInput($part = 'from', $title = 'Date of Birth')
    {
        $name = $part;

        return $this->_getDateBlock()->setName(
            $name
        )->setId(
            $name
        )->setTitle(
            __($title)
        )->setImage(
            $this->getViewFileUrl('Magento_Theme::calendar.png')
        )->setDateFormat(
            //$this->_localeDate->getDateFormat(\IntlDateFormatter::FULL)
            "dd.MM.yyyy"
        )->setClass(
            'input-text validate-date-custom'
        )
        ->setYearsRange('-120y:c+nn')
        ->setMaxDate('-1d')
        ->setChangeMonth('true')
        ->setChangeYear('true')
        ->setShowOn('both')
        ->getHtml();
    }

    public function hasHeaderLogo(){
        $logo = $this->_scopeConfig->getValue('wettbewerb/general/logo');
        if(strlen($logo)){
            return true;
        } else {
            return false;
        }
    }

    public function getHeaderLogo(){
        $imgUrl = 'pub/media/wettbewerb/'.$this->_scopeConfig->getValue('wettbewerb/general/logo');

        return $imgUrl;
    }
}
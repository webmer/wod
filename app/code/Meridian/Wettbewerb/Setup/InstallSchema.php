<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_meridian_wettbewerb_users = $setup->getConnection()->newTable($setup->getTable('meridian_wettbewerb_users'));

        $table_meridian_wettbewerb_users->addColumn(
            'users_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );

        $table_meridian_wettbewerb_users->addColumn(
            'created',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'created'
        );
        
        $table_meridian_wettbewerb_users->addColumn(
            'vorname',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'vorname'
        );
        
        $table_meridian_wettbewerb_users->addColumn(
            'strasse',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'strasse'
        );
        
        $table_meridian_wettbewerb_users->addColumn(
            'plz',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'plz'
        );
        
        $table_meridian_wettbewerb_users->addColumn(
            'ort',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'ort'
        );
        
        $table_meridian_wettbewerb_users->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'email'
        );

        $table_meridian_wettbewerb_users->addColumn(
            'phone',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'phone'
        );
        
        $table_meridian_wettbewerb_users->addColumn(
            'conditions',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'conditions'
        );

        $table_meridian_wettbewerb_users->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'name'
        );

        $table_meridian_wettbewerb_users->addColumn(
            'date_birth',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'date birth'
        );

        $table_meridian_wettbewerb_users->addColumn(
            'gender',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'gender'
        );

        $setup->getConnection()->createTable($table_meridian_wettbewerb_users);

        $setup->endSetup();
    }
}

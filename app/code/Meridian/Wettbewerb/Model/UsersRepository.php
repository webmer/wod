<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Model;

use Meridian\Wettbewerb\Model\ResourceModel\Users\CollectionFactory as UsersCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Meridian\Wettbewerb\Api\Data\UsersSearchResultsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Meridian\Wettbewerb\Api\Data\UsersInterfaceFactory;
use Magento\Framework\Api\SortOrder;
use Meridian\Wettbewerb\Model\ResourceModel\Users as ResourceUsers;
use Magento\Framework\Reflection\DataObjectProcessor;
use Meridian\Wettbewerb\Api\UsersRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class UsersRepository implements usersRepositoryInterface
{

    protected $usersCollectionFactory;

    protected $dataObjectProcessor;

    protected $dataUsersFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $resource;

    protected $searchResultsFactory;

    protected $usersFactory;


    /**
     * @param ResourceUsers $resource
     * @param UsersFactory $usersFactory
     * @param UsersInterfaceFactory $dataUsersFactory
     * @param UsersCollectionFactory $usersCollectionFactory
     * @param UsersSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceUsers $resource,
        UsersFactory $usersFactory,
        UsersInterfaceFactory $dataUsersFactory,
        UsersCollectionFactory $usersCollectionFactory,
        UsersSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->usersFactory = $usersFactory;
        $this->usersCollectionFactory = $usersCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataUsersFactory = $dataUsersFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Meridian\Wettbewerb\Api\Data\UsersInterface $users
    ) {
        /* if (empty($users->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $users->setStoreId($storeId);
        } */
        try {
            $users->getResource()->save($users);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the users: %1',
                $exception->getMessage()
            ));
        }
        return $users;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($usersId)
    {
        $users = $this->usersFactory->create();
        $users->getResource()->load($users, $usersId);
        if (!$users->getId()) {
            throw new NoSuchEntityException(__('Users with id "%1" does not exist.', $usersId));
        }
        return $users;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->usersCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Meridian\Wettbewerb\Api\Data\UsersInterface $users
    ) {
        try {
            $users->getResource()->delete($users);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Users: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($usersId)
    {
        return $this->delete($this->getById($usersId));
    }
}

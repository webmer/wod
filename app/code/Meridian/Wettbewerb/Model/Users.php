<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Model;

use Meridian\Wettbewerb\Api\Data\UsersInterface;

class Users extends \Magento\Framework\Model\AbstractModel implements UsersInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Meridian\Wettbewerb\Model\ResourceModel\Users');
    }

    /**
     * Get users_id
     * @return string
     */
    public function getUsersId()
    {
        return $this->getData(self::USERS_ID);
    }

    /**
     * Set users_id
     * @param string $usersId
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setUsersId($usersId)
    {
        return $this->setData(self::USERS_ID, $usersId);
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get created
     * @return string
     */
    public function getCreated()
    {
        return $this->getData(self::CREATED);
    }

    /**
     * Set created
     * @param string $created
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setCreated($created)
    {
        return $this->setData(self::CREATED, $created);
    }

    /**
     * Get vorname
     * @return string
     */
    public function getVorname()
    {
        return $this->getData(self::VORNAME);
    }

    /**
     * Set vorname
     * @param string $vorname
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setVorname($vorname)
    {
        return $this->setData(self::VORNAME, $vorname);
    }

    /**
     * Get strasse
     * @return string
     */
    public function getStrasse()
    {
        return $this->getData(self::STRASSE);
    }

    /**
     * Set strasse
     * @param string $strasse
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setStrasse($strasse)
    {
        return $this->setData(self::STRASSE, $strasse);
    }

    /**
     * Get plz
     * @return string
     */
    public function getPlz()
    {
        return $this->getData(self::PLZ);
    }

    /**
     * Set plz
     * @param string $plz
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setPlz($plz)
    {
        return $this->setData(self::PLZ, $plz);
    }

    /**
     * Get ort
     * @return string
     */
    public function getOrt()
    {
        return $this->getData(self::ORT);
    }

    /**
     * Set ort
     * @param string $ort
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setOrt($ort)
    {
        return $this->setData(self::ORT, $ort);
    }

    /**
     * Get email
     * @return string
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * Set email
     * @param string $email
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
        return $this->getData(self::PHONE);
    }

    /**
     * Set phone
     * @param string $phone
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setPhone($phone)
    {
        return $this->setData(self::PHONE, $phone);
    }

    /**
     * Get conditions
     * @return string
     */
    public function getConditions()
    {
        return $this->getData(self::CONDITIONS);
    }

    /**
     * Set conditions
     * @param string $conditions
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setConditions($conditions)
    {
        return $this->setData(self::CONDITIONS, $conditions);
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get date_birth
     * @return string
     */
    public function getDateBirth()
    {
        return $this->getData(self::DATE_BIRTH);
    }

    /**
     * Set date_birth
     * @param string $date_birth
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setDateBirth($date_birth)
    {
        return $this->setData(self::DATE_BIRTH, $date_birth);
    }

    /**
     * Get gender
     * @return string
     */
    public function getGender()
    {
        return $this->getData(self::GENDER);
    }

    /**
     * Set gender
     * @param string $gender
     * @return \Meridian\Wettbewerb\Api\Data\UsersInterface
     */
    public function setGender($gender)
    {
        return $this->setData(self::GENDER, $gender);
    }
}

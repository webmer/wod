<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\Wettbewerb\Model\ResourceModel;

class Users extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('meridian_wettbewerb_users', 'users_id');
    }
}

<?php

namespace Meridian\ProductsLable\Helper;

class Data extends \Magiccart\Alothemes\Helper\Data
{
    public function getLabels($product)
    {
        $html = '';
        if ($this->isNew($product)) {
            if (!$this->scopeConfig->getValue('new_lable/settings/image')) {
                return '';
            }
            $imgUrl = 'pub/media/lable/' . $this->scopeConfig->getValue('new_lable/settings/image');
            $html .= '<span class="top-left"><span class="labelnew"> <img src="' . '/' . $imgUrl . '"></span></span>';
            return $html;
        }
        $price = $product->getPrice();
        $finalPrice = $product->getFinalPrice();
        if ($price > $finalPrice) {
            $label = $price ? floor(($finalPrice / $price) * 100 - 100) . '%' : '';
            $html .= '<span class="top-left newprice"><span class="labelnewprice">' . __($label) . '</span></span>';
            return $html;
        }

        $lable = $product->getLabel();
        $label = !empty($lable) ? $product->getLabel() : '';

        if ($label) {
            $html .= '<span class="top-left newprice"><span class="labelsale">' . __($label) . '</span></span>';
            return $html;
        }
        return '';
    }
}

?>
<?php
/**
 * Copyright © 2018 Magento, Webmeridian  (http://webmeridian.org/). All rights reserved.
 */

namespace Meridian\Checkout\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use  Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;


    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;



    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory,
        \Magento\Quote\Setup\QuoteSetupFactory $quoteSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addGenderAttributeToCustomerAddress($setup);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->removeGenderAttributeFromForms($setup);
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addDateAttribute($setup);
        }
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->addLiechtensteinStates($setup);
        }
        $setup->endSetup();
    }

    /**
     * Create and add new attribute for customer address - Gender
     *
     * @param ModuleDataSetupInterface $setup
     */
    private function addGenderAttributeToCustomerAddress(ModuleDataSetupInterface $setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerSetup->addAttribute('customer_address', 'gender', [
            'label' => 'Gender',
            'input' => 'select',
            'type' => 'static',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'required' => false,
            'position' => 1,
            'visible' => true,
            'system' => false,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'is_searchable_in_grid' => false,
            'option' => ['values' => ['Male', 'Female']],
            'backend' => ''
        ]);

        $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'gender')
            ->addData([
                'used_in_forms' => [
                    'customer_address_edit',
                    'customer_register_address'
                ]
            ]);
        $attribute->save();

        $connection = $setup->getConnection();

        $connection->addColumn(
            $setup->getTable('quote_address'),
            'gender',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Gender'
            ]
        );

        $connection->addColumn(
            $setup->getTable('sales_order_address'),
            'gender',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Gender'
            ]
        );

        $attribute->save();

    }


    private function removeGenderAttributeFromForms($setup)
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'gender')
            ->addData([
                'used_in_forms' => []
            ]);

        $attribute->save();
    }


    private function addDateAttribute($setup)
    {

        /** @var \Magento\Quote\Setup\QuoteSetup $quoteInstaller */
        $quoteInstaller = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);

        /** @var \Magento\Sales\Setup\SalesSetup $salesInstaller */
        $salesInstaller = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);

        $quoteInstaller->addAttribute(
            'quote',
            'birthday',
            ['type' => Table::TYPE_TEXT, 'length' => '64k', 'nullable' => true]
        );

        $salesInstaller->addAttribute(
            'order',
            'birthday',
            ['type' => Table::TYPE_TEXT, 'length' => '64k', 'nullable' => true, 'grid' => true]
        );

    }

    private function addLiechtensteinStates($setup)
    {

        /**
         * Fill table directory/country_region
         * Fill table directory/country_region_name for en_US locale
         */
        $data = [
            ['LI', 'LI-05', 'Balzers'],
            ['LI', 'LI-01', 'Eschen'],
            ['LI', 'LI-02', 'Gamprin'],
            ['LI', 'LI-03', 'Mauren'],
            ['LI', 'LI-06', 'Planken'],
            ['LI', 'LI-04', 'Ruggell'],
            ['LI', 'LI-07', 'Schaan'],
            ['LI', 'LI-08', 'Schellenberg'],
            ['LI', 'LI-09', 'Triesen'],
            ['LI', 'LI-10', 'Triesenberg'],
            ['LI', 'LI-11', 'Vaduz']
        ];

        foreach ($data as $row) {
            $bind = ['country_id' => $row[0], 'code' => $row[1], 'default_name' => $row[2]];
            $setup->getConnection()->insert($setup->getTable('directory_country_region'), $bind);
            $regionId = $setup->getConnection()->lastInsertId($setup->getTable('directory_country_region'));

            $bind = ['locale' => 'en_US', 'region_id' => $regionId, 'name' => $row[2]];
            $setup->getConnection()->insert($setup->getTable('directory_country_region_name'), $bind);
        }
    }


}

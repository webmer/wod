<?php

namespace Meridian\Checkout\Model;


use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Cms\Block\Widget\Block;
use Magento\Framework\App\ObjectManager;
use Magento\Cms\Model\BlockFactory;
use Magento\Store\Model\StoreManagerInterface ;
use Magento\Framework\UrlInterface;

class CheckoutConfigProvider implements ConfigProviderInterface
{
    protected $cmsBlockWidget;

    protected $blockFactory;

    protected $storeManager;
    protected $urlBuilder;


    public function __construct(Block $block, $blockIdentifier)
    {
        $this->blockFactory = ObjectManager::getInstance()->get(BlockFactory::class);
        $this->storeManager = ObjectManager::getInstance()->get(StoreManagerInterface::class);
        $this->urlBuilder = ObjectManager::getInstance()->get(UrlInterface::class);

        $storeId = $this->storeManager->getStore()->getId();
        /** @var \Magento\Cms\Model\Block $block */
        $blockModel = $this->blockFactory->create();
        $blockModel->setStoreId($storeId)->load($blockIdentifier);

        $this->cmsBlockWidget = $block;
        $block->setData('block_id', $blockModel->getId());
        $block->setTemplate('Magento_Cms::widget/static_block/default.phtml');
    }

    public function getConfig()
    {

        return [
            'cmsBlockHtml' => $this->cmsBlockWidget->toHtml(),
            'customerAccountHistoryUrl' => $this->urlBuilder->getUrl('customer/account')
        ];
    }
}
<?php

namespace Meridian\Checkout\Block\Checkout;

use Magento\Checkout\Helper\Data;
use Magento\Framework\App\ObjectManager;

class LayoutProcessor extends \Magento\Checkout\Block\Checkout\LayoutProcessor
{
    /**
     * @var \Magento\Customer\Model\AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var \Magento\Ui\Component\Form\AttributeMapper
     */
    protected $attributeMapper;

    /**
     * @var AttributeMerger
     */
    protected $merger;

    /**
     * @var \Magento\Customer\Model\Options
     */
    private $options;

    /**
     * @var Data
     */
    private $checkoutDataHelper;

    /**
     * @param \Magento\Customer\Model\AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param \Magento\Ui\Component\Form\AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     */
    public function __construct(
        \Magento\Customer\Model\AttributeMetadataDataProvider $attributeMetadataDataProvider,
        \Magento\Ui\Component\Form\AttributeMapper $attributeMapper,
        \Magento\Checkout\Block\Checkout\AttributeMerger $merger
    ) {
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
    }

    /**
     * @deprecated
     * @return \Magento\Customer\Model\Options
     */
    private function getOptions()
    {
        if (!is_object($this->options)) {
            $this->options = ObjectManager::getInstance()->get(\Magento\Customer\Model\Options::class);
        }
        return $this->options;
    }

    /**
     * @return array
     */
    private function getAddressAttributes()
    {
        /** @var \Magento\Eav\Api\Data\AttributeInterface[] $attributes */
        $attributes = $this->attributeMetadataDataProvider->loadAttributesCollection(
            'customer_address',
            'customer_register_address'
        );

        $elements = [];
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            if ($attribute->getIsUserDefined()) {
                continue;
            }
            $elements[$code] = $this->attributeMapper->map($attribute);
            if (isset($elements[$code]['label'])) {
                $label = $elements[$code]['label'];
                $elements[$code]['label'] = __($label);
            }
        }
        return $elements;
    }

    /**
     * Convert elements(like prefix and suffix) from inputs to selects when necessary
     *
     * @param array $elements address attributes
     * @param array $attributesToConvert fields and their callbacks
     * @return array
     */
    private function convertElementsToSelect($elements, $attributesToConvert)
    {
        $codes = array_keys($attributesToConvert);
        foreach (array_keys($elements) as $code) {
            if (!in_array($code, $codes)) {
                continue;
            }
            $options = call_user_func($attributesToConvert[$code]);
            if (!is_array($options)) {
                continue;
            }
            $elements[$code]['dataType'] = 'select';
            $elements[$code]['formElement'] = 'select';

            foreach ($options as $key => $value) {
                $elements[$code]['options'][] = [
                    'value' => $key,
                    'label' => $value,
                ];
            }
        }

        return $elements;
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $attributesToConvert = [
            'prefix' => [$this->getOptions(), 'getNamePrefixOptions'],
            'suffix' => [$this->getOptions(), 'getNameSuffixOptions'],
        ];

        $elements = $this->getAddressAttributes();
        $elements = $this->convertElementsToSelect($elements, $attributesToConvert);
        // The following code is a workaround for custom address attributes
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']
        )) {
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children'] = $this->processPaymentChildrenComponents(
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children'],
                $elements
            );
        }

        // add billing info in billing step magentoexperts
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billingadd']['children']

        )) {

            $component['billing-address-form'] = $this->getBillingAddressComponent(
                'shared',
                $elements
            );

            $jsLayout['components']['checkout']['children']['steps']['children']['billingadd']['children'] = array_merge_recursive(
                $component,
                $jsLayout['components']['checkout']['children']['steps']['children']['billingadd']['children']
            );

            // add custom attributes
            $skope = 'billingAddress';
            $step = 'billingadd';
            $fieldset = 'billing-address-form';
//            $this->addGenderFieldToLayout($jsLayout, $skope, $step, $fieldset);
        }

        // remove billing form from payment methods magentoexperts
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['payments-list']['children'])) {
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['payments-list']['children'] = [];
        }

        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
        )) {
            $fields = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = $this->merger->merge(
                $elements,
                'checkoutProvider',
                'shippingAddress',
                $fields
            );

            // add custom attributes
            $skope = 'shippingAddress';
            $step = 'shipping-address';
            $fieldset = 'shipping-address-fieldset';
//            $this->addGenderFieldToLayout($jsLayout, $skope, $step, $fieldset);
        }

        return $jsLayout;
    }

    /**
     * Appends billing address form component to payment layout
     *
     * @param array $paymentLayout
     * @param array $elements
     *
     * @return array
     */
    private function processPaymentChildrenComponents(
        array $paymentLayout,
        array $elements
    ) {
        if (!isset($paymentLayout['payments-list']['children'])) {
            $paymentLayout['payments-list']['children'] = [];
        }

        if (!isset($paymentLayout['afterMethods']['children'])) {
            $paymentLayout['afterMethods']['children'] = [];
        }

        // if billing address should be displayed on Payment method or page
        if ($this->getCheckoutDataHelper()->isDisplayBillingOnPaymentMethodAvailable()) {
            $paymentLayout['payments-list']['children'] = array_merge_recursive(
                $paymentLayout['payments-list']['children'],
                $this->processPaymentConfiguration(
                    $paymentLayout['renders']['children'],
                    $elements
                )
            );
        } else {
            $component['billing-address-form'] = $this->getBillingAddressComponent(
                'shared',
                $elements
            );

            $paymentLayout['afterMethods']['children'] = array_merge_recursive(
                $component,
                $paymentLayout['afterMethods']['children']
            );
        }

        return $paymentLayout;
    }

    /**
     * Inject billing address component into every payment component
     *
     * @param array $configuration list of payment components
     * @param array $elements attributes that must be displayed in address form
     * @return array
     */
    private function processPaymentConfiguration(array &$configuration, array $elements)
    {
        $output = [];
        foreach ($configuration as $paymentGroup => $groupConfig) {
            foreach ($groupConfig['methods'] as $paymentCode => $paymentComponent) {
                if (empty($paymentComponent['isBillingAddressRequired'])) {
                    continue;
                }

                $output[$paymentCode . '-form'] = $this->getBillingAddressComponent(
                    $paymentCode,
                    $elements
                );
            }
            unset($configuration[$paymentGroup]['methods']);
        }

        return $output;
    }

    /**
     * Gets billing address component details
     *
     * @param string $paymentCode
     * @param array $elements
     *
     * @return array
     */
    private function getBillingAddressComponent($paymentCode, $elements)
    {
        return [
            'component' => 'Meridian_Checkout/js/view/billing-address',
            'displayArea' => 'billing-address-form-' . $paymentCode,
            'provider' => 'checkoutProvider',
            'deps' => 'checkoutProvider',
            'dataScopePrefix' => 'billingAddress' . $paymentCode,
            'sortOrder' => 1,
            'children' => [
                'address-list' => [
                    'component' => 'Meridian_Checkout/js/view/billing-address/list',
                    'displayArea' => 'billing-address-list',

                ],
                'form-fields' => [
                    'component' => 'uiComponent',
                    'displayArea' => 'additional-fieldsets',
                    'children' => $this->merger->merge(
                        $elements,
                        'checkoutProvider',
                        'billingAddress' . $paymentCode,
                        [
                            'country_id' => [
                                'sortOrder' => 115,
                            ],
                            'region' => [
                                'visible' => false,
                            ],
                            'region_id' => [
                                'component' => 'Magento_Ui/js/form/element/region',
                                'config' => [
                                    'template' => 'ui/form/field',
                                    'elementTmpl' => 'ui/form/element/select',
                                    'customEntry' => 'billingAddress' . $paymentCode . '.region',
                                ],
                                'validation' => [
                                    'required-entry' => true,
                                ],
                                'filterBy' => [
                                    'target' => '${ $.provider }:${ $.parentScope }.country_id',
                                    'field' => 'country_id',
                                ],
                            ],
                            'postcode' => [
                                'sortOrder' => 75,
                                'component' => 'Magento_Ui/js/form/element/post-code',
                                'validation' => [
                                    'required-entry' => true,
                                ],
                            ],
                            'city' => [
                                'sortOrder' => 76,
                            ],
                            'company' => [
                                'validation' => [
                                    'min_text_length' => 0,
                                ],
                            ],
                            'fax' => [
                                'validation' => [
                                    'min_text_length' => 0,
                                ],
                            ],
                            'telephone' => [
                                'sortOrder' => 68,
                                'validation' => [
                                    'validate-phoneLax' => true,
                                ],
                            ],
                            'prefix' => [
                                'component' => 'Meridian_Checkout/js/view/form/element/select',
                                'validation' => [
                                    'required-entry' => true,
                                ],
                            ]
                        ]
                    ),
                ],
            ],
        ];
    }

    /**
     * Get checkout data helper instance
     *
     * @return Data
     * @deprecated
     */
    private function getCheckoutDataHelper()
    {
        if (!$this->checkoutDataHelper) {
            $this->checkoutDataHelper =
                ObjectManager::getInstance()->get(Data::class);
        }

        return $this->checkoutDataHelper;
    }

//    /**
//     * @param array $jsLayout
//     * @param $skope
//     * @param $step
//     */
//    public function addGenderFieldToLayout(array &$jsLayout, $skope, $step, $fieldset)
//    {
//        $customAttributeCode = 'gender';
//        $customField = [
//            'component' => 'Magento_Ui/js/form/element/select',
//            'config' => [
//                // customScope is used to group elements within a single form (e.g. they can be validated separately)
//                'customScope' => $skope.'.custom_attributes',
//                'customEntry' => null,
//                'template' => 'ui/form/field',
//                'elementTmpl' => 'ui/form/element/select'/*,
//                'tooltip' => [
//                    'description' => 'this is what the field is for',
//                ],*/
//            ],
//            'dataScope' => $skope.'.custom_attributes' . '.' . $customAttributeCode,
//            'label' => 'Gender',
//            'provider' => 'checkoutProvider',
//            'sortOrder' => 0,
//            'validation' => [
//                'required-entry' => true
//            ],
//            /*'options' => [],*/
//            'filterBy' => null,
//            'customEntry' => null,
//            'visible' => true,
//        ];
//
//        $jsLayout['components']['checkout']['children']['steps']['children'][$step]['children'][$skope]['children'][$fieldset]['children'][$customAttributeCode] = $customField;
//    }
}

<?php
/**
 * Copyright © 2018 Magento, Webmeridian  (http://webmeridian.org/). All rights reserved.
 */

namespace Meridian\Checkout\Plugin\Model\Quote;

class ShippingAddressManagement
{
    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {
        // todo need to process
        $extAttributes = $address->getExtensionAttributes();
        if (!empty($extAttributes)) {
            $address->setGender($address->getGender());
        }
    }
}
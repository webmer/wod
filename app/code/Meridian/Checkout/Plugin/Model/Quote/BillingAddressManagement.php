<?php
/**
 * Copyright © 2018 Magento, Webmeridian  (http://webmeridian.org/). All rights reserved.
 */

namespace Meridian\Checkout\Plugin\Model\Quote;

class BillingAddressManagement
{
    public function beforeAssign(
        \Magento\Quote\Model\BillingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    ) {
        // todo need to process
        $extAttributes = $address->getExtensionAttributes();
        if (!empty($extAttributes)) {
            $address->setGender($address->getGender());
        }
    }
}
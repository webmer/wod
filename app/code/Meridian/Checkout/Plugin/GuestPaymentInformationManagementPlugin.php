<?php
/**
 * @category Mageants FrontOrderComment
 * @package Mageants_FrontOrderComment
 * @copyright Copyright (c) 2017 Mageants
 * @author Mageants Team <support@mageants.com>
 */

namespace Meridian\Checkout\Plugin;

class GuestPaymentInformationManagementPlugin
{


    /** @var \Magento\Sales\Model\OrderFactory $orderFactory */
    protected $orderFactory;

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;


    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;


    /**
     * @var \Magento\Sales\Model\Order\CustomerManagement
     */
    protected $customerManagement;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;


    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;


    /**
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     */
    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Quote\Model\Quote $quote,
        \Meridian\Checkout\Model\Order\CustomerManagement $customerManagement,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Checkout\Model\Session  $session
    )
    {
        $this->orderFactory = $orderFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quote = $quote;
        $this->customerManagement = $customerManagement;
        $this->customer = $customer;
        $this->storeManager = $storeManager;
        $this->orderRepository = $orderRepository;
        $this->subscriberFactory = $subscriberFactory;
        $this->session = $session;
    }

    /**
     * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
     * @param \Closure $proceed
     * @param int $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     *
     * @return int $orderId
     */
    public function aroundSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $subject,
        \Closure $proceed,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    )
    {
        /** @param string $comment */
        $comment = NULL;
        // get JSON post data
        $request_body = file_get_contents('php://input');
        // decode JSON post data into array
        $data = json_decode($request_body, true);
        // get order comments
        if (isset ($data['paymentMethod']['extension_attributes']['birthday'])) {
            $birthday = $data['paymentMethod']['extension_attributes']['birthday'];
            if ($birthday) {
                // remove any HTML tags
                $birthday = strip_tags($birthday);
            }

        }
        // run parent method and capture int $orderId
        $orderId = $proceed($cartId, $email, $paymentMethod, $billingAddress);

        if ($birthday) {

            /** @param \Magento\Sales\Model\OrderFactory $order */
            $order = $this->orderFactory->create()->load($orderId);
            if ($order->getData('entity_id')) {
                /** @param string $status */
                $order->setBirthday($birthday);
                $order->save();
            }
        }


        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        $this->customer->setData('website_id', $websiteId);

        $customerRegisterEmail = null;
        $customerRegisterPassword = null;
        if (isset ($data['paymentMethod']['extension_attributes']['customer_register_email'])) {
            $customerRegisterEmail = $data['paymentMethod']['extension_attributes']['customer_register_email'];

        }
        if (isset ($data['paymentMethod']['extension_attributes']['customer_register_password'])) {
            $customerRegisterPassword = $data['paymentMethod']['extension_attributes']['customer_register_password'];
        }

        if($customerRegisterEmail && $customerRegisterPassword){
           $emailToCheck = $customerRegisterEmail;
        }else{
            $emailToCheck = $email;
        }

        $customer = $this->customer->loadByEmail($emailToCheck);
        if(!$customer->getId()){
            $createAccount = null;
            if (isset ($data['paymentMethod']['extension_attributes']['create_account'])) {
                $createAccount = $data['paymentMethod']['extension_attributes']['create_account'];
            }

            if($customerRegisterEmail && $customerRegisterPassword){
                $this->customerManagement->create($orderId, $customerRegisterEmail, $customerRegisterPassword);
                $this->session->clearQuote();
            }elseif($createAccount){
                $this->customerManagement->create($orderId);
            }
        }else{
            $currentOrder = $this->orderRepository->get($orderId);
            $currentOrder->setCustomerId($customer->getId());
            $this->orderRepository->save($currentOrder);
        }

        $subscribeNewsletter = null;
        if (isset ($data['paymentMethod']['extension_attributes']['subscribe_newsletter'])) {
            $subscribeNewsletter = $data['paymentMethod']['extension_attributes']['subscribe_newsletter'];
        }
        if($subscribeNewsletter){
            $this->subscriberFactory->create()->subscribe($email);
        }


        return $orderId;
    }

}
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'moment',
    'mageUtils',
    'Magento_Ui/js/form/element/abstract',
    'moment-timezone-with-data'
], function (moment, utils, Abstract) {
    'use strict';

    return Abstract.extend({
        defaults: {

            tooltipTpl: 'Meridian_Checkout/form/element/emptytooltip'

        }

    });
});

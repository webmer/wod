/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/sidebar'
    ],
    function($, Component, quote, stepNavigator, sidebarModel) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Meridian_Checkout/agreements'
            },

            isVisible: function() {
                // return !quote.isVirtual() && stepNavigator.isProcessed('shipping');
                return true;
            },


            placeOrder : function(){


                     $(".payment-method._active").find('.action.primary.checkout').trigger( 'click' );

            }


        });
    }
);

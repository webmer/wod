/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/checkout-data'
    ],
    function (
        ko,
        $,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        quote,
        customer,
        paymentService,
        checkoutData
    ) {
        'use strict';

        var mixin = {
            /**
             * @return {Boolean}
             */
            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);
                $('#additional-info-form .place-order-button').show();
                return true;
            },

            isChecked: ko.computed(function () {
                if(quote.paymentMethod()){
                    $('#additional-info-form .place-order-button').show();
                    return quote.paymentMethod().method;
                }
                return null;
            })
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);

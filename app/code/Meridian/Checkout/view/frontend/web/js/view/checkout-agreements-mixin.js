define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'Magento_CheckoutAgreements/js/model/agreements-modal'
    ], function (ko, $, Component, agreementsModal) {
        'use strict';

        var mixin = {
            modalAgreementId: ko.observable(null),

            defaults: {
                template: 'Meridian_Checkout/checkout-agreements'
            },

            showContent: function (element) {
                this.modalTitle(element.checkboxText);
                this.modalContent(element.content);
                this.modalAgreementId(element.agreementId);
                agreementsModal.showModal();
            }
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);

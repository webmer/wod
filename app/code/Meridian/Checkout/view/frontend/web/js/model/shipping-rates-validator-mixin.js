define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/action/select-shipping-address',
        'uiRegistry'

    ], function ($,
                 ko,
                 addressConverter,
                 selectShippingAddress,
                 uiRegistry
    ) {
        'use strict';

        return function (target) {
            target.validateFields = function() {
                var addressFlat = addressConverter.formDataProviderToFlatData(
                        this.collectObservedData(),
                        'shippingAddress'
                    ),
                    address;

                if (this.validateAddressData(addressFlat)) {
                    addressFlat = uiRegistry.get('checkoutProvider').shippingAddress;
                    var addrKey = addressFlat.getKey();
                    if(addrKey.indexOf('customer-address') == -1){
                        address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
                        selectShippingAddress(address);
                    }

                }
            };


            return target;
        };
    }
);

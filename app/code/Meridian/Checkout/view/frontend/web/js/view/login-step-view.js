define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/password-strength-indicator'
    ],
    function (
        ko,
        $,
        Component,
        _,
        stepNavigator,
        customer,
        Indicator
    ) {
        'use strict';
        /**
         *
         * mystep - is the name of the component's .html template,
         * <Meridian>_<Module>  - is the name of the your module directory.
         *
         */
        return Component.extend({
            defaults: {
                template: 'Meridian_Checkout/login'
            },

            //add here your logic to display step,
            isVisible: ko.observable(!customer.isLoggedIn()),
            isShowNext:  ko.observable(customer.isLoggedIn()),
            isCheckout:  ko.observable(true),
            isRegistration:  ko.observable(false),
            isGuest:  ko.observable(false),
            isEditVisible: ko.observable(false),
            loginFormValidator: null,
            usernameSelector: '#customer-email',
            registerUrl: window.checkoutConfig.registerUrl,
            /**
             *
             * @returns {*}
             */
            initialize: function () {
                this._super();
                 if (!customer.isLoggedIn()) {
                    // if (true) {
                    // register your step
                    stepNavigator.registerStep(
                        //step code will    be used as step content id in the component template
                        'step_login',
                        //step alias
                        null,
                        //step title value
                        'Login',
                        //observable property with logic when display step or hide step
                        this.isVisible,

                        _.bind(this.navigate, this),

                        /**
                         * sort order value
                         * 'sort order value' < 10: step displays before shipping step;
                         * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                         * 'sort order value' > 20 : step displays after payment step
                         */
                        1
                    );
                }

                var hashString = window.location.hash.replace('#', '');
                if (hashString !== '' && hashString !== 'step_login') {
                    this.isEditVisible(true);
                }

                // this.checkFillEmail();
                this.isShowNext(true);
                return this;
            },

            /**
             * The navigate() method is responsible for navigation between checkout step
             * during checkout. You can add custom logic, for example some conditions
             * for switching to your custom step
             */
            navigate: function () {
                this.isVisible(true);
            },

            /**
             * @returns void
             */
            navigateToNextStep: function () {
                stepNavigator.next();
                if(this.isCheckout()){
                    $("div.checkout-createAccount").show();
                    this.isCheckout(false);
                    this.isGuest(true);
                }

                this.isEditVisible(true);
                jQuery('#step_login').removeClass('active');
            },

            toLoginStep: function () {
                if(!customer.isLoggedIn()){
                    jQuery('.action-shipping-rm-edit').trigger('click');
                    jQuery('.action-payment-rm-edit').trigger('click');
                    jQuery('.action-shipping-method-rm-edit').trigger('click');
                    jQuery('.action-mystep-rm-edit').trigger('click');
                    jQuery('#step_login').addClass('active');
                    $('#additional-info-form .place-order-button').hide();

                    stepNavigator.navigateTo('step_login');
                    if(this.isCheckout() || this.isGuest()){
                        $('#checkout-step-login').show();
                        $('#checkout-step-registration').hide();
                        $("div.checkout-createAccount").hide();
                        this.isGuest(false);
                        this.isCheckout(true);
                    }else if(this.isRegistration()){
                        $('#checkout-step-login').hide();
                        $('#checkout-step-registration').show();
                    }
                }
            },

            rmEdit: function () {

                this.isEditAddressVisible(false);
            },

            switchToRegistration: function () {
                $("#checkout-step-registration").addClass('active');

                $("div.checkout-createAccount").hide();
                $('#passwordIndicator')  // we expect that page contains markup <tag data-role="example">..</tag>
                    .each(function (index, element) {
                        Indicator({}, element);  // 'element' is single DOM node.
                    });

                this.isCheckout(false);
                this.isRegistration(true);
            },
            switchToLogin: function () {
                $("#checkout-step-registration").removeClass('active');
                $("div.checkout-createAccount").show();

                this.isCheckout(true);
                this.isRegistration(false);
            }

        });
    }
);

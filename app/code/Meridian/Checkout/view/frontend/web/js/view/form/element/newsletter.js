
define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'Magento_Ui/js/form/element/single-checkbox'

    ],
    function (ko, $, Component, SingleCheckbox) {
        'use strict';
        var checkoutConfig = window.checkoutConfig;


        return SingleCheckbox.extend({
            defaults: {
                visible: true
            }


        });
    }
);

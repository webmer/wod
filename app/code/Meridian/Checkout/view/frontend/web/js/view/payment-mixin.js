define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data'
    ], function (ko, $, stepNavigator, getPaymentInformation, quote, checkoutData) {
        'use strict';

        var mixin = {
            isInternalCall: false,
            isMethodVisible: ko.observable(false),
            isBillingVisible: ko.observable(false),
            isEditAddressVisible: ko.observable(false),
            isEditMethodVisible: ko.observable(false),
            isVisible: ko.observable(false),

            initialize: function () {
                this.template = 'Meridian_Checkout/payment';
                this._super();
                this.isVisible(false);
                this.changeVisible();

                var hashString = window.location.hash.replace('#', '');
                if (hashString === 'payment') {
                    this.isVisible(true);
                    this.isMethodVisible(true);
                    this.isMethodVisible(true);
                    this.isBillingVisible(true);
                    this.isEditAddressVisible(true);

                }

                return this;
            },

            changeVisible: function () {
                var self = this;
                self.isVisible.subscribe(function(isVisible) {
                    if (isVisible) {
                        if (!self.isInternalCall) {
                            self.isBillingVisible(false);
                            self.isMethodVisible(true);
                        }
                    } else {
                        self.isBillingVisible(false);
                        self.isMethodVisible(false);
                    }

                    this.isInternalCall = false;
                });
            },

            navigate: function () {
                this.isVisible(true);
            },

            toBillingAddress: function () {
                this.isInternalCall = true;
                stepNavigator.navigateTo('payment');
                this.isBillingVisible(true);
                this.isMethodVisible(false);
            },

            toPaymentMethods: function () {
                var self = this;

                self.isInternalCall = true;
                stepNavigator.navigateTo('payment');
                getPaymentInformation().done(function () {
                    self.isBillingVisible(false);
                    self.isMethodVisible(true);
                });
            },

            nextPaymentMethods: function () {
                var self = this;

                self.isInternalCall = true;
                stepNavigator.navigateTo('payment');
                getPaymentInformation().done(function () {
                    self.isBillingVisible(false);
                    self.isMethodVisible(true);
                    self.isEditAddressVisible(true);
                });
            },

            rmEdit: function () {

                this.isEditAddressVisible(false);
            }

        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);

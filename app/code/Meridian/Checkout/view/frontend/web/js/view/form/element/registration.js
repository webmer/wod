/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/action/check-email-availability',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/full-screen-loader',
    'mage/translate'
], function (
    $,
    Component,
    ko,
    customer,
    checkEmailAvailability,
    quote,
    checkoutData,
    fullScreenLoader,
    $t
) {
    'use strict';

    var validatedEmail = checkoutData.getValidatedEmailValue();

    if (validatedEmail && !customer.isLoggedIn()) {
        quote.guestEmail = validatedEmail;
    }

    return Component.extend({
        defaults: {
            template: 'Meridian_Checkout/form/element/registration',
            email: checkoutData.getInputFieldEmailValue(),
            emailFocused: false,
            isLoading: false,
            isPasswordVisible: true,
            listens: {
                email: 'emailHasChanged',
                emailFocused: 'validateEmail'
            }
        },
        checkDelay: 2000,
        checkRequest: null,
        isEmailCheckComplete: null,
        isCustomerLoggedIn: customer.isLoggedIn,
        emailCheckTimeout: 0,
        showToLoginLink: ko.observable(false),

        /**
         * Initializes observable properties of instance
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super()
                .observe(['email', 'emailFocused', 'isLoading', 'isPasswordVisible']);

            return this;
        },

        /**
         * Callback on changing email property
         */
        emailHasChanged: function () {

        },

        /**
         * Check email existing.
         */
        checkEmailAvailability: function (email) {
            var self = this;
            this.validateRequest();
            this.isEmailCheckComplete = $.Deferred();
            this.isLoading(true);
            return checkEmailAvailability(this.isEmailCheckComplete, email);

        },

        /**
         * If request has been sent -> abort it.
         * ReadyStates for request aborting:
         * 1 - The request has been set up
         * 2 - The request has been sent
         * 3 - The request is in process
         */
        validateRequest: function () {
            if (this.checkRequest != null && $.inArray(this.checkRequest.readyState, [1, 2, 3])) {
                this.checkRequest.abort();
                this.checkRequest = null;
            }
        },

        /**
         * Local email validation.
         *
         * @param {Boolean} focused - input focus.
         * @returns {Boolean} - validation result.
         */
        validateEmail: function (focused) {
            var loginFormSelector = 'form[data-role=email-with-possible-registration]',
                usernameSelector = loginFormSelector + ' input[name=email]',
                loginForm = $(loginFormSelector),
                validator;

            loginForm.validation();

            if (focused === false && !!this.email()) {
                return !!$(usernameSelector).valid();
            }

            validator = loginForm.validate();

            return validator.check(usernameSelector);
        },

        /**
         * Log in form submitting callback.
         *
         * @param {HTMLElement} registerForm - form element.
         */
        login: function (registerForm) {
            var registerData = {},
                formDataArray = $(registerForm).serializeArray();

            formDataArray.forEach(function (entry) {
                registerData[entry.name] = entry.value;
            });
            var self = this;
            if (this.isPasswordVisible() && $(registerForm).validation() && $(registerForm).validation('isValid')) {
                $('#customer-register-email-error').remove();
                this.checkRequest = this.checkEmailAvailability(registerData.email).always(function() {
                    self.isLoading(false);

                    if(self.checkRequest.responseText == 'true'){
                        self.showToLoginLink(false);
                        jQuery('#step_login button.continue').trigger('click');
                    }else{
                        var errorMessage = $t('This account was used');
                        var errorHtml = '<div for="customer-register-email" generated="true" class="mage-error" id="customer-register-email-error">' + errorMessage + ' </div>';

                        var loginFormSelector = 'form[data-role=email-with-possible-registration]',
                            usernameSelector = loginFormSelector + ' input[name=email]';

                        if($('#customer-register-email-error').length){
                            $('#customer-register-email-error').append(errorMessage);
                        }else{
                            $(usernameSelector).after(errorHtml);
                        }
                        self.showToLoginLink(true);
                    }
                    fullScreenLoader.stopLoader();
                });
            }
        },
        switchToLogin: function () {
            $("span.action-switch-to-login").trigger('click');
        }
    });
});

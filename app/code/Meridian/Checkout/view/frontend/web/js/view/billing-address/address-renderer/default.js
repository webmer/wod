/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'uiRegistry',
    'mage/utils/objects',
    'Magento_Checkout/js/action/create-billing-address'
], function($, ko,
            Component,
            selectBillingAddressAction,
            quote,
            formPopUpState,
            checkoutData,
            customerData,
            registry,
            mageUtils,
            createBillingAddress
) {
    'use strict';
    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Meridian_Checkout/billing-address/address-renderer/default'
        },

        initObservable: function () {
            this._super();
            this.isSelected = ko.computed(function() {
                var isSelected = false;
                var billingAddress = quote.billingAddress();
                if (billingAddress) {
                    isSelected = billingAddress.getKey() == this.address().getKey();
                }
                return isSelected;
            }, this);

            return this;
        },

        getCountryName: function(countryId) {
            return (countryData()[countryId] != undefined) ? countryData()[countryId].name : "";
        },

        /** Set selected customer shipping address  */
        selectAddress: function() {
            selectBillingAddressAction(this.address());
            checkoutData.setSelectedBillingAddress(this.address().getKey());
            jQuery('.action-show-billing-next').trigger('click');
        },
        editCustomerAddress: function() {

            this.selectAddress();

            if (!mageUtils.isObject(this.address().street)) {
                var streetObj = {};
                var streetArray = this.address().street;
                streetArray.forEach(function (item, i, arr) {
                    streetObj[i] = arr[i];
                });
                this.address().street = streetObj;
            }

            if(this.address().regionId){
                this.address().region_id = this.address().regionId;
            }
            if(this.address().countryId){
                this.address().country_id = this.address().countryId;
            }

            checkoutData.setBillingAddressFromData(this.address());

            var billingAddressCode = 'billingAddressshared';
            registry.async('checkoutProvider')(function (checkoutProvider) {
               var defaultAddressData = checkoutProvider.get(billingAddressCode);

                if (defaultAddressData === undefined) {
                    return;
                }
               var  billingAddressData = checkoutData.getBillingAddressFromData();
                if (billingAddressData) {
                    checkoutProvider.set(
                        billingAddressCode,
                        $.extend(true, {}, defaultAddressData, billingAddressData)
                    );
                }

                checkoutProvider.on(billingAddressCode, function (providerBillingAddressData) {
                    checkoutData.setBillingAddressFromData(providerBillingAddressData);
                }, billingAddressCode);
            });
            $('#edit-billing-address').trigger('click');
        },
        editAddress: function() {
            $('#new-billing-address').trigger('click');
        }
    });
});

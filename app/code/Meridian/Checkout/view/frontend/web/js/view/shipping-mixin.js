define(
    [
        'jquery',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/create-shipping-address',
        'uiRegistry',
        'mage/storage',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/utils/objects',
        'Magento_Customer/js/customer-data'
    ], function ($, ko, customer,
                 stepNavigator,
                 addressList,
                 addressConverter,
                 quote,
                 selectShippingAddress,
                 checkoutData,
                 createShippingAddress,
                 registry,
                 storage,
                 fullScreenLoader,
                 mageUtils,
                 customerData
    ) {
        'use strict';
        customerData.reload('directory-data');
        var countryData = customerData.get('directory-data');
        var mixin = {
            visibleAddress: ko.observable(false),
            visibleMethods: ko.observable(false),
            isEditAddressVisible: ko.observable(false),
            isEditMethodVisible: ko.observable(false),
            isInternalCall: false,
            isMethodVisible:ko.observable(false),
            isFormInline: true,
            isFormInlineVisible: ko.observable(false),
            isShowNextButton: ko.observable(true),
            showSaveAddressButton: ko.observable(false),
            shippingFormFilled: false,
            initialize: function () {
                this.template = 'Meridian_Checkout/shipping';
                this.visible = ko.observable(false); // set visible to be initially false to have your step show first
                this._super();
                this.changeVisible();

                if(!this.isCustomerLoggedIn()){
                    this.isFormInlineVisible(true);
                }
                var hashString = window.location.hash.replace('#', '');
                if (hashString === 'shipping') {
                    //|| customer.isLoggedIn()) {
                    this.isEditAddressVisible(true);
                    this.visibleAddress(true);

                } else if (hashString === 'payment') {
                    this.isEditAddressVisible(true);
                    this.isEditMethodVisible(true);
                }

                return this;
            },
            initObservable: function () {
                this._super();

                return this;
            },
            navigate: function () {
                this.visible(true);
            },

            changeVisible: function () {
                var self = this;
                self.visible.subscribe(function(isVisible) {
                    if (isVisible) {
                        var hasNewAddress = addressList.some(function (address) {
                            return address.getType() == 'new-customer-address';
                        });

                        self.isNewAddressAdded(hasNewAddress);
                        if(!self.isInternalCall) {
                            var isUseForShipping = jQuery('#billing-use-for-shipping');
                            if(isUseForShipping.length && isUseForShipping.prop('checked')) {
                                self.visibleAddress(false);
                                self.visibleMethods(true);
                            }else{
                                self.visibleAddress(true);
                                self.visibleMethods(false);
                            }
                        }
                    } else {
                        self.visibleAddress(false);
                        self.visibleMethods(false);
                    }

                    self.isInternalCall = false;
                });
            },

            toShippingAddress: function () {
                if(stepNavigator.getActiveItemIndex() > 1){
                    jQuery('.action-payment-rm-edit').trigger('click');
                    jQuery('.action-shipping-method-rm-edit').trigger('click');
                    this.isInternalCall = true;
                    this.visibleAddress(true);
                    this.visibleMethods(false);
                    stepNavigator.navigateTo('shipping','checkout-step-title');
                    $('#additional-info-form .place-order-button').hide();
                }
            },


            toShippingMethods: function () {
                if(stepNavigator.getActiveItemIndex() > 1) {
                    this.isInternalCall = true;
                    stepNavigator.navigateTo('shipping', 'checkout-step-shipping_method');
                    this.visibleAddress(false);
                    this.visibleMethods(true);
                    $('#additional-info-form .place-order-button').hide();
                }
            },

            nextShippingMethods: function () {
                 if (this.validateShippingInformation()) {
                    this.isInternalCall = true;
                     // window.location = window.checkoutConfig.checkoutUrl + "#" + 'shipping';
                     // document.body.scrollTop = document.documentElement.scrollTop = 0;
                    // stepNavigator.navigateTo('shipping', 'checkout-step-shipping_method');
                    this.visibleAddress(false);
                    this.visibleMethods(true);
                    this.isEditAddressVisible(true);
                    this.isEditMethodVisible(true);
                }
            },
            /**
             * @return {Boolean}
             */
            validateShippingInformation: function () {
                var shippingAddress,
                    addressData,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();

                if (!quote.shippingMethod()) {
                    this.errorValidationMessage('Please specify a shipping method.');

                    return false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (this.isFormInline && this.isFormInlineVisible()) {
                    this.source.set('params.invalid', false);
                    this.source.trigger('shippingAddress.data.validate');

                    if (this.source.get('shippingAddress.custom_attributes')) {
                        this.source.trigger('shippingAddress.custom_attributes.data.validate');
                    }

                    if (this.source.get('params.invalid') ||
                        !quote.shippingMethod().method_code ||
                        !quote.shippingMethod().carrier_code ||
                        !emailValidationResult
                    ) {
                        return false;
                    }

                    shippingAddress = quote.shippingAddress();
                    addressData = addressConverter.formAddressDataToQuoteAddress(
                        this.source.get('shippingAddress')
                    );

                    //Copy form data to quote shipping address object
                    for (var field in addressData) {

                        if (addressData.hasOwnProperty(field) &&
                            shippingAddress.hasOwnProperty(field) &&
                            typeof addressData[field] != 'function' &&
                            _.isEqual(shippingAddress[field], addressData[field])
                        ) {
                            shippingAddress[field] = addressData[field];
                        } else if (typeof addressData[field] != 'function' &&
                            !_.isEqual(shippingAddress[field], addressData[field])) {
                            shippingAddress = addressData;
                            break;
                        }
                    }

                    if (customer.isLoggedIn()) {
                        shippingAddress.save_in_address_book = 1;
                    }
                    selectShippingAddress(shippingAddress);
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();

                    return false;
                }

                return true;
            },
            rmEdit: function () {
                this.isEditAddressVisible(false);
            },


            rmEditShip: function () {
              this.isEditMethodVisible(false);
            },
            showFormInline: function () {
                if(this.shippingFormFilled) {
                    registry.async('checkoutProvider')(function (checkoutProvider) {
                        var shippingAddressData = Object.assign({}, checkoutData.getShippingAddressFromData());
                        shippingAddressData.prefix = '';
                        shippingAddressData.telephone = '';
                        shippingAddressData.city = '';
                        shippingAddressData.company = '';
                        shippingAddressData.firstname = '';
                        shippingAddressData.lastname = '';
                        shippingAddressData.postcode = '';
                        shippingAddressData.region = '';
                        shippingAddressData.region_id = '';
                        shippingAddressData.customerAddressId = null;
                        shippingAddressData.street = null;
                        if (shippingAddressData) {
                            checkoutProvider.set(
                                'shippingAddress',
                                $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                            );
                        }
                    });
                }

                $('.shipping-address-item').removeClass('selected-item');
                this.isShowNextButton(true);
                this.showSaveAddressButton(false);


                this.isFormInlineVisible(true);
            },
            editCustomerAddress: function () {
                this.shippingFormFilled = true;
                this.isFormInlineVisible(true);
                this.isShowNextButton(false);
                this.showSaveAddressButton(true);
                if($( window ).width() < 1024){
                    $('html, body').animate({
                        scrollTop: $("#shipping-new-address-form").offset().top
                    }, 2000);
                }
            },
            hideFormInline: function () {
                this.isFormInlineVisible(false);
            },
            hideNextButton: function(){
                this.isShowNextButton(false);
            },
            showNextButton: function(){
                this.isShowNextButton(true);
            },
            saveCustomerAddress: function (){
                if (this.validateShippingInformation()) {
                    var self = this;
                    var shippingAddressCode = 'shippingAddress';

                    registry.async('checkoutProvider')(function (checkoutProvider) {
                        var addressData = checkoutProvider.get(shippingAddressCode);
                        var saveAddressUrl = 'checkoutaddress/billing/saveaddress';
                        fullScreenLoader.startLoader();
                        storage.post(
                            saveAddressUrl, JSON.stringify(addressData),
                            true
                        ).always(
                            function (response) {

                                if (!response.errors) {
                                    if (mageUtils.isObject(addressData.street)) {
                                        addressData.street = Object.values(addressData.street);
                                    }

                                    var region;

                                    if (addressData.region_id
                                        && countryData()[addressData.country_id]
                                        && countryData()[addressData.country_id]['regions']
                                    ) {
                                        region = countryData()[addressData.country_id]['regions'][addressData.region_id];
                                        if (region) {
                                            addressData.regionCode = region['code'];
                                            addressData.region = region['name'];
                                            addressData.regionId = addressData.region_id;
                                        }
                                    }
                                    if (addressData.country_id) {
                                        addressData.countryId = addressData.country_id;
                                    }
                                    var isAddressUpdated = addressList().some(function (currentAddress, index, addresses) {
                                        if (currentAddress.getKey() == checkoutData.getSelectedShippingAddress()) {
                                            addresses[index] = $.extend(true, {}, currentAddress, addressData);
                                            return true;
                                        }

                                        return false;
                                    });

                                    if (isAddressUpdated) {
                                        addressList.valueHasMutated();
                                    }

                                }

                                self.hideFormInline();
                                self.isShowNextButton(true);
                                self.showSaveAddressButton(false);
                                fullScreenLoader.stopLoader();
                            }
                        );

                    });
                }
            }
        };


        return function (target) {
            return target.extend(mixin);
        };
    }
);


define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'Magento_Ui/js/form/element/single-checkbox',
        'Magento_Customer/js/model/customer'

    ],
    function (ko, $, Component, SingleCheckbox, customer) {
        'use strict';
        var checkoutConfig = window.checkoutConfig;

        return SingleCheckbox.extend({
            defaults: {
                visible: !customer.isLoggedIn()

            }

        });
    }
);

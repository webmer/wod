/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'uiRegistry',
    'mage/utils/objects',
    'Magento_Checkout/js/action/create-shipping-address'
], function($, ko,
            Component,
            selectShippingAddressAction,
            quote,
            formPopUpState,
            checkoutData,
            customerData,
            registry,
            mageUtils,
            createShippingAddress
) {
    'use strict';
    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Meridian_Checkout/shipping-address/address-renderer/default'
        },
        
        initObservable: function () {
            this._super();
            this.isSelected = ko.computed(function() {
                var isSelected = false;
                var shippingAddress = quote.shippingAddress();
                if (shippingAddress) {
                    isSelected = shippingAddress.getKey() == this.address().getKey();
                }
                return isSelected;
            }, this);

            return this;
        },

        getCountryName: function(countryId) {
            return (countryData()[countryId] != undefined) ? countryData()[countryId].name : "";
        },

        /** Set selected customer shipping address  */
        selectAddress: function() {
            jQuery('.action-shipping-hideforminline').trigger('click');
            jQuery('.action-show-shipping-next').trigger('click');
            selectShippingAddressAction(this.address());
            checkoutData.setSelectedShippingAddress(this.address().getKey());

        },

        editAddress: function() {
            this.selectAddress();
            $('#new-shipping-address').trigger('click');
        },
        editCustomerAddress: function() {

            if (!mageUtils.isObject(this.address().street)) {
                var streetObj = {};
                var streetArray = this.address().street;
                streetArray.forEach(function (item, i, arr) {
                    streetObj[i] = arr[i];
                });
                this.address().street = streetObj;
            }
            if(this.address().regionId){
                this.address().region_id = this.address().regionId;
            }
            if(this.address().countryId){
                this.address().country_id = this.address().countryId;
            }

            selectShippingAddressAction(this.address());
            checkoutData.setShippingAddressFromData(this.address());
            checkoutData.setSelectedShippingAddress(this.address().getKey());
            registry.async('checkoutProvider')(function (checkoutProvider) {
                var shippingAddressData = checkoutData.getShippingAddressFromData();
                if (shippingAddressData) {
                    checkoutProvider.set(
                        'shippingAddress',
                        $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                    );
                }
            });
            //$('.shipping-address-item').addClass('selected-item');
            $('#edit-shipping-address').trigger('click');
        }
    });
});

/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true*/
/*global define*/
define(
    [
        'ko',
        'underscore',
        'Magento_Ui/js/form/form',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Ui/js/model/messageList',
        'mage/translate',
        'uiRegistry',
        'jquery',
        'mage/storage',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/utils/objects',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/postcode-validator'
    ],
    function (
        ko,
        _,
        Component,
        customer,
        addressList,
        quote,
        createBillingAddress,
        selectBillingAddress,
        checkoutData,
        checkoutDataResolver,
        customerData,
        setBillingAddressAction,
        globalMessageList,
        $t,
        registry,
        $,
        storage,
        fullScreenLoader,
        mageUtils,
        addressConverter,
        postcodeValidator
    ) {
        'use strict';
        customerData.reload('directory-data');
        var lastSelectedBillingAddress = null,
            newAddressOption = {
                /**
                 * Get new address label
                 * @returns {String}
                 */
                getAddressInline: function () {
                    return $t('New Address');
                },
                customerAddressId: null
            },

            countryData = customerData.get('directory-data'),
            addressOptions = addressList().filter(function (address) {
                return address.getType() == 'customer-address';
            });

        addressOptions.push(newAddressOption);

        return Component.extend({
            defaults: {
                template: 'Meridian_Checkout/billing-address'
            },
            currentBillingAddress: quote.billingAddress,
            addressOptions: addressOptions,
            customerHasAddresses: addressOptions.length > 1,
            billingFormFilled: false,

            /**
             * Init component
             */
            initialize: function () {

                this.applyDefaultBillingAddress();


                this._super();
                quote.paymentMethod.subscribe(function () {
                    checkoutDataResolver.resolveBillingAddress();
                }, this);
            },

            applyDefaultBillingAddress: function (isEstimatedAddress) {
                var billingAddress,
                    addressData,
                    isBillingAddressInitialized;


                billingAddress = quote.billingAddress();

                if (!billingAddress) {

                        isBillingAddressInitialized = addressList.some(function (address) {
                            if (address.isDefaultBilling()) {
                                addressData = address;


                                selectBillingAddress(address);
                                checkoutData.setSelectedBillingAddress(address.getKey());
                                return true;
                            }

                            if(address.getKey() == 'new-customer-address'){

                                this.showNewAddressButton(false);
                            }
                            return false;
                        });
                }
            },
            /**
             * @return {exports.initObservable}
             */
            initObservable: function () {
                this._super()
                    .observe({
                        selectedAddress: null,
                        isAddressDetailsVisible: quote.billingAddress() != null,
                        isAddressFormVisible: !customer.isLoggedIn() || addressOptions.length == 1,
                        isAddressSameAsShipping: false,
                        saveInAddressBook: 1,
                        showNewAddressButton: customer.isLoggedIn(),
                        showSaveAddressButton: ko.observable(false)
                    });

                quote.billingAddress.subscribe(function (newAddress) {
                    if (quote.isVirtual()) {
                        this.isAddressSameAsShipping(false);
                    } else {
                        this.isAddressSameAsShipping(false);
                    }


                    if(customer.isLoggedIn() && quote.billingAddress()){
                        this.hideAddressForm();
                    }

                    if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                        this.saveInAddressBook(newAddress.saveInAddressBook);
                    } else {
                        this.saveInAddressBook(1);
                    }
                    this.isAddressDetailsVisible(true);
                }, this);



                addressList.subscribe(
                    function(changes) {
                        var self = this;
                        changes.forEach(function(change) {
                            if(change.value.getKey() == 'new-customer-address'){
                                self.showNewAddressButton(false);
                            }
                        });
                    },
                    this,
                    'arrayChange'
                );


                return this;
            },

            canUseShippingAddress: ko.computed(function () {
                return !quote.isVirtual() && quote.shippingAddress() && quote.shippingAddress().canUseForBilling();
            }),

            /**
             * @param {Object} address
             * @return {*}
             */
            addressOptionsText: function (address) {
                return address.getAddressInline();
            },

            /**
             * @return {Boolean}
             */
            useShippingAddress: function () {
                if (this.isAddressSameAsShipping()) {
                    selectBillingAddress(quote.shippingAddress());

                    this.updateAddresses();
                    this.isAddressDetailsVisible(true);
                } else {
                    lastSelectedBillingAddress = quote.billingAddress();
                    quote.billingAddress(null);
                    this.isAddressDetailsVisible(false);
                }
                checkoutData.setSelectedBillingAddress(null);

                return true;
            },

            /**
             * Update address action
             */
            updateAddress: function () {
                if (this.selectedAddress() && this.selectedAddress() != newAddressOption) {
                    selectBillingAddress(this.selectedAddress());
                    checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                } else {
                    this.source.set('params.invalid', false);
                    this.source.trigger(this.dataScopePrefix + '.data.validate');

                    if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                        this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                    }

                    if (!this.source.get('params.invalid')) {
                        var addressData = this.source.get(this.dataScopePrefix),
                            newBillingAddress;

                        if (customer.isLoggedIn() && !this.customerHasAddresses) {
                            this.saveInAddressBook(1);
                        }
                        addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                        newBillingAddress = createBillingAddress(addressData);

                        // New address must be selected as a billing address
                        selectBillingAddress(newBillingAddress);
                        checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                        checkoutData.setNewCustomerBillingAddress(addressData);
                    }
                }
                this.updateAddresses();
            },

            /**
             * Edit address action
             */
            editAddress: function () {
                lastSelectedBillingAddress = quote.billingAddress();
                quote.billingAddress(null);
                checkoutData.setSelectedBillingAddress(null);
                this.isAddressDetailsVisible(false);
            },

            /**
             * Cancel address edit action
             */
            cancelAddressEdit: function () {
                this.restoreBillingAddress();

                if (quote.billingAddress()) {
                    // restore 'Same As Shipping' checkbox state
                    this.isAddressSameAsShipping(
                        quote.billingAddress() != null &&
                            quote.billingAddress().getCacheKey() == quote.shippingAddress().getCacheKey() &&
                            !quote.isVirtual()
                    );
                    this.isAddressDetailsVisible(true);
                }
            },

            /**
             * Restore billing address
             */
            restoreBillingAddress: function () {
                if (lastSelectedBillingAddress != null) {
                    selectBillingAddress(lastSelectedBillingAddress);
                }
            },
            saveCustomerAddress: function () {
                if(customer.isLoggedIn() && !this.validateBillingInformation()){
                    var self = this;
                    var billingAddressCode = 'billingAddressshared';

                    registry.async('checkoutProvider')(function (checkoutProvider) {
                        var addressData = checkoutProvider.get(billingAddressCode);
                        delete(addressData.birthday);
                        var saveAddressUrl = 'checkoutaddress/billing/saveaddress';
                        fullScreenLoader.startLoader();
                        storage.post(
                            saveAddressUrl, JSON.stringify(addressData),
                            true
                        ).always(
                            function (response) {

                                if (!response.errors) {
                                    if (mageUtils.isObject(addressData.street)) {
                                        addressData.street = Object.values(addressData.street);
                                    }

                                    var region;

                                    if (addressData.region_id
                                        && countryData()[addressData.country_id]
                                        && countryData()[addressData.country_id]['regions']
                                    ) {
                                        region = countryData()[addressData.country_id]['regions'][addressData.region_id];
                                        if (region) {
                                            addressData.regionCode = region['code'];
                                            addressData.region = region['name'];
                                            addressData.regionId = addressData.region_id;
                                        }
                                    }
                                    if (addressData.country_id) {
                                        addressData.countryId = addressData.country_id;
                                    }

                                    var isAddressUpdated = addressList().some(function (currentAddress, index, addresses) {
                                        if (currentAddress.getKey() == checkoutData.getSelectedBillingAddress()) {
                                            addresses[index] = $.extend(true, {}, currentAddress, addressData);
                                            return true;
                                        }

                                        return false;
                                    });

                                    if (isAddressUpdated) {
                                        addressList.valueHasMutated();
                                    }

                                }

                                self.hideAddressForm();
                                jQuery('.action-show-billing-next').trigger('click');
                                self.showSaveAddressButton(false);
                                fullScreenLoader.stopLoader();
                            }
                        );

                    });
                }
            },

            showAddressForm: function () {

                if(this.billingFormFilled){
                    var billingAddressCode = 'billingAddressshared';
                    registry.async('checkoutProvider')(function (checkoutProvider) {
                        var defaultAddressData = checkoutProvider.get(billingAddressCode);

                        if (defaultAddressData === undefined) {
                            return;
                        }
                        var  billingAddressData = Object.assign({}, checkoutData.getBillingAddressFromData());
                        billingAddressData.prefix = null;
                        billingAddressData.telephone = '';
                        billingAddressData.city = '';
                        billingAddressData.company = '';
                        billingAddressData.firstname = '';
                        billingAddressData.lastname = '';
                        billingAddressData.postcode = '';
                        billingAddressData.region = '';
                        billingAddressData.region_id = null;
                        if ( Array.isArray(billingAddressData.street)) {
                            var streetObj = {};
                            var streetArray = billingAddressData.street;
                            streetArray.forEach(function (item, i, arr) {
                                streetObj[i] = arr[i];
                            });
                            billingAddressData.street = streetObj;
                        }else if(billingAddressData.street != 'undefined'){
                            billingAddressData.street =  {0: ''};
                        }
                        if (billingAddressData) {
                            checkoutProvider.set(
                                billingAddressCode,
                                $.extend(true, {}, defaultAddressData, billingAddressData)
                            );
                        }

                        checkoutProvider.on(billingAddressCode, function (providerBillingAddressData) {
                            checkoutData.setBillingAddressFromData(providerBillingAddressData);
                        }, billingAddressCode);
                    });
                }
                this.showSaveAddressButton(false);
                jQuery('.action-show-billing-next').trigger('click');
                this.isAddressFormVisible(true);
                this.editAddress();
            },
            showEditCustomerAddressForm: function () {
                this.billingFormFilled = true;
                this.isAddressFormVisible(true);
                this.showSaveAddressButton(true);
                jQuery('.action-hide-billing-next').trigger('click');
                if($( window ).width() < 1024){
                    $('html, body').animate({
                        scrollTop: $("#billing-new-address-form").offset().top
                    }, 2000);
                }
            },

            hideAddressForm: function(){
                this.isAddressFormVisible(false);
            },
            /**
             * @param {int} countryId
             * @return {*}
             */
            getCountryName: function (countryId) {
                return countryData()[countryId] != undefined ? countryData()[countryId].name : '';
            },

            /**
             * Trigger action to update shipping and billing addresses
             */
            updateAddresses: function () {
                if (window.checkoutConfig.reloadOnBillingAddress ||
                    !window.checkoutConfig.displayBillingOnPaymentMethod
                ) {
                    setBillingAddressAction(globalMessageList);
                }
            },

            /**
             * Get code
             * @param {Object} parent
             * @returns {String}
             */
            getCode: function (parent) {
                return _.isFunction(parent.getCode) ? parent.getCode() : 'shared';
            },




            validateBillingInformation:  function () {
                jQuery('.action.action-update').trigger('click');
                if (!_.isFunction(this.source)) {
                    this.source = registry.get('checkoutProvider');
                }
                this.source.set('params.invalid', false);
                this.source.trigger('billingAddressshared.data.validate');
                registry.async('checkout.steps.billingadd.billing-address-form.form-fields.postcode')(this.doPostcodeValidation.bind(this));

                return this.source.get('params.invalid');
            },

            doPostcodeValidation: function (postcodeElement) {
                var countryId = $('select[name="country_id"]').val(),
                    validationResult,
                    warnMessage;

                if (postcodeElement == null || postcodeElement.value() == null) {
                    return true;
                }

                postcodeElement.warn(null);
                validationResult = postcodeValidator.validate(postcodeElement.value(), countryId);

                if (!validationResult) {
                    warnMessage = $t('Provided Zip/Postal Code seems to be invalid.');

                    if (postcodeValidator.validatedPostCodeExample.length) {
                        warnMessage += $t(' Example: ') + postcodeValidator.validatedPostCodeExample.join('; ') + '. ';
                    }
                    warnMessage += $t('If you believe it is the right one you can ignore this notice.');
                    postcodeElement.warn(warnMessage);
                }

                return validationResult;



            },


        });
    }
);


define(
    [
        'jquery',
        'Magento_Ui/js/modal/modal',
        'mage/translate'
    ], function ($, modal, $t) {
        'use strict';

        return function (target) {

            target.createModal = function(element) {
                this.modalWindow = element;
                var self = this;
                var options = {
                    'type': 'popup',
                    'modalClass': 'agreements-modal',
                    'responsive': true,
                    'innerScroll': true,
                    'trigger': '.show-modal',
                    'buttons': [
                        {
                            text: $t('Accept'),
                            class: 'action secondary action-hide-popup',
                            click: function() {

                                var agreementId = $(element).find('div[data-agreementid]').data('agreementid');
                                if(agreementId){
                                    $('#agreement_' + agreementId).attr( 'checked', 'checked' );
                                }

                                this.closeModal();
                            }
                        },
                        {
                            text: $t('Decline'),
                            class: 'action secondary action-hide-popup',
                            click: function() {
                                var agreementId = $(element).find('div[data-agreementid]').data('agreementid');
                                if(agreementId){
                                    $('#agreement_' + agreementId).removeAttr('checked');
                                }
                                this.closeModal();
                            }
                        }
                    ]
                };
                modal(options, $(this.modalWindow));
            };

            return target;
        };
    }
);

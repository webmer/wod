define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Meridian_Checkout/js/model/order-comment-validator'
    ],
    function (Component, additionalValidators, commentValidator) {
        'use strict';

        additionalValidators.registerValidator(commentValidator);

        return Component.extend({});
    }
);
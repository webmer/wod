define(['jquery', 'mage/utils/wrapper'], function ($, wrapper) {
    'use strict';
    return function (placeOrderAction) {

        return wrapper.wrap(placeOrderAction, function (originalAction, paymentData, redirectOnSuccess, messageContainer) {

            var data = $("#customer-birthday").attr('value');
            if (!paymentData.extension_attributes) {
                paymentData.extension_attributes = {birthday: data}
            } else {
                paymentData.extension_attributes.birthday = data;
            }


            var createAccount = $("#checkout-createAccount").attr('checked');

            if(createAccount){
                paymentData.extension_attributes.create_account = 1;
            }


            var subscribeNewsletter = $("#checkout-newsletter").attr('checked');

            if(subscribeNewsletter){
                paymentData.extension_attributes.subscribe_newsletter = 1;
            }

            var customerRegisterEmail = $("#customer-register-email").attr('value');
            var customerRegisterPassword = $("#customer-register-password").attr('value');
            var isRegisterformActive = $("#checkout-step-registration").hasClass('active');

            if(customerRegisterEmail && customerRegisterPassword && isRegisterformActive){
                paymentData.extension_attributes.customer_register_email = customerRegisterEmail;
                paymentData.extension_attributes.customer_register_password = customerRegisterPassword;

                var loginData = {};
                loginData['username'] = customerRegisterEmail;
                loginData['password'] = customerRegisterPassword;

                window.checkoutConfig.loginOnSuccess = loginData;
            }
            return originalAction(paymentData, redirectOnSuccess, messageContainer);
        })
    }

});
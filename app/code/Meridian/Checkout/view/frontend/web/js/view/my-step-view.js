
define(

    [
        'ko',
        'jquery',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Checkout/js/model/postcode-validator',
        'Magento_Customer/js/model/address-list',
        'mage/translate',
        'uiRegistry'

    ],
    function (
        ko,
        $,
        Component,
        _,
        stepNavigator,
        customer,
        quote,
        addressConverter,
        checkoutData,
        selectShippingAddress,
        selectBillingAddress,
        createShippingAddress,
        createBillingAddress,
        setBillingAddressAction,
        postcodeValidator,
        addressList,
        $t,
        registry

    ) {
        'use strict';
        /**
         *
         * mystep - is the name of the component's .html template,
         * SDbullion_Nfusions  - is the name of your module directory.
         *
         */
        return Component.extend({
            defaults: {
                template: 'Meridian_Checkout/mystep',

            },

            //add here your logic to display step,
            isEditAddressVisible: ko.observable(false),
            isVisible: ko.observable(false),
            isBillingCloneToShipping: false,
            isShowNextButton: ko.observable(true),
            /**
             *
             * @returns {*}
             */
            initialize: function () {
                this._super();
                // register your step


                stepNavigator.registerStep('billingadd', null, 'Billing', this.isVisible,_.bind(this.navigate, this),2);
                var hashString = window.location.hash.replace('#', '');
                if (hashString === 'billingadd'|| customer.isLoggedIn()){
                    this.isEditAddressVisible(true);
                    this.isVisible(true);
                }
                return this;
            },

            navigate: function () {

            },

            navigateToNextStep: function () {
                if((customer.isLoggedIn() && checkoutData.getSelectedBillingAddress()) || !this.validateBillingInformation()){
                    this.syncWithBilling();
                    stepNavigator.next();
                    this.isEditAddressVisible(true);
                    this.isVisible(false);
                }
            },


            validateBillingInformation:  function () {
                jQuery('.action.action-update').trigger('click');
                if (!_.isFunction(this.source)) {
                    this.source = registry.get('checkoutProvider');
                }
                this.source.set('params.invalid', false);
                this.source.trigger('billingAddressshared.data.validate');


                var  loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());

                    if(!emailValidationResult){
                        this.source.set('params.invalid', true);
                    }
                }

                registry.async('checkout.steps.billingadd.billing-address-form.form-fields.postcode')(this.doPostcodeValidation.bind(this));

                return this.source.get('params.invalid');
            },

            doPostcodeValidation: function (postcodeElement) {
                var countryId = $('select[name="country_id"]').val(),
                    validationResult,
                    warnMessage;

                if (postcodeElement == null || postcodeElement.value() == null) {
                    return true;
                }

                postcodeElement.warn(null);
                validationResult = postcodeValidator.validate(postcodeElement.value(), countryId);

                if (!validationResult) {
                    warnMessage = $t('Provided Zip/Postal Code seems to be invalid.');

                    if (postcodeValidator.validatedPostCodeExample.length) {
                        warnMessage += $t(' Example: ') + postcodeValidator.validatedPostCodeExample.join('; ') + '. ';
                    }
                    warnMessage += $t('If you believe it is the right one you can ignore this notice.');
                    postcodeElement.warn(warnMessage);
                }

                return validationResult;



            },
            toBillingAdress: function () {
                if(stepNavigator.getActiveItemIndex() > 0) {
                    jQuery('.action-shipping-rm-edit').trigger('click');
                    jQuery('.action-payment-rm-edit').trigger('click');
                    jQuery('.action-shipping-method-rm-edit').trigger('click');

                    stepNavigator.navigateTo('billingadd', 'checkout-step-title');
                    $('#additional-info-form .place-order-button').hide();
                    this.isVisible(true);
                }
            },

            hideNextButton: function(){
                this.isShowNextButton(false);
            },
            showNextButton: function(){
                this.isShowNextButton(true);
            },

            rmEdit: function () {

                this.isEditAddressVisible(false);
            },
            syncWithBilling: function() {
                var newShippingAddress, newBillingAddress;
                var isUseForShipping = jQuery('#billing-use-for-shipping');

                if (!_.isFunction(this.source)) {
                    this.source = registry.get('checkoutProvider');
                }
                var addressData;
                if (!customer.isLoggedIn()) {
                    var billingAddressData = this.source.get('billingAddressshared');
                    newBillingAddress = createBillingAddress(billingAddressData);
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setBillingAddressFromData(billingAddressData);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress($.extend(true, {}, billingAddressData));

                    if (isUseForShipping.length && (isUseForShipping.prop('checked') || this.isBillingCloneToShipping)) {

                        var cloneShippingAddressData = Object.assign({}, billingAddressData);
                        if (this.isBillingCloneToShipping && !isUseForShipping.prop('checked')) {
                            cloneShippingAddressData.prefix = '';
                            cloneShippingAddressData.telephone = '';
                            cloneShippingAddressData.city = '';
                            cloneShippingAddressData.company = '';
                            cloneShippingAddressData.firstname = '';
                            cloneShippingAddressData.lastname = '';
                            cloneShippingAddressData.postcode = '';
                            cloneShippingAddressData.region = '';
                            cloneShippingAddressData.region_id = '';
                            cloneShippingAddressData.street = ['', ''];
                        }
                        newShippingAddress = createShippingAddress(cloneShippingAddressData);
                        selectShippingAddress(newShippingAddress);
                        checkoutData.setShippingAddressFromData(cloneShippingAddressData);
                        checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                        checkoutData.setNewCustomerShippingAddress($.extend(true, {}, cloneShippingAddressData));

                        registry.async('checkoutProvider')(function (checkoutProvider) {
                            var shippingAddressData = checkoutData.getShippingAddressFromData();
                            if (shippingAddressData) {
                                checkoutProvider.set(
                                    'shippingAddress',
                                    $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                                );
                            }
                        });
                        this.isBillingCloneToShipping = true;
                    }
                }else{

                    var selectedBillingAddressKey = checkoutData.getSelectedBillingAddress();

                    if(!selectedBillingAddressKey){
                        var billingAddressData = this.source.get('billingAddressshared');
                        newBillingAddress = createBillingAddress(billingAddressData);
                        selectBillingAddress(newBillingAddress);
                        checkoutData.setBillingAddressFromData(billingAddressData);
                        checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                        checkoutData.setNewCustomerBillingAddress($.extend(true, {}, billingAddressData));




                        var cloneShippingAddressData = Object.assign({}, billingAddressData);
                        newShippingAddress = createShippingAddress(cloneShippingAddressData);
                        // selectShippingAddress(newShippingAddress);
                        checkoutData.setShippingAddressFromData(cloneShippingAddressData);
                        checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                        checkoutData.setNewCustomerShippingAddress($.extend(true, {}, cloneShippingAddressData));

                        registry.async('checkoutProvider')(function (checkoutProvider) {
                            var shippingAddressData = checkoutData.getShippingAddressFromData();
                            if (shippingAddressData) {
                                checkoutProvider.set(
                                    'shippingAddress',
                                    $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                                );
                            }
                        });
                    }



                    if (isUseForShipping.length && isUseForShipping.prop('checked')) {
                        if(selectedBillingAddressKey){
                            addressList.some(function (address) {
                                if (address.getKey() == selectedBillingAddressKey) {
                                    selectShippingAddress(address);
                                    return true;
                                }

                                return false;
                            });
                        }
                    }
                }
            }
        });
    }
);
define([
    'Meridian_Checkout/js/action/login-on-success',
        'mage/url'
], function (loginAction, url) {
    'use strict';
        return function (target) {
            target.execute = function () {

                var loginData = window.checkoutConfig.loginOnSuccess;
                var customerAccountHistoryUrl = window.checkoutConfig.customerAccountHistoryUrl;

                if(loginData){
                    var response = loginAction(loginData, customerAccountHistoryUrl);
                    if (response) {
                        window.location.replace(url.build(this.redirectUrl));
                    }

                }else{
                    window.location.replace(url.build(this.redirectUrl));
                }
            };

            return target;
        };
    }
);

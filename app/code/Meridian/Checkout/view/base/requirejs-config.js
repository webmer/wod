var config = {
    'config': {
        'mixins': {
            'Magento_Checkout/js/view/shipping': {
                'Meridian_Checkout/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/view/payment': {
                'Meridian_Checkout/js/view/payment-mixin': true
            },
            'Magento_Checkout/js/view/payment/default': {
                'Meridian_Checkout/js/view/payment/default-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'Meridian_Checkout/js/view/place-order-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Meridian_Checkout/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/action/create-shipping-address': {
                'Meridian_Checkout/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/model/step-navigator': {
                'Meridian_Checkout/js/model/step-navigator-mixin': true
            },
            'Magento_Checkout/js/action/redirect-on-success': {
                'Meridian_Checkout/js/action/redirect-on-success-mixin': true
            },
            'Magento_CheckoutAgreements/js/model/agreements-modal': {
                'Meridian_Checkout/js/model/agreements-modal-mixin': true
            },
            'Magento_CheckoutAgreements/js/view/checkout-agreements': {
                'Meridian_Checkout/js/view/checkout-agreements-mixin': true
            },
            'Magento_Checkout/js/model/shipping-rates-validator': {
                'Meridian_Checkout/js/model/shipping-rates-validator-mixin': true
            }
        }
    }
};

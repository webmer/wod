<?php

namespace Meridian\OrdersSort\Block\Magento\Sales\Order;
use \Magento\Framework\App\ObjectManager;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;


class Recent extends \Magento\Sales\Block\Order\Recent
{
    protected $customerSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        array $data = [])
    {
        $this->customerSession = $customerSession;
        parent::__construct(
            $context,
            $orderCollectionFactory,
            $customerSession,
            $orderConfig,
            $data
        );
    }

    public function getCurrentCustomer(){
        return $this->_customerSession->getCustomer()->getData();
    }
}

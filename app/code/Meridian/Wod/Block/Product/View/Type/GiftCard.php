<?php
namespace Meridian\Wod\Block\Product\View\Type;

class GiftCard extends \Amasty\GiftCard\Block\Product\View\Type\GiftCard
{
    public function isPredefinedAmount()
    {
        return count($this->getListAmounts()) > 0;
    }

    public function getCustomerFirstname()
    {
        $firstName = (string)$this->getCustomerSession()->getCustomer()->getFirstname();

        return trim($firstName);
    }

    public function getCustomerLastname()
    {
        $lastName  = (string)$this->getCustomerSession()->getCustomer()->getLastname();

        return trim($lastName);
    }
}
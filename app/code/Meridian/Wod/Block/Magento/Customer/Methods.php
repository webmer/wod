<?php

namespace Meridian\Wod\Block\Magento\Customer;

use Meridian\Wod\Helper\Data as wodHelper;

class Methods extends \Magento\Framework\View\Element\Template
{
    /**
     * Order Payment
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Payment\Collection
     */
    protected $_orderPayment;

    /**
     * Payment Helper Data
     *
     * @var \Magento\Payment\Helper\Data
     */
    protected $_paymentHelper;

    /**
     * Payment Model Config
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;

    protected $shipconfig;

    protected $scopeConfig;

    public $wodHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\Payment\Collection $orderPayment
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Magento\Payment\Model\Config $paymentConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\Payment\Collection $orderPayment,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magento\Shipping\Model\Config $shipconfig,
        wodHelper $wodHelper,
        array $data = []
    ) {
        $this->_orderPayment = $orderPayment;
        $this->_paymentHelper = $paymentHelper;
        $this->_paymentConfig = $paymentConfig;
        $this->shipconfig = $shipconfig;
        $this->scopeConfig = $context->getScopeConfig();
        $this->wodHelper = $wodHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get all payment methods
     *
     * @return array
     */
    public function getAllPaymentMethods()
    {
        return $this->_paymentHelper->getPaymentMethods();
    }

    /**
     * Get key-value pair of all payment methods
     * key = method code & value = method name
     *
     * @return array
     */
    public function getAllPaymentMethodsList()
    {
        return $this->_paymentHelper->getPaymentMethodList();
    }

    /**
     * Get active/enabled payment methods
     *
     * @return array
     */
    public function getActivePaymentMethods()
    {
        return $this->_paymentConfig->getActiveMethods();
    }

    /**
     * Get payment methods that have been used for orders
     *
     * @return array
     */
    public function getUsedPaymentMethods()
    {
        $collection = $this->_orderPayment;
        $collection->getSelect()->group('method');
        $paymentMethods[] = array('value' => '', 'label' => 'Any');
        foreach ($collection as $col) {
            //if(!$paymentMethods[$col->getMethod()]) {
                $paymentMethods[$col->getMethod()] = array('value' => $col->getMethod(), 'label' => $col->getAdditionalInformation()['method_title']);
            //}
        }
        return $paymentMethods;
    }

    public function getShippingMethods(){

        $activeCarriers = $this->shipconfig->getActiveCarriers();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        foreach($activeCarriers as $carrierCode => $carrierModel)
        {
            $options = array();
            if( $carrierMethods = $carrierModel->getAllowedMethods() )
            {
                foreach ($carrierMethods as $methodCode => $method)
                {
                    $code= $carrierCode.'_'.$methodCode;
                    $options[]=array('value'=>$code,'label'=>$method);

                }
                $carrierTitle =$this->scopeConfig->getValue('carriers/'.$carrierCode.'/title');

            }
            $methods[]=array('value'=>$options,'label'=>$carrierTitle);
        }
        return $methods;

    }
    
    public function getWodAttributes()
    {
        return $this->wodHelper->getWodAttributes();
    }
}
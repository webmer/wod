<?php

namespace Meridian\Wod\Block\Magento\Payment\Info;


use Magento\Framework\View\Element\Template;

class Instructions extends \Magento\Payment\Block\Info\Instructions
{
    protected $_template = 'info/instructions.phtml';

    public function __construct(Template\Context $context, array $data = [])
    {

        parent::__construct($context, $data);
    }


    public function getInstructions()
    {
        if ($this->_instructions === null) {
            $this->_instructions = $this->getInfo()->getAdditionalInformation(
                'instructions'
            ) ?: trim($this->getMethod()->getConfigData('instructions'));
        }
        $customerEmail = '';
        $customerEmail = $this->getMethod()->getInfoInstance()->getOrder()->getCustomerEmail();
        $this->_instructions = str_replace("{{customer_email}}", $customerEmail, $this->_instructions);
        return $this->_instructions;
    }
}

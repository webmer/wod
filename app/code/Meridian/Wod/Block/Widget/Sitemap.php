<?php
/**
 * Magiccart 
 * @category 	Magiccart 
 * @copyright 	Copyright (c) 2014 Magiccart (http://www.magiccart.net/) 
 * @license 	http://www.magiccart.net/license-agreement.html
 * @Author: DOng NGuyen<nguyen@dvn.com>
 * @@Create Date: 2016-01-05 10:40:51
 * @@Modify Date: 2016-06-08 15:01:42
 * @@Function:
 */

namespace Meridian\Wod\Block\Widget;

use Magento\Widget\Block\BlockInterface;

class Sitemap extends \Magento\Framework\View\Element\Template implements BlockInterface
{
    protected $shipconfig;

    protected $scopeConfig;

    protected $customerSession;

    protected $categoryFactory;

    protected $pageFactory;

    protected $_storeManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Shipping\Model\Config $shipconfig,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageFactory,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->shipconfig = $shipconfig;
        $this->customerSession = $customerSession;
        $this->categoryFactory = $categoryFactory;
        $this->pageFactory = $pageFactory;
        $this->_storeManager = $context->getStoreManager();

        parent::__construct($context, $data);
    }

    public function isUserLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    public function getAllCmsPages()
    {
        $cmscollection = $this->getCmsPageCollection();
        $pages = [];
        foreach ($cmscollection as $page) {
            if ($page->getIdentifier() == 'no-route') {
                continue;
            }

            $cmsId = $page->getId();
            $pages[$cmsId]['id'] = $page->getId();
            $pages[$cmsId]['title'] = $page->getTitle();
            $pages[$cmsId]['link_url'] = $page->getIdentifier();
        }

        return $pages;
    }

    protected function getCmsPageCollection()
    {
        $pageCollection = $this->pageFactory->create();
        // add Filter if you want
        $pageCollection->addFieldToFilter('is_active', \Magento\Cms\Model\Page::STATUS_ENABLED);
        $pageCollection->addStoreFilter($this->_storeManager->getStore()->getId());
        return $pageCollection;
    }

    public function catagorylistrecursiveHtml($categoryy)
    {
        $html = '';

        $html .= '<ul class="cat">';
        foreach ($categoryy as $catt) :
            $haschildren = '';
            if (count($catt->getChildrenCategories())) {
                $haschildren = ' isparent';
            }

            $html .= '<li class="category-item level-' . ($catt->getLevel() - 1) . $haschildren .'">';
            $html .= '<a href="'.$catt->getUrl(). '" title="' . $catt->getName() . '">';
            $html .= $catt->getName();
            $html .= '</a>';
            if ($haschildren) :
                $html .= $this->catagorylistrecursiveHtml($catt->getChildrenCategories());
            endif;
            $html .= '</li>';
        endforeach;
        $html .= '</ul>';
        return $html;
    }

    public function getCatCollection()
    {

        $categories = $this->categoryFactory->create()
            ->addAttributeToFilter('is_active', 1)
            ->addLevelFilter(1)
            ->addAttributeToSort('position')
            ->addAttributeToSelect('name');

        return $categories;
    }

}

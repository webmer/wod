<?php
/**
 * Magiccart 
 * @category 	Magiccart 
 * @copyright 	Copyright (c) 2014 Magiccart (http://www.magiccart.net/) 
 * @license 	http://www.magiccart.net/license-agreement.html
 * @Author: DOng NGuyen<nguyen@dvn.com>
 * @@Create Date: 2016-01-05 10:40:51
 * @@Modify Date: 2016-06-08 15:01:42
 * @@Function:
 */

namespace Meridian\Wod\Block\Widget;

use Magento\Widget\Block\BlockInterface;

class Lieferservice extends \Magento\Framework\View\Element\Template implements BlockInterface
{
    protected $shipconfig;

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Shipping\Model\Config $shipconfig,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->shipconfig = $shipconfig;

        parent::__construct($context, $data);
    }

    public function getShippingMethods(){

        $activeCarriers = $this->shipconfig->getActiveCarriers();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        foreach($activeCarriers as $carrierCode => $carrierModel)
        {
            $options = array();
            if( $carrierMethods = $carrierModel->getAllowedMethods() )
            {
                foreach ($carrierMethods as $methodCode => $method)
                {
                    $code= $carrierCode.'_'.$methodCode;
                    $options[]=array('value'=>$code,'label'=>$method);

                }
                $carrierTitle =$this->scopeConfig->getValue('carriers/'.$carrierCode.'/title');

            }
            $methods[]=array('value'=>$options,'label'=>$carrierTitle);
        }
        return $methods;

    }
}

<?php

namespace Meridian\Wod\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Meridian\Wod\Helper\Data as OpcHelper;
use Magento\Checkout\Model\Session\Proxy as CheckoutSession;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;

class ExtendedCheckoutConfigProvider implements ConfigProviderInterface
{
    public $opcHelper;
    public $checkoutSession;
    public $urlBuilder;
    public $assetRepo;

    public function __construct(
        OpcHelper $opcHelper,
        UrlInterface $urlBuilder,
        Repository $assetRepo,
        CheckoutSession $checkoutSession
    )
    {
        $this->assetRepo = $assetRepo;
        $this->opcHelper = $opcHelper;
        $this->urlBuilder = $urlBuilder;
        $this->checkoutSession = $checkoutSession;
    }

    public function getConfig()
    {
        $config = [];

        $config['iwdOpcSettings'] = $this->getSettings();

        return $config;
    }

    public function getSettings()
    {
        $settings = [];

        $settings['defaultShippingMethod'] = $this->opcHelper->getDefaultShippingMethod();
        $settings['defaultPaymentMethod'] = $this->opcHelper->getDefaultPaymentMethod();

        return $settings;
    }
}
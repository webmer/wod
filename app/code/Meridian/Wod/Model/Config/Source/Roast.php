<?php

namespace Meridian\Wod\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;

use Magento\Framework\DB\Ddl\Table;

/**

 * Custom Attribute Renderer

 */

class Roast extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource

{

    /**

     * @var OptionFactory

     */

    protected $optionFactory;

    /**

     * @param OptionFactory $optionFactory

     */

    /**

     * Get all options

     *

     * @return array

     */

    public function getAllOptions()

    {

        /* your Attribute options list*/

        $this->_options=[

            ['label'=>'Not selected', 'value'=>'0'],

            ['label'=>'hell', 'value'=>'1'],

            ['label'=>'hell-mittel', 'value'=>'2'],

            ['label'=>'mittel', 'value'=>'3'],

            ['label'=>'mittel-dunkel', 'value'=>'4'],

            ['label'=>'dunkel', 'value'=>'5']

        ];

        return $this->_options;

    }



}
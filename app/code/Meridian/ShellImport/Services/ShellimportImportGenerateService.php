<?php


namespace Meridian\ShellImport\Services;

use Meridian\ShellImport\Contracts\ShellimportService;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class ShellimportImportGenerateService
 *
 * @package Meridian\ShellImport\Services
 */
class ShellimportImportGenerateService implements ShellimportService
{

    /**
     * @var integer
     */
    private $version;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * ShellimportImportGenerateService constructor.
     *
     * @param DirectoryList $directoryList
     */
    public function __construct( DirectoryList $directoryList )
    {
        $this->version = (int)microtime(true);
        $this->directoryList = $directoryList;
    }

    /**
     * Return generated shellimport version
     * @return string
     */
    public function generate()
    {
        $fileName = "Import".$this->version.".csv.sample";
        $templatePath =
            dirname(dirname(__FILE__))
            . DIRECTORY_SEPARATOR . "Template"
            . DIRECTORY_SEPARATOR .'productimport_template.csv';
        $shellimportFileContent = file_get_contents($templatePath);

        $shellimportDir =
            $this->directoryList->getRoot()
            . DIRECTORY_SEPARATOR . ShellimportService::PRODUCTIMPORT_DIR . DIRECTORY_SEPARATOR;

        try{
            mkdir($shellimportDir, 0777, true);
        } catch ( \Exception $exception ){}

        $file = fopen($shellimportDir . $fileName, "w");
        fwrite($file, $shellimportFileContent);
        fclose($file);
        return "ImportProduct";
    }

}
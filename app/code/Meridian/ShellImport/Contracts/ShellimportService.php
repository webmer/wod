<?php

namespace Meridian\ShellImport\Contracts;

/**
 * Interface ShellimportService
 *
 * @package Meridian\ShellImport\Contracts
 */
interface ShellimportService
{

    /**
     * Shellimport template file name
     *
     * @var string
     */
    const TEMPLATE_FILE_NAME = "productimport_template.csv";

    /**
     * Shellimport dir name
     *
     * @var string
     */
    const PRODUCTIMPORT_DIR = "productsimport";

    /**
     * Content what have to be ignoring on scan dirs
     *
     * @var array
     */
    const IGNORING_CONTENT = ['.', '..', ".gitkeep", ".gitignore","img"];

    /**
     * Shellimport Namespace
     *
     * @var string
     */
    const PRODUCTIMPORT_NAMESPACE = "ShellImport";

}
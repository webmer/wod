<?php
namespace Meridian\ShellImport\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * Class ImportCommand
 *
 * @package Meridian\ShellImport\Console\Command
 */
class ImportCommand extends Command
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\State $state
    ) {
        $this->objectManager = $objectManager;
        $this->state = $state;
        parent::__construct();
    }

    /**
     * protected function configure()
     */
    protected function configure()
    {
        $this->setName('meridian-productsimport:import')
            ->setDescription('Import products');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $import = $this->getImportModel();
        $import->setImagesPath('productsimport/img/');
        $fileNames = glob("productsimport/*.csv");
        foreach ($fileNames as $fileName) {
            try {
                $import->setFile(realpath($fileName));
                $result = $import->execute();

                if ($result) {
                    $output->writeln('<info>The import was successful.</info>');
                } else {
                    $output->writeln('<error>Import failed.</error>');
                    $errors = $import->getErrors();
                    foreach ($errors as $error) {
                        $output->writeln('<error>' . $error->getErrorMessage() . ' - ' . $error->getErrorDescription() . '</error>');
                    }
                }
            } catch (FileNotFoundException $e) {
                $output->writeln('<error>File not found.</error>');

            } catch (\InvalidArgumentException $e) {
                $output->writeln('<error>Invalid source.</error>');
            }
        }
    }

    /**
     * @return \Meridian\ShellImport\Model\Import
     */
    protected function getImportModel()
    {
        $this->state->setAreaCode('adminhtml');
        return $this->objectManager->create('Meridian\ShellImport\Model\Import');
    }
}


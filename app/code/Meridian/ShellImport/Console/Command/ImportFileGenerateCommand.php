<?php

namespace Meridian\ShellImport\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

/**
 *
 *
 * @package Meridian\ShellImport\Console\Command
 */
class ImportFileGenerateCommand extends Command
{

    /**
     * Init command full name
     */
    protected function configure()
    {
        $this->setName('meridian-productsimport:generate')
            ->setDescription('Generate import file.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Run meridian-productsimport:generate.');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $generateService = $objectManager->get('\Meridian\ShellImport\Services\ShellimportImportGenerateService');
        $shellimportClass = $generateService->generate();
        $output->writeln('Generated shellimport: ' . $shellimportClass);
    }

}
<?php

namespace Meridian\ResetEmail\Model;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

use Magento\Customer\Api\Data\ValidationResultsInterfaceFactory;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Customer\Model\Config\Share as ConfigShare;

use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Metadata\Validator;

use Magento\Framework\Api\ExtensibleDataObjectConverter;

use Magento\Framework\App\Config\ScopeConfigInterface;

use Magento\Framework\Encryption\EncryptorInterface as Encryptor;

use Magento\Framework\Event\ManagerInterface;

use Magento\Framework\Exception\InputException;


use Magento\Framework\DataObjectFactory as ObjectFactory;

use Magento\Framework\Registry;

use Psr\Log\LoggerInterface as PsrLogger;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Math\Random;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\StringUtils as StringHelper;
use Magento\Store\Model\StoreManagerInterface;
use Meridian\ResetEmail\Model\EmailNotificationReset;

class AccountManagementReset extends \Magento\Customer\Model\AccountManagement
{

    /**
     * @deprecated
     */

    const XML_PATH_FORGOT_EMAIL_TEMPLATE = 'resetemail/resetemail/password_reset_ones';
    protected $mathRandom;
    protected $emailNotification;
    protected $customerRepository;
    protected $customerFactory;
    protected $meridianNotif;

    public function __construct(
        CustomerFactory $customerFactory,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        Random $mathRandom,
        Validator $validator,
        ValidationResultsInterfaceFactory $validationResultsDataFactory,
        AddressRepositoryInterface $addressRepository,
        CustomerMetadataInterface $customerMetadataService,
        CustomerRegistry $customerRegistry,
        PsrLogger $logger,
        Encryptor $encryptor,
        ConfigShare $configShare,
        StringHelper $stringHelper,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        DataObjectProcessor $dataProcessor,
        Registry $registry,
        CustomerViewHelper $customerViewHelper,
        DateTime $dateTime,
        Customer $customerModel,
        ObjectFactory $objectFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        EmailNotificationReset $meridianNotif
    ) {
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->mathRandom = $mathRandom;
        $this->meridianNotif = $meridianNotif;
        parent::__construct($customerFactory, $eventManager, $storeManager, $mathRandom, $validator, $validationResultsDataFactory, $addressRepository, $customerMetadataService, $customerRegistry, $logger, $encryptor, $configShare, $stringHelper, $customerRepository, $scopeConfig, $transportBuilder, $dataProcessor, $registry, $customerViewHelper, $dateTime, $customerModel, $objectFactory, $extensibleDataObjectConverter);
    }


    public function initiatePasswordResetCustom($email, $template, $websiteId = null)
    {
        if ($websiteId === null) {
//            $websiteId = $this->storeManager->getStore()->getWebsiteId();
        }
        $websiteId = 1;
        // load customer by email
//        $customer = $this->customerRepository->get($email, $websiteId);
//        $limit = 20;
//        $customersCollections = $this->getCustomersCollection();
//        $customersCollections->addAttributeToFilter('password_hash', array('null' => true));
//        $customersCollections->addAttributeToFilter('rp_token', array('null' => true));

//        foreach ($customersCollections as $item) {
            $customer = $this->customerRepository->get($email, $websiteId);
//            $limit--;
            $newPasswordToken = $this->mathRandom->getUniqueHash();
            $this->changeResetPasswordLinkToken($customer, $newPasswordToken);
            try {
                switch ($template) {
                    case AccountManagementReset::EMAIL_RESET:

                        $this->getEmailNotification()->passwordResetConfirmation($customer);
                        break;
                    default:
                        throw new InputException(
                            __(
                                'Invalid value of "%value" provided for the %fieldName field.',
                                ['value' => $template, 'fieldName' => 'email type']
                            )
                        );
                }
                return true;
            } catch (MailException $e) {
                // If we are not able to send a reset password email, this should be ignored
                $this->logger->critical($e);
            }
//            if ($limit < 0) break;
//        }

        return false;
    }

    protected function getCustomersCollection()
    {
        return $this->customerFactory->create()->getCollection()
            ->addAttributeToSelect("*");
    }

    private function getEmailNotification()
    {
        if (!($this->meridianNotif)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                EmailNotificationReset::class
            );
        } else {
            return $this->meridianNotif;
        }
    }
}


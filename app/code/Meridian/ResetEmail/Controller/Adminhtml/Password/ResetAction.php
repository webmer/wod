<?php

namespace Meridian\ResetEmail\Controller\Adminhtml\Password;

use Meridian\ResetEmail\Api\AccountManagementInterface;
use Meridian\ResetEmail\Model\AccountManagementReset;
use Magento\Customer\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\CustomerFactory;


class ResetAction extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    protected $scopeConfig;

    /** @var AccountManagementInterface */
    protected $customerAccountManagement;

    /** @var Escaper */
    protected $escaper;

    /**
     * @var Session
     */
    protected $session;

    protected $customerFactory;

    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementReset $customerAccountManagement,
        Escaper $escaper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        CustomerFactory $customerFactory
    )
    {
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->escaper = $escaper;
        $this->scopeConfig = $scopeConfig;
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
//        $resultRedirect = $this->resultRedirectFactory->create();
//        $email = (string)$this->getRequest()->getPost('email');
//        $email = 'mobile322@gmail.com';
        $amount = $this->scopeConfig->getValue('resetemail/resetemail/amount');
        $customersCollections = $this->getCustomersCollection();
        $customersCollections->addAttributeToFilter('password_hash', array('null' => true));
        $customersCollections->addAttributeToFilter('rp_token', array('null' => true));
        $customersCollections->addAttributeToFilter('email', array('eq' => 'olgagolovnja@gmail.com'));
        $customersCollections->setPageSize($amount);
        $customersCollections->setCurPage(1);
        foreach ($customersCollections as $item) {
            $email = $item->getEmail();
            if ($email) {
                try {
                    $this->customerAccountManagement->initiatePasswordResetCustom(
                        $email,
                        AccountManagementReset::EMAIL_RESET
                    );
                } catch (NoSuchEntityException $exception) {
                    // Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
                }
            }
        }
    }

    protected function getCustomersCollection()
    {
        return $this->customerFactory->create()->getCollection()
            ->addAttributeToSelect("*");

    }

}


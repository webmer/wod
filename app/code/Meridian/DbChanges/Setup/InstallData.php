<?php

namespace Meridian\DbChanges\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory
    )
    {
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }

    /**
     * Upgrades DB for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();
        $select = $connection->select();
        $select->from($setup->getTable('catalog_product_entity_text'))
            ->where('attribute_id = :attribute_id');
        $binds = ['attribute_id' => 155];
        $result = $connection->fetchAll($select, $binds);
        $table = 'catalog_product_entity_varchar';
            foreach ($result as $item){
                $select = $connection->select();
                $select->from($table)
                    ->where('attribute_id = :attribute_id')
                    ->where('entity_id = :entity_id')
                    ->where('store_id = :store_id');
                $binds = ['attribute_id' => 155, 'entity_id' => $item['entity_id'],
                    'store_id' => $item['store_id']
                ];
                $result2 = $connection->fetchAll($select, $binds);
                if(!count($result2)){
                    $sql = "INSERT INTO " . $table . "(attribute_id, store_id, entity_id, value) VALUES
                    ('155', '". $item['store_id'] ."', '" . $item['entity_id'] ."', '" . $item['value']. "')";
                    $connection->query($sql);
                }
            }
        $sql = "DELETE FROM catalog_product_entity_text WHERE attribute_id = '155'";
        $connection->query($sql);
        $setup->endSetup();
    }
}
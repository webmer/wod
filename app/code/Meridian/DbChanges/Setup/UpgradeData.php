<?php
/**
 * Copyright © 2018 Magento, Webmeridian  (http://webmeridian.org/). All rights reserved.
 */

namespace Meridian\DbChanges\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use  Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;


    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;



    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory,
        \Magento\Quote\Setup\QuoteSetupFactory $quoteSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $this->moveDataForLieferzeitAttribute($setup);
        }

        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $this->moveDataForLieferzeitNikotine($setup);
        }

        if (version_compare($context->getVersion(), '0.0.5', '<')) {
            $this->moveDataForLieferzeitWodTar($setup);
        }

        if (version_compare($context->getVersion(), '0.0.6', '<')) {
            $this->moveDataForLieferzeitWodKohlenmonoxid($setup);
        }

        if (version_compare($context->getVersion(), '0.0.7', '<')) {
            $this->moveDataForWodHerkunft($setup);
            $this->moveDataForWodAroma($setup);
        }

        if (version_compare($context->getVersion(), '0.0.8', '<')) {
            $this->moveDataForAttribute($setup, 178);
            $this->moveDataForAttribute($setup, 179);
            $this->moveDataForAttribute($setup, 180);
            $this->moveDataForAttribute($setup, 181);
            $this->moveDataForAttribute($setup, 182);
            $this->moveDataForAttribute($setup, 183);
            $this->moveDataForAttribute($setup, 184);
            $this->moveDataForAttribute($setup, 187);
            $this->moveDataForAttribute($setup, 188);
            $this->moveDataForAttribute($setup, 189);
            $this->moveDataForAttribute($setup, 190);
        }

        if (version_compare($context->getVersion(), '0.0.9', '<')) {
            $this->moveDataForAttribute($setup, 167);
            $this->moveDataForAttribute($setup, 168);
            $this->moveDataForAttribute($setup, 169);
            $this->moveDataForAttribute($setup, 170);
            $this->moveDataForAttribute($setup, 171);
            $this->moveDataForAttribute($setup, 172);
            $this->moveDataForAttribute($setup, 173);
            $this->moveDataForAttribute($setup, 174);
            $this->moveDataForAttribute($setup, 175);
            $this->moveDataForAttribute($setup, 176);
        }

        $setup->endSetup();
    }


    private function moveDataForLieferzeitAttribute($setup)
    {
        $this->moveDataForAttribute($setup, 157);
    }

    private function moveDataForLieferzeitNikotine($setup)
    {
        $this->moveDataForAttribute($setup, 163);
    }


    private function moveDataForLieferzeitWodTar($setup)
    {
        $this->moveDataForAttribute($setup, 164);
    }

    private function moveDataForLieferzeitWodKohlenmonoxid($setup)
    {
        $this->moveDataForAttribute($setup, 165);
    }

    private function moveDataForWodHerkunft($setup)
    {
        $this->moveDataForAttribute($setup, 159);
    }

    private function moveDataForWodAroma($setup)
    {
        $this->moveDataForAttribute($setup, 160);
    }


    private function moveDataForAttribute($setup, $attributeId)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();
        $select = $connection->select();
        $select->from($setup->getTable('catalog_product_entity_text'))
            ->where('attribute_id = :attribute_id');
        $binds = ['attribute_id' => $attributeId];
        $result = $connection->fetchAll($select, $binds);
        $table = 'catalog_product_entity_varchar';
        foreach ($result as $item){
            $select = $connection->select();
            $select->from($table)
                ->where('attribute_id = :attribute_id')
                ->where('entity_id = :entity_id')
                ->where('store_id = :store_id');
            $binds = ['attribute_id' => $attributeId, 'entity_id' => $item['entity_id'],
                'store_id' => $item['store_id']
            ];
            $result2 = $connection->fetchAll($select, $binds);

            $item['value'] = str_replace("'", "\'", $item['value']);
            if(!count($result2)){
                $sql = "INSERT INTO " . $table . "(attribute_id, store_id, entity_id, value) VALUES
                    ('" . $attributeId . "', '". $item['store_id'] ."', '" . $item['entity_id'] ."', '" . $item['value']. "')";
                $connection->query($sql);
            }
        }
        $sql = "DELETE FROM catalog_product_entity_text WHERE attribute_id = '" . $attributeId . "'";
        $connection->query($sql);
        $setup->endSetup();
    }
}

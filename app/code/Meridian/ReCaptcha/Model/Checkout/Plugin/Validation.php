<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\ReCaptcha\Model\Checkout\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Meridian\ReCaptcha\Helper\Data as ReCaptchaHelper;
use Meridian\ReCaptcha\Model\Provider;

/**
 * Class Validation
 */
class Validation
{
    /**
     * Recaptcha Request Variable Name
     */
    const PARAM_RECAPTCHA = 'g-recaptcha-response';
    /**
     * @var \Magento\Checkout\Api\AgreementsValidatorInterface
     */
    protected $agreementsValidator;
    /**
     * @var ReCaptchaHelper
     */
    private $_helper;
    /**
     * @var Provider
     */
    private $_provider;
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;
    /**
     * @var JsonHelper
     */
    private $_jsonHelper;

    public function __construct(
        \Magento\Checkout\Api\AgreementsValidatorInterface $agreementsValidator,
        \Magento\Framework\App\Request\Http $request,
        ReCaptchaHelper $helper,
        Provider $provider,
        JsonHelper $jsonHelper
    ) {
        $this->agreementsValidator = $agreementsValidator;
        $this->_helper = $helper;
        $this->_provider = $provider;
        $this->request = $request;
        $this->_jsonHelper = $jsonHelper;
    }

    /**
     * @param \Magento\Checkout\Api\PaymentInformationManagementInterface $subject
     * @param int $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        if ($this->isEnabled()) {
            $this->validateReCaptcha($paymentMethod);
        }
    }

    /**
     * @param \Magento\Checkout\Api\PaymentInformationManagementInterface $subject
     * @param int $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSavePaymentInformation(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        if ($this->isEnabled()) {
            $this->validateReCaptcha($paymentMethod);
        }
    }

    /**
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return void
     */
    protected function validateReCaptcha(\Magento\Quote\Api\Data\PaymentInterface $paymentMethod)
    {
        $request = $this->request;
        $recaptcha = $this->_getReCaptcha($request);
        if (!empty($recaptcha) &&
            $this->_provider->validate($recaptcha, $this->_helper->getSecretKey())) {
            return true;
        }
    }

    /**
     * Verify if validation needed
     * @return bool
     */
    protected function isEnabled()
    {
        return $this->_helper->isEnabled();
    }

    /**
     * Retrieve ReCAPTCHA Value
     *
     * @param RequestInterface $request
     * @return string|null
     */
    protected function _getReCaptcha(RequestInterface $request)
    {
        return $request->isXmlHttpRequest()
            ? $this->_getDecodeReCaptcha($request)
            : $request->getPost(self::PARAM_RECAPTCHA);
    }

    /**
     * Retrieve Decode ReCAPTCHA Value
     *
     * @param RequestInterface $request
     * @return string|null
     */
    protected function _getDecodeReCaptcha(RequestInterface $request)
    {
        if ($request->getContent()) {
            $params = $this->_jsonHelper->jsonDecode($request->getContent());
            if (isset($params[self::PARAM_RECAPTCHA])) {
                return $params[self::PARAM_RECAPTCHA];
            }
        }
        return null;
    }
}
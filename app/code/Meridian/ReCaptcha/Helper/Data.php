<?php

namespace Meridian\ReCaptcha\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Request\DataPersistorInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MODULE_ENABLED = 'customer/re_captcha/enabled';
    const SITE_KEY = 'customer/re_captcha/site_key';
    const SECRET_KEY = 'customer/re_captcha/secret_key';
    const XML_FORMS = 'customer/re_captcha/forms';

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var array
     */
    private $postData = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * List uses Models of Captcha
     * @var array
     */
    protected $_captcha = [];
    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $_encryptor;

    /**
     * Type of ReCaptcha config path
     */
    const XML_TYPE = 'image';

    /**
     * Size of ReCaptcha config path
     */
    const XML_SIZE = 'normal';

    /**
     * Color theme of ReCaptcha config path
     */
    const XML_THEME = 'light';

    const HANDLE_CHECKOUT_INDEX = 'checkout_index_index';

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->scopeConfig = $context->getScopeConfig();
        $this->_encryptor = $encryptor;
    }

    /**
     * Is the module enabled in configuration.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getStoreConfig(self::MODULE_ENABLED);
    }

    /**
     * The recaptcha site key.
     *
     *
     * @return string
     */
    public function getSiteKey()
    {
        $config = $this->getStoreConfig(self::SITE_KEY);
        return $this->_encryptor->decrypt($config);
    }

    /**
     * The recaptcha secret key.
     *
     *
     * @return string
     */
    public function getSecretKey()
    {
        $config = $this->getStoreConfig(self::SECRET_KEY);
        return $this->_encryptor->decrypt($config);
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getStoreConfig($path)
    {
        $store = $this->getStoreId();
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Retrieve Allowed forms
     *
     * @return  string|null
     */
    public function getForms()
    {
        return $this->getStoreConfig(self::XML_FORMS);
    }

    /**
     * @return mixed
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getStoreId();
    }

    /**
     * Get value from POST by key
     *
     * @param string $key
     * @return string
     */
    public function getPostValue($key)
    {
        if (null === $this->postData) {
            $this->postData = (array) $this->getDataPersistor()->get('wettbewerb_form');
            $this->getDataPersistor()->clear('wettbewerb_form');
        }

        if (isset($this->postData[$key])) {
            return (string) $this->postData[$key];
        }

        return '';
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * Retrieve Assoc Array Of ReCaptcha Configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'enabled' => $this->isEnabled(),
            'type' => $this->getType(),
            'size' => $this->getSize(),
            'theme' => $this->getTheme(),
            'sitekey' => $this->getSiteKey()
        ];
    }

    /**
     * Retrieve Type of ReCaptcha
     *
     * @return  string|null
     */
    public function getType()
    {
        return static::XML_TYPE;
    }

    /**
     * Retrieve Size of ReCaptcha
     *
     * @return  string|null
     */
    public function getSize()
    {
        return static::XML_SIZE;
    }

    /**
     * Retrieve Color theme of ReCaptcha
     *
     * @return  string|null
     */
    public function getTheme()
    {
        return static::XML_THEME;
    }

    /**
     * Checks is Referer Url
     *
     * @param string $post
     * @return bool
     */
    public function isReferer($post)
    {
        if (self::HANDLE_CHECKOUT_INDEX == $post) {
            return true;
        }
        return false;
    }

    /**
     * Get the redirect URL
     *
     * @param  string $post
     * @return string
     */
    public function getRedirectUrl($post)
    {
        $name = self::HANDLE_CHECKOUT_INDEX;
        if ($name == $post) {
            return str_replace('_', '/', $name);
        }
    }
}
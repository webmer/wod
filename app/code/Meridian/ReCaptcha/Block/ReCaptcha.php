<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\ReCaptcha\Block;

class ReCaptcha extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'Meridian_ReCaptcha::captcha.phtml';

    /**
     * @var \Meridian\ReCaptcha\Helper\Data $dataHelper
     */
    protected $dataHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Meridian\ReCaptcha\Helper\Data $dataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Meridian\Wettbewerb\Helper\Data $dataHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->dataHelper = $dataHelper;
    }

    public function isEnabled()
    {
        return $this->dataHelper->isEnabled();
    }

    public function getSiteKey()
    {
        return $this->dataHelper->getSiteKey();
    }
}
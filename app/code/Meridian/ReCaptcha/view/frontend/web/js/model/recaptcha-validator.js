define(
    [
        'jquery',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'mage/url',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Ui/js/model/messageList',
        'mage/translate'
    ],
    function ($, customer, quote, urlBuilder, urlFormatter, errorProcessor, messageContainer, __) {
        'use strict';

        return {
            validate: function () {
                var isCustomer = customer.isLoggedIn();

                if(isCustomer){
                    return true;
                }else{
                    if (grecaptcha.getResponse() != "") {
                        $('.recaptcha-error').first().hide();
                        return true;
                    } else {
                        $('.recaptcha-error').first().show();
                        return false;
                    }
                }
            }
        };
    }
);
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Meridian_ReCaptcha/js/model/recaptcha-validator'
    ],
    function (Component, additionalValidators, commentValidator) {
        'use strict';

        additionalValidators.registerValidator(commentValidator);

        return Component.extend({});
    }
);
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
define([
    'jquery',
    'uiComponent',
	'ko',
	'domReady!'
],
function ($, Component, ko) {
    'use strict';
	
    return Component.extend({
        /**
         * Default Config Option
         * @var {Object}
         */			
        defaults: {
            template: 'Meridian_ReCaptcha/checkout/reCaptcha'
        },
		
        /**
         * Config Option
         * @var {Object}
         */		
		config: {
			enabled: false,
			type: 'image',
			size: 'normal',
			theme: 'light',
			sitekey: null
		},
		
        /**
         * initialize Component
         * @return {Void}
         */	
        initialize: function () {
            this._super();
            if (window[this.configSource] && window[this.configSource].meridianReCaptcha) {
                $.extend(this.config, window[this.configSource].meridianReCaptcha);
            }
        },
		
        /**
         * Check Functionality Should be Enabled
         * @return {Boolean}
         */
        isEnabled: function () {
            //return this.config.enabled;
            return true;
        },
		
        /**
         * Check Compact Size
         * @return {Boolean}
         */
        isCompact: function () {
            return this.config.size == 'compact';
        },
		
        /**
         * Retrieve Config
         * @return {Object}
         */
        getConfig: function () {
            return this.config;
        }	
    });
});

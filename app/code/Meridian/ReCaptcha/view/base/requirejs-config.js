/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
var config = {
    map: {
        '*': {
            'meridian/recaptcha' : 'Meridian_ReCaptcha/js/reCaptcha'
        }
    }
};
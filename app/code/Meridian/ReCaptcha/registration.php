<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Meridian_ReCaptcha',
    __DIR__
);

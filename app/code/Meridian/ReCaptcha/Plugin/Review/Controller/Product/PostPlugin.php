<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */

namespace Meridian\ReCaptcha\Plugin\Review\Controller\Product;

use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Catalog\Model\ProductRepository;
use Meridian\ReCaptcha\Helper\Data;

class PostPlugin
{
    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Post constructor.
     * @param RedirectFactory $resultRedirectFactory
     * @param ManagerInterface $messageManager
     * @param Data $dataHelper
     */
    public function __construct(
        RedirectFactory $resultRedirectFactory,
        ManagerInterface $messageManager,
        ProductRepository $productRepository,
        Data $dataHelper
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager = $messageManager;
        $this->productRepository = $productRepository;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param \Magento\Review\Controller\Product\Post $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\Controller\Result\Redirect|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundExecute(
        \Magento\Review\Controller\Product\Post $subject,
        \Closure $proceed
    ) {
        if ($this->dataHelper->isEnabled()) {
            $recaptchaResponse = $subject->getRequest()->getPost('g-recaptcha-response');
            $productId = $subject->getRequest()->getParam('id');

            if ($recaptchaResponse) {
                $secretKey = $this->dataHelper->getSecretKey();
                $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" .
                    $secretKey . "&response=" . $recaptchaResponse . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
                $result = json_decode($response, true);

                if (isset($result['success']) && $result['success']) {
                    return $proceed();
                } else {
                    return $this->recaptchaError($productId);
                }
            } else {
                return $this->recaptchaError($productId);
            }
        }

        return $proceed();
    }

    public function afterExecute(
        \Magento\Review\Controller\Product\Post $subject,
        $result
    ) {
        $messageCollection = $this->messageManager->getMessages(true);
        if($messageCollection->getCountByType('success')) {
            $productId = $subject->getRequest()->getParam('id');
            $product = $this->productRepository->getById($productId);
            $successMessage = __('You submit you review to %1 product.', $product->getName());
            $this->messageManager->addSuccess($successMessage);
        }

        return $result;
    }

    /**
     * @param $productId
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function recaptchaError($productId)
    {
        $product = $this->productRepository->getById($productId);
        $productUrl = $product->getUrlModel()->getUrl($product);
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->messageManager->addErrorMessage(__('Please Fill Recaptcha To Continue'));
        $resultRedirect->setPath($productUrl);

        return $resultRedirect;
    }
}
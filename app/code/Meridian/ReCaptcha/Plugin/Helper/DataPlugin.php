<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\ReCaptcha\Plugin\Helper;

class DataPlugin
{
    protected $request;

    public function __construct(
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->request = $request;
    }


    public function afterGetRedirectUrl($post, $result)
    {
        if($result == 'catalog/product/view'){
            $id = $this->request->getParam('id');
            if($id){
                $result .= '/id/'.$id;
            }
        }
        return $result;
    }


    public function aroundIsFormAllowed($subject, $proceed, $name)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');

        $originalResult = $proceed($name);

        if($customerSession->isLoggedIn() && $name == 'checkout_index_index') {
            return false;
        }else{
            return $originalResult;
        }
    }
}
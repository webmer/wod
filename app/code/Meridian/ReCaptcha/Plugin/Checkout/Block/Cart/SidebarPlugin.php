<?php
/**
 * (с) WebMeridian - 2018. All rights reserved.
 */
namespace Meridian\ReCaptcha\Plugin\Checkout\Block\Cart;

use Magento\Checkout\Block\Cart\Sidebar;
use Meridian\ReCaptcha\Model\Checkout\ConfigProvider;
use Meridian\ReCaptcha\Helper\Data as ReCaptchaHelper;

/**
 * Checkout Sidebar Plugin
 */
class SidebarPlugin
{
    /**
     * Config Provider
     *
     * @var \Meridian\ReCaptcha\Model\Checkout\ConfigProvider
     */
    protected $_configProvider;
    /**
     * @var ReCaptchaHelper
     */
    private $_helper;

    /**
     * Initialize Plugin
     *
     * @param ConfigProvider $configProvider
     */
    public function __construct(
        ConfigProvider $configProvider,
        ReCaptchaHelper $helper
    ) {
        $this->_configProvider = $configProvider;
        $this->_helper = $helper;
    }

    /**
     * Retrieve Minicart Config
     *
     * @param Sidebar $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(Sidebar $subject, array $result)
    {
        if($this->isEnabled()) {
            return array_merge_recursive(
                $result,
                $this->_configProvider->getConfig()
            );
        }
        return $result;
    }

    /**
     * if enable in admin
     * @return bool
     */
    private function isEnabled()
    {
        return $this->_helper->isEnabled();
    }
}
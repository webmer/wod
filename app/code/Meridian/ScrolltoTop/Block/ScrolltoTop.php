<?php

namespace Meridian\ScrolltoTop\Block;

use Magento\Framework\View\Element\Template;

class ScrolltoTop extends Template
{
    
    /**
     * Get base url without store code
     */
    public function getBaseUrl() {
        return $this->_storeManager
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getConfig($key)
    {
        $result = $this->_scopeConfig->getValue($key,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $result;
    }
   
}
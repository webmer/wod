<?php
/**
 * Meridian Infotech
 * Meridian ScrolltoTop Extension
 *
 * @category   Meridian
 * @package    Meridian_ScrolltoTop
 * @copyright  Copyright © 2006-2016 Meridian (https://www.meridianinfotech.com)
 * @license    https://www.meridianinfotech.com/magento-extension-license/
 */
?>
<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Meridian_ScrolltoTop',
    __DIR__
);
<?php
/**
 * Meridian Infotech
 * Meridian ScrolltoTop Extension
 *
 * @category   Meridian
 * @package    Meridian_ScrolltoTop
 * @copyright  Copyright © 2006-2016 Meridian (https://www.meridianinfotech.com)
 * @license    https://www.meridianinfotech.com/magento-extension-license/
 */
?>
<?php

namespace Meridian\ScrolltoTop\Model\Source;

class Display extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public function getAllOptions() {
        if (!$this->_options) {
            $this->_options = [
                ['label' => 'Text Only', 'value' => 1],
                ['label' => 'Image Only', 'value' => 2],
                ['label' => 'Both Text and Image', 'value' => 3],
            ];
        }
        return $this->_options;
    }
}
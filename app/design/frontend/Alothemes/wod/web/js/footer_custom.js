require(['jquery'], function ($) {


    onResize = function () {
        if ($(window).width() >= 768) {
            $(".footer_info_wrap").show();
        }
    };

    $(document).ready(function () {
        $(".footer_title").click(function (k) {
            if ($(window).width() < 768) {
                $(".footer_title").removeClass("opened");
                var l = $(this).addClass("opened").next();
                isShowing = l.is(":visible");
                $(".footer_title.toggle").next(".footer_info_wrap").slideUp(300);

                if (!isShowing) {
                    l.slideDown(300);
                }
                else {
                    $(this).removeClass("opened");
                }

                k.preventDefault();
            }
        });

        $(window).resize(function () {
            onResize();
        });
    });
    jQuery('div.magiccategory-custom:even').addClass('even');
    jQuery('div.magiccategory-custom:odd').addClass('odd');
});

require(['jquery'], function ($) {
    $(".title_paragraph.toggle").click(function (k) {
        var l = $(this).next();
        isShowing = l.is(":visible");
        $(".title_paragraph.toggle").next(".text_wrap").slideUp(300);
        if (!isShowing) {
            l.slideDown(300)
        }
        k.preventDefault();
    });
});

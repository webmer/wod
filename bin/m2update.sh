#!/bin/bash

composer update
php bin/magento setup:upgrade
php bin/magento snatch-migrations:migrate
php bin/magento indexer:reindex
php bin/magento setup:static-content:deploy en_US
php bin/magento setup:static-content:deploy de_DE
php bin/magento setup:static-content:deploy de_CH
php bin/magento setup:di:compile

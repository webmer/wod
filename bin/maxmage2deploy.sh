#!/bin/bash
clear
echo -e '\E[47;31m'"\033[1mPlease  check your VirtualHost settings.\033[0m"
echo -e '\E[37;44m'"\033[1mYou must have some like:\033[0m"
echo -e "<VirtualHost *:80>"
echo "    ServerName magento2.local"
echo "    DocumentRoot \"/var/www/magento\""
echo "    DirectoryIndex index.php"
echo "    <Directory /var/www/magento/>"
echo "        Options Indexes FollowSymLinks MultiViews"
echo "        AllowOverride All"
echo "        Order allow,deny"
echo "        allow from all"
echo "    </Directory>"
echo "</VirtualHost>"
echo -e '\E[47;31m'"\033[1mDon't fogget about a2enmod.\033[0m"
echo -e '\E[37;44m'"\033[1m chmod -R 777 /var/www/html/MAGENTO_2_ROOT_DIRECTORY/ \033[0m"
echo -e '\E[37;44m'"\033[1m sudo a2enmod rewrite \033[0m"
echo -e '\E[37;44m'"\033[1m sudo service apache2 restart \033[0m"

#composer require cedricblondeau/magento2-module-catalog-import-command
#composer require dompdf/dompdf

echo -e '\E[47;31m'"\033[1mCOMPOSER INSTALL:\033[0m"
composer update



# Check db_host value
if [ -z "$db_host" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter your db-host. Example: localhost \033[0m"
    read db_host
fi


# Check db_name value
if [ -z "$db_name" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter your existing database. Example: magento2 \033[0m"
    read db_name
fi


# Check db_user
if [ -z "$db_user" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter your DB user. Example: root \033[0m"
    read db_user
fi

# Check db_pass
if [ -z "$db_pass" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter your DB user password. Example: rootpassword \033[0m"
    read db_pass
fi

# Check mage_base_url
if [ -z "$mage_base_url" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter your base url for magento(it is your virtual host name). Example: http://magento2.local \033[0m"
    read mage_base_url
fi

# Check mage_admin_user value
if [ -z "$mage_admin_user" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter admin user name. Example: admin_user \033[0m"
    read mage_admin_user
fi

#get admin first name
if [ -z "$mage_admin_firstname" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter admin user firstname. Example: Firstname \033[0m"
    read mage_admin_firstname
fi

# Check mage_admin_lastname
if [ -z "$mage_admin_lastname" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter admin user Lastname. Example: Lastname \033[0m"
    read mage_admin_lastname
fi

# Check mage_admin_email
if [ -z "$mage_admin_email" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter admin user email. Example: m.kindgeek@kindgeek.com \033[0m"
    read mage_admin_email
fi

# Check mage_admin_pass value
if [ -z "$mage_admin_pass" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter admin user password(For login tu admin panel). Example: kindgeek123 \033[0m"
    read mage_admin_pass
fi

# Check mage_admin_path value
if [ -z "$mage_admin_path" ]
  then
    echo -e '\E[47;35m'"\033[1mPlease enter your path to admin panel.Example: admin \033[0m"
    read mage_admin_path
fi

php -f bin/magento setup:install --db-host="$db_host" --db-name="$db_name" --db-user="$db_user" --db-password="$db_pass" --base-url="$mage_base_url" --admin-user="$mage_admin_user" --admin-firstname="$mage_admin_firstname" --admin-lastname="$mage_admin_lastname" --admin-email="$mage_admin_email" --admin-password="$mage_admin_pass" --language="en_US" --currency="USD" --backend-frontname="$mage_admin_path" --use-rewrites="1"

rm -rf var/generation/
chmod -R 777 app/
chmod -R 777 dev/
chmod -R 777 bin/
chmod -R 777 var/
chmod -R 777 pub/
chmod -R 777 lib/
chmod -R 777 vendor/
chmod -R 777 phpserver/
chmod -R 777 setup/
chmod -R 777 index.php
chmod -R 777 composer.json
chmod -R 777 composer.lock
chmod -R 777 auth.json.sample
chmod -R 777 grunt-config.json.sample
chmod -R 777 Gruntfile.js.sample
chmod -R 777 package.json.sample

php bin/magento deploy:mode:set developer
php bin/magento cache:disable

php bin/magento indexer:reindex
php bin/magento setup:upgrade

